var event = {
  validation : function(){
    let validation = {
      rules :{
        'pharmacist-first-name': {
          required : true
        },
        'pharmacist-middle-name': {
          required : true
        },
        'pharmacist-last-name': {
          required : true
        },
        'bc_birthdate':{
          required : true,
          date: true
        },
        'bc_phone_num1':{
          required : true
        },
        'user-email': {
          required : true,
          email : true
        },
        'user-name':{
          required : true,
          minlength: 6
        }
      }
    };

    if($("#main-form").data('action') == 'add'){
      validation.rules['user-password'] = {
        required : true,
        minlength: 6
      };
      validation.rules['user-re-password'] = {
        required : true,
        equalTo : "[name=user-password]"
      };
    }

    $("#main-form").validate(validation);
  }
}


$(document).ready(function(){
  event.validation();
});
