$(document).ready(function(){

  $('#bhws-list').DataTable( {
      responsive: true,
      processing: true,
      serverSide: true,
      bSort: true,
      ajax: {
        url : global.site_name + 'users/bhws_ref',
        type : 'POST',
        dataType : 'json',
        data : function(params){
          return params;
        },
        dataSrc: function(result){
          // let format = {
          //   data : result.data
          // , recordsTotal:result.count
          // };
          result.recordsTotal = result.total_count;
          result.recordsFiltered = result.count;

          return result.data;
        },
        cache: true
      },
      columnDefs: [ {
        //This is for the custom button
          targets: -1,
          data: "id",
          render: function ( data, type, row, meta ) {
            // return '';
            let id = data;
            let html = '<div class="text-center"><a class="btn btn-sm btn-success has-tooltip" title="View" href="' + global.site_name + 'users/edit-bhws/' + id  + '"><i class="fa fa-search"></i></a> ' +
            '<a class="btn btn-sm btn-info has-tooltip" title="Edit" href="' + global.site_name + 'users/edit-bhws/' + id  + '"><i class="fa fa-pencil"></i></a> ' +
            '<button class="btn btn-sm btn-danger has-tooltip delete-row" title="Delete" value="' + id + '"><i class="fa fa-trash"></i></button></div>'
            return html;
          }
      }, ''],
      initComplete: function(){
        let toolbar = '<div class="pull-right"><a class="btn btn-default" role="button" href="' + global.site_name + 'users/add-bhws/' + '"><i class="fa fa-file">&nbsp</i> ADD</a></div>';;
        $("div.dt-toolbar").html(toolbar);
      },
      processing : function( e, settings, processing ) {
        //Loading animation here
        //$('#processingIndicator').css( 'display', processing ? 'block' : 'none' );
      },
      fnDrawCallback: function (oSettings) {
        $('.delete-row').click(function(){
          let params = {};
          params['id'] = $(this).val();
          bootbox.confirm({
              title: "Delete row",
              message: "Are you sure you want to delete this?",
              buttons: {
                  cancel: {
                      label: '<i class="fa fa-times"></i> Cancel'
                  },
                  confirm: {
                      label: '<i class="fa fa-check"></i> Confirm'
                  }
              },
              callback: function (result) {
                if(result){
                  $.ajax({
                    url : global.site_name + 'users/delete-bhws/',
                    type : 'POST',
                    dataType : 'json',
                    data : params,
                    success : function(){
                      bootbox.alert("Delete Successful");
                      $('#bhws-list').DataTable().ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        bootbox.alert("Something went wrong!");
                        //alert(xhr.status);
                        //alert(thrownError);
                    }
                  })
                }
              }
          });

        });

      },
      dom: 'l<"dt-toolbar">frtip',
      buttons: [
          {
              text: 'ADD',
              action: function ( e, dt, node, config ) {

              }
          }
      ],
      order:[[0,'asc']],
      columns: [
        { "data": "health_worker" },
        { "data": "brgy_name" },
        { "data": "user_name" },
        {   "data": "bc_gender"
          ,   "render": function(data, type, full, meta) {
              return full['bc_gender'] + ' / ' + full['age'];
          }
        },
        { "data": "user_email"},
        { "data": "bhws_id"
        , "searchable": false}
      ]
  });
});
