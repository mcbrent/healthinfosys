var event = {
  validation : function(){
    let validation = {
      rules :{
        'designated-barangay':{
          required: true
        },
        'admin-first-name': {
          required : true
        },
        'admin-last-name': {
          required : true
        },
        'admin-last-name': {
          required : true
        },
        'bc_birthdate':{
          required : true,
          date: true
        },
        'bc_phone_num1':{
          required : true
        },
        'user-email': {
          required : true,
          email : true
        },
        'user-name':{
          required : true,
          minlength: 6
        }
      }
    };

    if($("#main-form").data('action') == 'add'){
      validation.rules['user-password'] = {
        required : true,
        minlength: 6
      };
      validation.rules['user-re-password'] = {
        required : true,
        equalTo : "[name=user-password]"
      };
    }

    $("#main-form").validate(validation);
  }
}


$(document).ready(function(){

  event.validation();

  $('[name=designated-barangay]').select2({
    allowClear: true,
    ajax:{
      url: global.site_name + 'reference/ref',
      dataType: 'json',
      type:'post',
      data: function(params){
          let search = $.isEmptyObject(params)? '' : params.term;
          let option = {
            columns :[
              {
                "data": "brgy_name"
              , "searchable": true },
              {
                "data": "brgy_id"
              , "searchable": false }
            ],
            order : [
              {
                'column' : 0,
                'dir'   : 'asc'
              }
            ],
            search : {
              'value' : search
            , 'regex' : false
            },
            option : {
              'type' : 'barangay'
            },
            length : 25,
            condition:[]
          };
          option.condition.push({
              column  : 'city_id'
            , value   : '474'
          });
          return option;
      },
      processResults: function (data) {
      // Tranforms the top-level key of the response object from 'items' to 'results'
        let result = [];
        data['data'].forEach(function(d){
          result.push({
            id  : d.brgy_id
          , text: d.brgy_name
          });
        });
        return {
          results: result
        }
      }
    }
  });

});
