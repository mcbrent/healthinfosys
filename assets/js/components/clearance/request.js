$(document).ready(function(){


  $("#main-form").validate({
      rules : {
          'resident-first-name' : {
            required : true
          }
        ,  'resident-middle-name' : {
            required : true
          }
        ,  'resident-last-name' : {
            required : true
          }
        ,  'add-desc' : {
            required : true
          }
        ,  'current-barangay' : "required"
        ,  'resident-gender' : {
            required : true
          }
        ,  'resident-birthday' : {
            required : true,
            date: true
          }
        , 'resident-civil-status' : {
            required : true
          }
        , 'resident-nationality' : {
            required : true
          }
        , 'phone-number-1' : {
            required : true
          }
        , 'resident-yrs-community' : {
            required : true
          , number: true
          }
        , 'resident-weight' : {
            required : true
          , number: true
          }
        , 'resident-height' : {
            required : true
          , number: true
          }
        , 'resident-alias' : {
            required : true
          }
        , 'resident-occupation' : {
            required : true
          }
        , 'resident-religion' : {
            required : true
          }
        , 'resident-completion' : {
            required : true
          }
        , 'resident-hair-color' : {
            required : true
          }
        , 'resident-eye-color' : {
            required : true
          }
        , 'resident-body-size' : {
            required : true
          }
        , 'resident-distinguishing-marks' : {
            required : true
          }
        , 'purpose' : {
            required : true
          }
      },
      messages : {
        'current-barangay' : "Select Barangay"
      , 'phone-number-1' : "At least 1 phone number is required"
      }
 });

  $('[name=current-barangay]').select2({
    allowClear: true,
    ajax:{
      url: global.site_name + 'reference/ref',
      dataType: 'json',
      type:'post',
      data: function(params){
          let search = $.isEmptyObject(params)? '' : params.term;
          let option = {
            columns :[
              {
                "data": "brgy_name"
              , "searchable": true },
              {
                "data": "brgy_id"
              , "searchable": false }
            ],
            order : [
              {
                'column' : 0,
                'dir'   : 'asc'
              }
            ],
            search : {
              'value' : search
            , 'regex' : false
            },
            option : {
              'type' : 'brgy'
            },
            length : 25,
            condition:[]
          };
          option.condition.push({
              column  : 'city_id'
            , value   : '447'
          });
          return option;
      },
      processResults: function (data) {
      // Tranforms the top-level key of the response object from 'items' to 'results'
        let result = [];
        data['data'].forEach(function(d){
          result.push({
            id  : d.brgy_id
          , text: d.brgy_name
          });
        });
        return {
          results: result
        }
      }
    }
  });


}).on("keypress", "form", function(event) {
  return event.keyCode != 13;
});
