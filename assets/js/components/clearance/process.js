$(document).ready(function(){
  $('#print-process').click(function(){
    let resident_id = $(this).val();
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: global.site_name + 'resident/print',
        data : {
          'resident': resident_id
        },
        success : function(result){
          console.log(result)
           var win = window.open(global.site_name + 'upload/pdf/'+result.pdf_name, '_blank');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it
                alert('Please allow popups for this website');
            }

        },
        error: function (request, status, error) {

        }
    });
  });


  $('.save-file').click(function(){
    let params = {};
    params['id'] = $(this).val();

    $.ajax({
      url :  global.site_name + 'resident/save-file' ,
      type : 'POST',
      dataType : 'json',
      data : params,
      success : function(result){
        window.open(
          global.site_name + 'upload/temp/' + result.file_name,
          '_blank');
      },
      error: function (xhr, ajaxOptions, thrownError) {
          bootbox.alert("Something went wrong!");
          //alert(xhr.status);
          //alert(thrownError);
      }
    })


  });

}).on("keypress", "form", function(event) {
  return event.keyCode != 13;
});
