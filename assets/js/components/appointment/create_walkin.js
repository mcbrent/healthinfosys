$(document).ready(function(){
  $("[name=schedule-date]").datepicker({
      keyboardNavigation: false,
      forceParse: false,
      autoclose: true,
      todayBtn: true,
      todayHighlight: true,
      format:'mm/dd/yyyy'
  });

  $('#modal-select-resident').click(function(){
    let search_resident = $('.select2-resident-key').val();
    $('[name=resident-no]').val(search_resident)
  });

  $('#main-form').validate({
      rules : {
          'schedule-date' : {
            required : true
          }
        ,  'schedule-time' : {
            required : true
          }
      }
    })

    // $("[name=schedule-date]").datetimepicker({
    //   defaultDate: "11/1/2013",
    //   disabledDates: [
    //       moment("12/25/2013"),
    //       new Date(2013, 11 - 1, 21),
    //       "11/22/2013 00:53"
    //   ]
    // });
});
