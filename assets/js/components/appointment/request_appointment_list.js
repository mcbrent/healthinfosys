$(document).ready(function(){
  $('.quick-check-in').click(function(){
    let appointment_id = $(this).val();
    let current_btn = $('.quick-check-in');
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: global.site_name + 'appointment/quick-check-in',
        data : {
          'appointment-id': appointment_id
        },
        success : function(result){
          console.log(result)
          current_btn.hide();
        },
        error: function (request, status, error) {

        }
    });
  });
});
