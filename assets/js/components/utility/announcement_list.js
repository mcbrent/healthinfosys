$(document).ready(function(){


    let config = {
      url : global.site_name + 'utility/announcement-table',
      order_col : 0,
      req_data : {},
      column :[
        { "data": "announcement_date" },
        { "data": "announcement_title" },
        { "data": "annoucement_id"
        , "searchable": false}
      ],
      add_url : global.site_name + 'utility/add-announcement',
      edit_url  : global.site_name + 'utility/edit-announcement',
      delete_url : global.site_name + 'utility/delete-announcement',
      page_var : {},
      columnDefs : {}
    }
    helper.datatable_basic('.datatable-basic', config);

});
