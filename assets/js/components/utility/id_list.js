$(document).ready(function(){

  $('#all-user-list').DataTable( {
      responsive: true,
      processing: true,
      serverSide: true,
      bSort: true,
      ajax: {
        url : global.site_name + 'utility/id-list-ref',
        type : 'POST',
        dataType : 'json',
        data : function(params){
          return params;
        },
        dataSrc: function(result){
          // let format = {
          //   data : result.data
          // , recordsTotal:result.count
          // };
          result.recordsTotal = result.total_count;
          result.recordsFiltered = result.count;

          return result.data;
        },
        cache: true
      },
      columnDefs: [ {
        //This is for the custom button
          targets: -1,
          data: "id",
          render: function ( data, type, row, meta ) {
            // return '';
            let id = data;
            let html = '<div class="text-center"><button class="btn btn-sm btn-success has-tooltip print-id" title="Print ID" value="' + id + '"><i class="fa fa-file-pdf-o"></i></button> <button class="btn btn-sm btn-info has-tooltip print-referral" title="Print Referral" value="' + id + '"><i class="fa fa-print"></i></button></div> '
            return html;
          }
      }, ''],
      initComplete: function(){

      },
      processing : function( e, settings, processing ) {
        //Loading animation here
        //$('#processingIndicator').css( 'display', processing ? 'block' : 'none' );
      },
      // fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
      //   console.log("Event Added")
      // },
      fnDrawCallback: function (oSettings) {
        $('.print-id').click(function(){
          window.open(global.site_name + 'utility/print-id/' + $(this).val() ,'_blank');
        });
        $('.print-referral').click(function(){
          window.open(global.site_name + 'utility/print-referral/' + $(this).val() ,'_blank');
        });

      },
      dom: 'l<"dt-toolbar">frtip',
      buttons: [
          {
              text: 'ADD',
              action: function ( e, dt, node, config ) {

              }
          }
      ],
      order:[[0,'asc']],
      columns: [

        {   "data": "first_name"
          ,   "render": function(data, type, full, meta) {
              return full['first_name'] + ' ' + full['middle_name'] + ' ' + full['last_name'];
          }
        },
        { "data": "user_type" },
        { "data": "user_id"
        , "searchable": false}
      ]
  });

});
