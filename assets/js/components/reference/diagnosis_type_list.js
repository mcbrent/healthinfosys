$(document).ready(function(){


    let config = {
      url : global.site_name + 'reference/ref',
      order_col : 1,
      req_data : {type : 'diagnosis-type'},
      column :[
        { "data": "dt_code"},
        { "data": "dt_name"},
        { "data": "dt_id"
        , "searchable": false}
      ],
      add_url : global.site_name + 'reference/add-diagnosis-type',
      edit_url  : global.site_name + 'reference/edit-diagnosis-type',
      delete_url : global.site_name + 'reference/delete-ref',
      page_var : {
        type : 'diagnosis-type'
      }
    }
    helper.datatable_basic('.datatable-basic', config);

});
