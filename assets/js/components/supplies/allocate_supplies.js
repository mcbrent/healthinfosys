$(document).ready(function(){
  $('#user-type').change(function(){
    let resident_type = $('#user-type option:selected').text();
    $('.type-description').text(resident_type);
    $('[name=user-id]').select2("val", "");
  });

  $('#allocate-supplies').click(function(){
    let table = [];

    $('#supplies-allocation-detail tbody tr').each(function(){
      let current_row = $(this);
      if(current_row.find('.select2-supplies').select2('val') != null){
        table.push({
          'action'  : current_row.data('state')
        , 'id'  : current_row.data('id')
        , 'supply_id'  : current_row.find('.select2-supplies').select2('val')
        , 'supply_quantity' : current_row.find('.supplies-quantity').val()
        })
      }


    });
    $('[name=supplies-allocation-detail-json]').val(JSON.stringify(table))
    $('#main-form').submit();
  });

  $("#main-form").validate({
        rules : {
            'user-id' : {
              required : true
            }
        }
   });

  $('[name=user-id]').select2({
    placeholder: "Select a User",
    allowClear: true,
    ajax:{
      url: global.site_name + 'users/user-ref',
      dataType: 'json',
      type:'post',
      data: function(params){
          let search = $.isEmptyObject(params)? '' : params.term;
          let option = {
            search      : search,
            'user-type' : $('#user-type').val()
          };
          return option;
      },
      processResults: function (data) {
      // Tranforms the top-level key of the response object from 'items' to 'results'
        let result = [];
        data.forEach(function(d){
          result.push({
            id  : d.user_id
          , text: d.user_fullname
          });
        });
        return {
          results: result
        }
      }
    }
  });

  $('#add-detail').click(function(){
    let row_value = {
      'state' : 'add'
    , 'number': allocate_supplies.generate_row_id()
    }

    //let row_template = allocate_supplies.row_template();
    //let current = allocate_supplies.convert_content($('#supplies-allocation-detail').data('content'));
    // let current = $('#supplies-allocation-detail').data('content');
    //current['row'] = "";
    //console.log(current)
    let row = allocate_supplies.row_template(row_value);
    $('#supplies-allocation-detail tbody').append(row);
    allocate_supplies.row_events();
    //console.log($('#supplies-allocation-detail').data('content'))
  });



});



var allocate_supplies = {
  row_template : function(row){
    let row_color = '';
    if(row.state == 'add'){
      row_color = 'success'
    }
    row.id = (row.id)? row.id : '0';



    let return_row = '<tr id="' + row.number + '" class="' + row_color + '" data-id="' + row.id + '" data-state="' + row.state + '">' +
      '<td>' +
        '<select class="form-control select2-supplies" >' +
        '</select>' +
      '</td>' +
      '<td>' +
        '<input type="number" min="0" class="form-control supplies-quantity limit-number"/>' +
      '</td>' +
      '<td class="text-center">' +
        '<button class="btn btn-sm btn-danger delete-row" type="button"><i class="fa fa-times"></i></button>' +
      '</td>' +
    '</tr>';
    return return_row;
  },
  convert_content : function(json_string){
    // return JSON.parse(decodeURIComponent(json_string));
    return JSON.parse(json_string);
  },
  row_events  : function(){
    $('.delete-row').unbind();
    $( ".select2-supplies" ).unbind();
    $('.delete-row').click(function(){
      let row = $(this).closest('tr');
      if(row.data('state') == 'add'){
        row.remove();
      }
    });

    $('.select2-supplies').select2({
      allowClear: true,
      ajax:{
        url: global.site_name + 'supplies/supplies-ref',
        dataType: 'json',
        type:'post',
        data: function(params){
            let search = $.isEmptyObject(params)? '' : params.term;
            let option = {
              columns :[
                {
                  "data": "ms_description"
                , "searchable": true },
                {
                  "data": "ms_unit"
                , "searchable": true },
                {
                  "data": "ms_actual_stock"
                , "searchable": true },
                {
                  "data": "ms_id"
                , "searchable": false }
              ],
              order : [
                {
                  'column' : 0,
                  'dir'   : 'asc'
                }
              ],
              search : {
                'value' : search
              , 'regex' : false
              },
              option : {
              },
              length : 25,
              start : 0,
              condition:[]
            };

            return option;
        },
        processResults: function (data) {
        // Tranforms the top-level key of the response object from 'items' to 'results'
          let result = [];
          data['data'].forEach(function(d){
            result.push({
              id  : d.ms_id
            , text: d.ms_description + ' / ' + d.ms_unit
            });
          });
          return {
            results: result
          }
        }
      }
    }).change(function(){
      let supply_id = $(this).val();
      let supplies_text = $(this).parent().parent().find('.supplies-quantity');
      $.ajax({
  			type: 'POST',
  			dataType: 'json',
  			url: global.site_name + 'supplies/check-supplies',
  			data : {
          supply_id : supply_id
        },
        complete : function(){
        },
  			success : function(result){
          supplies_text.attr('max',result.ms_actual_stock);
          console.log(result)
          // if(result.success){
  				// 	window.location.reload();
  				// }
  			}
  		});

      supplies_text.val('1');
    });
  },
  generate_row_id  : function(){
    if(($('#supplies-allocation-detail tbody tr').length) > 0){
      return Number($('#supplies-allocation-detail tbody tr:last').attr('id')) + 1;
    }
    else{
      return 1;
    }
  }
}
