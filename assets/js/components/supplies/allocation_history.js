$(document).ready(function(){

  $('#supplies-list').DataTable( {
      responsive: true,
      processing: true,
      serverSide: true,
      bSort: true,
      ajax: {
        url : global.site_name + 'supplies/supplies-history-ref',
        type : 'POST',
        dataType : 'json',
        data : function(params){
          return params;
        },
        dataSrc: function(result){
          // let format = {
          //   data : result.data
          // , recordsTotal:result.count
          // };
          result.recordsTotal = result.total_count;
          result.recordsFiltered = result.count;

          return result.data;
        },
        cache: true
      },
      columnDefs: [ {
        //This is for the custom button
          targets: -1,
          data: "id",
          render: function ( data, type, row, meta ) {
            // return '';
            let id = data;
            let html = '<div class="text-center">' +
            '<a class="btn btn-sm btn-info has-tooltip" title="Edit" href="' + global.site_name + 'supplies/edit-supplies/' + id  + '"><i class="fa fa-pencil"></i></a> ' +
            '</div>'
            return html;
          }
      }, ''],
      initComplete: function(){
        let toolbar = '<div class="pull-right"></div>';;
        $("div.dt-toolbar").html(toolbar);
      },
      processing : function( e, settings, processing ) {
        //Loading animation here
        //$('#processingIndicator').css( 'display', processing ? 'block' : 'none' );
      },
      fnDrawCallback: function (oSettings) {
        $('.delete-row').click(function(){
          let params = {};
          params['id'] = $(this).val();
          bootbox.confirm({
              title: "Delete row",
              message: "Are you sure you want to delete this?",
              buttons: {
                  cancel: {
                      label: '<i class="fa fa-times"></i> Cancel'
                  },
                  confirm: {
                      label: '<i class="fa fa-check"></i> Confirm'
                  }
              },
              callback: function (result) {
                if(result){
                  $.ajax({
                    url : global.site_name + 'supplies/delete-supplies/',
                    type : 'POST',
                    dataType : 'json',
                    data : params,
                    success : function(){
                      bootbox.alert("Delete Successful");
                      $('#supplies-list').DataTable().ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        bootbox.alert("Something went wrong!");
                        //alert(xhr.status);
                        //alert(thrownError);
                    }
                  })
                }
              }
          });

        });

      },
      dom: 'l<"dt-toolbar">rtip',
      buttons: [
          {
              text: 'ADD',
              action: function ( e, dt, node, config ) {

              }
          }
      ],
      order:[[0,'desc']],
      columns: [
        { "data": "sa_date" },
        { "data": "from_name",
          "render": function(data, type, full, meta) {
                 return '<b>' + full['from_type'] + '</b> - ' + full['from_name'];
          }
        },
        { "data": "to_name",
          "render": function(data, type, full, meta) {
                 return '<b>' + full['to_type'] + '</b> - ' + full['to_name'];
          }
        },
        // {   "data": "ms_actual_stock"
        //   ,   "render": function(data, type, full, meta) {
        //       return full['ms_actual_stock'] + ' / ' + full['ms_unit'];
        //   }
        // },
        { "data": "sa_id"
        , "searchable": false}
      ]
  });
});
