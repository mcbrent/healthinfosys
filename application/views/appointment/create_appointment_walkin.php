<div class="wrapper wrapper-content">
  <div class="row">

    <div class="col-lg-10">
    </div>
    <div class="col-lg-2 text-right">
      <button class="btn btn-info dim has-tooltip form-submit" name="save" title="Save Appointment" data-form="main-form"><i class="fa fa-pencil"></i> Create Appointment</button>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3>Create Appointment</h3>
        </div>
        <div class="panel-body">

          <form id="main-form" class="form-horizontal" method="POST" enctype="multipart/form-data" action="?">
            <input type="hidden" name="resident-no" value="0">
            <div class="form-group">
              <div class="col-md-10">
                <input type="text" class="form-control" placeholder="Scan QR-Code Here">
              </div>
              <div class="col-md-2">
                <button id="search-resident" type="button" class="btn btn-md has-tooltip" title="Search" data-toggle="modal" data-target="#resident-lookup"><i class="fa fa-search"></i></button>
              </div>
            </div>
            <div class="panel panel-success" style="display:none" >
                <div class="panel-heading" data-toggle="collapse" data-target="#resident-info-panel">
                  <h4>Resident Information</h4>
                </div>
                <div id="resident-info-panel" class="panel-body collapse" >
                  <div class="row">
                    <div class="col-md-6">

                    </div>
                  </div>

                </div>
            </div>
            <div class="hr-line-solid"></div>
            <div class="form-group">
              <div class="col-md-3">
                <h5>Check in Date</h5>
                <input type="text" name="schedule-date" class="form-control" placeholder="Date of Appointment" autocomplete="off">
              </div>
              <div class="col-md-3">
                <h5>&nbsp;</h5>
                <select name="schedule-time" class="form-control select2-basic">
                  <option value="0">12:00AM</option>
                  <option value="1">1:00AM</option>
                  <option value="2">2:00AM</option>
                  <option value="3">3:00AM</option>
                  <option value="4">4:00AM</option>
                  <option value="5">5:00AM</option>
                  <option value="6">6:00AM</option>
                  <option value="7">7:00AM</option>
                  <option value="8">8:00AM</option>
                  <option value="9">9:00AM</option>
                  <option value="10">10:00AM</option>
                  <option value="11">11:00AM</option>
                  <option value="12">12:00PM</option>
                  <option value="13">1:00PM</option>
                  <option value="14">2:00PM</option>
                  <option value="15">3:00PM</option>
                  <option value="16">4:00PM</option>
                  <option value="17">5:00PM</option>
                  <option value="18">6:00PM</option>
                  <option value="19">7:00PM</option>
                  <option value="20">8:00PM</option>
                  <option value="21">9:00PM</option>
                  <option value="22">10:00PM</option>
                  <option value="23">11:00PM</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12">
                <h4>Initial Summary</h4>
                <small>State here some information from the resident for the medical officer.</small>
                <textarea class="form-control summernote" name="initial-summary"></textarea>
              </div>
            </div>
            <div class="hr-line-dashed"></div>


          </form>
        </div>
      </div>


    </div>
  </div>


</div>

<?php $this->load->view('appointment/resident_lookup') ?>
<script src="<?php echo JS_DIR ?>components/appointment/create_walkin.js"></script>
