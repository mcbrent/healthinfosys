<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
      <div class="col-lg-12">
              <div class="panel panel-primary">
                  <div class="panel-heading">
                      <h5>Request Appointment List</h5>
                  </div>
                  <div class="panel-body">

                    <div class="table-responsive">
                      <table id="complaint-list" class="table table-striped table-bordered table-hover" data-config="{}">
                        <thead>
                          <tr>
                            <th class="col-md-1 text-center">Time</th>
                            <th class="col-md-2 text-center">Requested Appointment Date</th>
                            <th class="col-md-2 text-center">Resident</th>
                            <th class="col-md-1 text-center">Gender / Age</th>
                            <th class="col-md-1 text-center">Barangay</th>
                            <th class="col-md-1 text-center">Barangay</th>
                            <th class="col-md-1 text-center">Resident No</th>
                            <th class="col-md-2 text-center">Status</th>
                            <th class="col-md-1 text-center no-sort">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php foreach($requestedAppointmentList as $appointment){ ?>
                          <tr>
                            <td><?php echo date('m/d/Y - h:i:s A', strtotime($appointment['appointment_requestsent_datetime']))?></td>
                            <td class="te"><?php echo date('M d Y - H:i A', strtotime($appointment['appointment_requested_datetime']))?></td>
                            <td><?php echo $appointment['resident'] ?></td>
                            <td class="text-uppercase"><?php echo $appointment['bc_gender'].' / '.$appointment['age'].' Year/s Old' ?></td>
                            <td><?php echo $appointment['brgy_name'] ?></td>
                            <td></td>
                            <td><?php echo $appointment['resident_no'] ?></td>
                            <td><?php echo "Waiting for approval" ?></td>
                            <td>
                              <div class="text-center">
                                <a class="btn btn-info has-tooltip" title="View Appointment" href="<?php echo DOMAIN.'appointment/approve_appointment/'.$appointment['appointment_id'] ?>"><i class="fa fa-search"></i></a>
                                <button class="btn btn-danger has-tooltip delete-row" title="Delete" value=""><i class="fa fa-trash"></i></button>
                              </div>
                            </td>
                          </tr>
                        <?php }?>
                        </tbody>
                      </table>
                    </div>

                  </div>
              </div>
          </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h5>Confirmed Appointment</h5>
                    </div>
                    <div class="panel-body">

                      <div class="table-responsive">
                        <table id="complaint-list" class="table table-striped table-bordered table-hover" data-config="{}">
                          <thead>
                            <tr>
                              <th class="col-md-1 text-center">Confirmed Time</th>
                              <th class="col-md-2 text-center">Appointment Schedule</th>
                              <th class="col-md-3 text-center">Resident</th>
                              <th class="col-md-1 text-center">Resident No</th>
                              <th class="col-md-2 text-center">Gender / Age</th>
                              <th class="col-md-1 text-center">Barangay</th>
                              <th class="col-md-1 text-center">Status</th>
                              <th class="col-md-1 text-center no-sort">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php foreach($confirmedAppointmentList as $appointment){ ?>
                            <tr>
                              <td><?php echo date('m/d/Y - h:i:s A', strtotime($appointment['appointment_requestsent_datetime']))?></td>
                              <td class="te"><?php echo date('M d Y - h:i A', strtotime($appointment['appointment_approved_sched']))?></td>
                              <td><?php echo $appointment['resident'] ?></td>
                              <td><?php echo $appointment['resident_no'] ?></td>
                              <td class="text-uppercase"><?php echo $appointment['bc_gender'].' / '.$appointment['age'].' Year/s Old' ?></td>
                              <td><?php echo $appointment['brgy_name'] ?></td>
                              <td class="text-center"><?php if($appointment['appointment_status'] == '3'){
                                echo 'Appointed';
                              }
                              else if($appointment['appointment_status'] == '4'){
                                echo 'Checked In';
                              } ?></td>
                              <td>
                                <div class="text-center">
                                  <?php if($appointment['appointment_status'] == '3'){ ?>
                                    <button class="btn btn-warning has-tooltip quick-check-in" title="Quick Check In" value="<?php echo $appointment['appointment_id'] ?>"><i class="fa fa-hospital-o"></i></button>
                                  <?php }
                                  ?>
                                  <a class="btn btn-info has-tooltip" title="View Appointment" href="<?php echo DOMAIN.'appointment/view-medical-treatment/'.$appointment['appointment_id'] ?>"><i class="fa fa-search"></i></a>
                                </div>
                              </td>
                            </tr>
                          <?php }?>
                          </tbody>
                        </table>
                      </div>

                    </div>
                </div>
            </div>
      </div>

</div>
<script src="<?php echo JS_DIR ?>components/appointment/request_appointment_list.js"></script>
