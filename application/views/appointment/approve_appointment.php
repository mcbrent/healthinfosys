<div class="wrapper wrapper-content">
  <div class="row">

    <div class="col-lg-6">
    </div>
    <div class="col-lg-6 text-right">
      <button class="btn btn-success dim has-tooltip submit" name="save" title="Approve Appointment" value="approved"><i class="fa fa-pencil"></i> Approve Appointment</button>
      <button class="btn btn-danger dim has-tooltip submit" name="save" title="Reject Appointment" value="declined"><i class="fa fa-times"></i> Reject Appointment</button>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3>Appointment Request</h3>
        </div>
        <div class="panel-body">

          <form id="main-form" class="form-horizontal" method="POST" enctype="multipart/form-data" action="?">
            <input type="hidden" name="action" value="declined">
            <div class="panel panel-success" style="display:none" >
                <div class="panel-heading" data-toggle="collapse" data-target="#resident-info-panel">
                  <h4>Resident Information</h4>
                </div>
                <div id="resident-info-panel" class="panel-body collapse" >
                  <div class="row">
                    <div class="col-md-6">

                    </div>
                  </div>

                </div>
            </div>
            <div class="hr-line-solid"></div>
            <div class="form-group">
              <div class="col-md-12">
                <h5>Requested Appoinment Date</h5>
                <p class="font-bold text-info"><?php echo date('F/d/Y - l - h:00 A', strtotime($form_data['appointment_requested_datetime'])) ?></p>
              </div>
            </div>
            <div class="form-group">

              <div class="col-md-3">
                <h5>Schedule Appointment Date</h5>
                <input type="text" name="schedule-date" class="form-control" placeholder="Date of Appointment" value="<?php echo date('m/d/Y', strtotime($form_data['appointment_requested_datetime'])) ?>">
              </div>
              <div class="col-md-3">
                <h5>&nbsp;</h5>
                <select name="schedule-time" class="form-control select2-basic">
                  <option value="0" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '0'? 'selected' : '' ?>>12:00AM</option>
                  <option value="1" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '1'? 'selected' : '' ?>>1:00AM</option>
                  <option value="2" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '2'? 'selected' : '' ?>>2:00AM</option>
                  <option value="3" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '3'? 'selected' : '' ?>>3:00AM</option>
                  <option value="4" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '4'? 'selected' : '' ?>>4:00AM</option>
                  <option value="5" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '5'? 'selected' : '' ?>>5:00AM</option>
                  <option value="6" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '6'? 'selected' : '' ?>>6:00AM</option>
                  <option value="7" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '7'? 'selected' : '' ?>>7:00AM</option>
                  <option value="8" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '8'? 'selected' : '' ?>>8:00AM</option>
                  <option value="9" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '9'? 'selected' : '' ?>>9:00AM</option>
                  <option value="10" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '10'? 'selected' : '' ?>>10:00AM</option>
                  <option value="11" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '11'? 'selected' : '' ?>>11:00AM</option>
                  <option value="12" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '12'? 'selected' : '' ?>>12:00PM</option>
                  <option value="13" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '13'? 'selected' : '' ?>>1:00PM</option>
                  <option value="14" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '14'? 'selected' : '' ?>>2:00PM</option>
                  <option value="15" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '15'? 'selected' : '' ?>>3:00PM</option>
                  <option value="16" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '16'? 'selected' : '' ?>>4:00PM</option>
                  <option value="17" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '17'? 'selected' : '' ?>>5:00PM</option>
                  <option value="18" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '18'? 'selected' : '' ?>>6:00PM</option>
                  <option value="19" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '19'? 'selected' : '' ?>>7:00PM</option>
                  <option value="20" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '20'? 'selected' : '' ?>>8:00PM</option>
                  <option value="21" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '21'? 'selected' : '' ?>>9:00PM</option>
                  <option value="22" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '22'? 'selected' : '' ?>>10:00PM</option>
                  <option value="23" <?php echo date('G', strtotime($form_data['appointment_requested_datetime'])) == '23'? 'selected' : '' ?>>11:00PM</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12">
                <h5>Request Appointment Response</h5>
                <textarea  name="appointment-approved-message" class="form-control" maxlength="250" placeholder="Enter response about the requested appointment"></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12">
                <h4>Initial Summary</h4>
                <small>State here some information from the resident for the medical officer.</small>
              </div>
              <div class="col-md-12">
                <div class="border-top-bottom p-sm"><?php echo $form_data['appointment_initial_summary']?></div>
              </div>
            </div>


          </form>
        </div>
      </div>


    </div>
  </div>


</div>

<?php $this->load->view('appointment/resident_lookup') ?>
<script src="<?php echo JS_DIR ?>components/appointment/approve_appointment.js"></script>
