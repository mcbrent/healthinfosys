<div class="wrapper wrapper-content">
  <div class="row">

    <div class="col-lg-6">
    </div>
    <div class="col-lg-6 text-right">
      <?php if($status['cancel_button']){ ?>
        <button class="btn btn-danger dim has-tooltip submit" name="save" title="Cancel Appointment" value="cancel"><i class="fa fa-ban"></i> Cancel Appointment</button>
      <?php }?>
      <?php if($status['confirm_button']){ ?>
        <button class="btn btn-success dim has-tooltip submit" name="save" title="Confirm Appointment" value="confirm"><i class="fa fa-check"></i> Confirm Appointment</button>
      <?php }?>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3>View Appointment</h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12">
              <div class="bg-muted">Status: <h3 class="text-capitalize"><u><?php echo $status['description']; ?></u></h3></div>
            </div>
          </div>
          <form id="main-form" class="form-horizontal" method="POST" enctype="multipart/form-data" action="?">
            <input type="hidden" name="action" value="none">
            <div class="panel panel-success" style="display:none" >
                <div class="panel-heading" data-toggle="collapse" data-target="#resident-info-panel">
                  <h4>Resident Information</h4>
                </div>
                <div id="resident-info-panel" class="panel-body collapse" >
                  <div class="row">
                    <div class="col-md-6">

                    </div>
                  </div>

                </div>
            </div>
            <div class="hr-line-solid"></div>
            <div class="form-group">
              <div class="col-md-12">
                <h5>Requested Appoinment Date</h5>
                <?php if($status['has_requested_date'] && $status['requested_date_done']){ ?>
                  <p class="text-danger"><strike><?php echo date('F/d/Y - l - h:00 A', strtotime($form_data['appointment_requested_datetime'])) ?></strike></p>
                <?php }
                else if($status['has_requested_date'] && !$status['requested_date_done']){ ?>
                  <p class="text-success"><?php echo date('F/d/Y - l - h:00 A', strtotime($form_data['appointment_requested_datetime'])) ?></p>
                <?php }
                else { ?>
                  <p class="text-muted"><?php echo date('F/d/Y - l - h:00 A', strtotime($form_data['appointment_requested_datetime'])) ?></p>
                <?php }?>
              </div>
            </div>
            <?php if($status['approved_sched']){ ?>
              <div class="form-group">
                <div class="col-md-12">
                  <h5>Schedule Appointment Date</h5>
                  <p class="font-bold text-info"><?php echo date('F/d/Y - l - h:00 A', strtotime($form_data['appointment_approved_sched'])) ?></p>
                </div>
              </div>
            <?php }?>
            <?php if($status['approved_message']){ ?>
              <div class="form-group">
                <div class="col-md-12">
                  <h5>Approved Message</h5>
                  <blockquote><?php echo $form_data['appointment_approved_message'] ?></blockquote>
                </div>
              </div>
            <?php }?>
            <div class="form-group">
              <div class="col-md-12">
                <h4>Initial Summary</h4>
                <small>State here some information from the resident for the medical officer.</small>
              </div>
              <div class="col-md-12">
                <div class="border-top-bottom p-sm"><?php echo $form_data['appointment_initial_summary']?></div>
              </div>
            </div>


          </form>
        </div>
      </div>


    </div>
  </div>


</div>

<?php $this->load->view('appointment/resident_lookup') ?>
<script src="<?php echo JS_DIR ?>components/appointment/approve_appointment.js"></script>
