<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
      <div class="col-lg-12">
              <div class="panel panel-primary">
                  <div class="panel-heading">
                      <h5>My Appointment List</h5>
                  </div>
                  <div class="panel-body">
                    <div class="row">
                      <dl class="col-md-3">
                        <dd>*Note: Row Color Meaning</dd>
                        <dd class="bg-muted">Request</dd>
                        <dd class="bg-warning">Approved</dd>
                        <dd class="bg-primary">Confirmed</dd>
                        <dd class="bg-success">Completed / Done</dd>
                        <dd class="bg-danger">Cancelled / Declined </dd>
                      </dl>
                    </div>

                    <div class="table-responsive">
                      <table id="appointment-list" class="table table-striped table-bordered table-hover" data-config="{}">
                        <thead>
                          <tr>
                            <th class="col-md-2 text-center">Appointment Date</th>
                            <th class="col-md-2 text-center">Status</th>
                            <th class="col-md-1 text-center no-sort">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php foreach($appointment_list as $appointment){
                          $color = '';
                          switch($appointment['appointment_status']){
                            case '1':
                            break;
                            case '2':
                              $color = 'warning';
                            break;
                            case '3':
                            case '4':
                              $color = 'primary';
                            break;
                            case '5':
                              $color = 'success';
                            break;
                            case '6':
                            case '7':
                            case '8':
                            case '9':
                              $color = 'danger';
                            break;
                          }
                          $status = '';
                          switch($appointment['appointment_status']){
                            case '1':
                              $status = 'Waiting for Approval';
                            break;
                            case '2':
                              $status = 'Approved. Needs Confirmation on the said appointment';
                            break;
                            case '3':
                            case '4':
                              $status = 'Confirmed Appointment';
                            break;
                            case '5':
                              $status = 'Medical Appointment / Treatment Completed ';
                            break;
                            case '6':
                            case '7':
                            case '8':
                            case '9':
                              $status = 'Cancelled';
                            break;
                          }

                          ?>
                          <tr class="<?php echo $color; ?>">
                            <td class="te"><?php echo date('M d Y - H:i A', strtotime($appointment['appointment_requested_datetime']))?></td>
                            <td><?php echo $status ?></td>
                            <td>
                              <div class="text-center">
                                <a class="btn btn-info has-tooltip" title="View Appointment" href="<?php echo DOMAIN.'appointment/view-appointment/'.$appointment['appointment_id'] ?>"><i class="fa fa-search"></i></a>
                                <button class="btn btn-danger has-tooltip delete-row" title="Delete" value=""><i class="fa fa-trash"></i></button>
                              </div>
                            </td>
                          </tr>
                        <?php }?>
                        </tbody>
                      </table>
                    </div>

                  </div>
              </div>
          </div>
    </div>

</div>
