<div class="wrapper wrapper-content">
  <div class="row">

    <div class="col-lg-10">
    </div>

    <div class="col-lg-2 text-right">
      <button class="btn btn-success dim has-tooltip form-submit" name="save" title="Resolve Complaint" data-form="main-form"><i class="fa fa-refresh"></i> RESOLVE</button>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-danger">
        <div class="panel-heading">
          <h3>View complaint</h3>
        </div>
        <div class="panel-body">
          <blockquote>
              <small>Complaint against <strong id="resident-fullname">
                <?php echo strtoupper(
                  $form_data['resident_first_name']. ' '.
                  $form_data['resident_middle_name']. ' '.
                  $form_data['resident_last_name']. ' '.
                  $form_data['resident_name_ext'])?></strong>,
                  <strong id="resident-age">
                  <?php echo date_diff(date_create($form_data['resident_birthday']), date_create('now'))->y?>
                  </strong> year/s old, <strong class="text-uppercase"><?php echo $form_data['resident_gender']?></strong>,
                  from <strong class="text-uppercase" id="resident-address">
                  <?php echo $form_data['add_desc']. " - ".$form_data['address_brgy']['brgy_name'] ?>, Batangas City, Philippines</strong>.
              </small>
          </blockquote>
          <div class="hr-line-solid"></div>

            <div class="form-group">
              <div class="col-sm-12">
                <h4>Complaint Title</h4>
                <input class="form-control text-uppercase" type="text" readonly placeholder="Complaint Title" value="<?php echo $form_data['complaint_title'] ?>">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6">
                <h4>Complaint Date</h4>
                <input type="text" readonly class="form-control" placeholder="Select Complaint Date" value="<?php echo $form_data['complaint_date_filed'] ?>">
              </div>
              <div class="col-sm-6">
                <h4>Complaint Type</h4>
                <input type="text" readonly class="form-control" value="<?php echo $form_data['complaint_type'] ?>">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <div class="panel">
                  <?php echo $form_data['complaint_description'] ?>
                </div>
              </div>
            </div>
        </div>
      </div>


    </div>
  </div>


</div>
<script src="<?php echo JS_DIR ?>components/resident/resident_profile.js"></script>
