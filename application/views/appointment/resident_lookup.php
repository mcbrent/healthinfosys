<div class="modal inmodal fade" id="resident-lookup" role="dialog" data-no="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Select Resident</h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="form-group">
                  <small id="search-status" class="text-danger"></small>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <h5>Resident Search</h5>
                    <select class="form-control select2-resident-key" data-placeholder="Enter Resident Name / No" style="width: 100%">
                    </select>

                  </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-ban"></i> Cancel</button>
                <button type="button" id="modal-select-resident" data-dismiss="modal" class="btn btn-primary" value="add"><i class="fa fa-floppy-o"></i> Select Resident</button>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
  $('.select2-resident-key').select2({
      allowClear: true,
      ajax:{
        url: global.site_name + 'resident/resident_lookup',
        dataType: 'json',
        type:'post',
        data: function(params){

            return params;
        },
        processResults: function (data) {
        // Tranforms the top-level key of the response object from 'items' to 'results'
        console.log(data)
          let result = [];
          data.forEach(function(d){
            result.push({
              id  : d.resident_id
            , text: d.resident_no + ' - ' + d.resident
            });
          });
          return {
            results: result
          }
        }
      }
    });
})
</script>
