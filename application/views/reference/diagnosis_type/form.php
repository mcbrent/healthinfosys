<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
    <div class="col-lg-12 text-right">
      <button class="btn btn-primary has-tooltip form-submit" title="Save" data-form="main-form"><i class="fa fa-file"></i></button>
    </div>
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5><?php echo ucfirst($action) ?> Diagnosis Type</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="fullscreen-link">
                        <i class="fa fa-expand"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
              <form id="main-form" action="" method="POST" class="form-horizontal">
                  <div class="form-group">
                      <div class="col-sm-12">
                        <h5>Diagnosis Code</h5>
                        <input type="text" name="dt_code" class="form-control" placeholder="Diagnosis Code" value="<?php echo $form_data['dt_code']?>">
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-sm-12">
                        <h5>Diagnosis Type</h5>
                        <input type="text" name="dt_name" class="form-control" placeholder="Diagnosis Type" value="<?php echo $form_data['dt_name']?>">
                      </div>
                  </div>
            </div>
        </div>
    </div>
  </div>


</div>
