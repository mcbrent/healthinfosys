<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
      <div class="col-lg-12">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h5>Supplies Allocation History</h5>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table id="supplies-list" class="table table-striped table-bordered table-hover" data-config="{}">
                  <thead>
                    <tr>
                      <th class="col-md-2 text-center">Date</th>
                      <th class="col-md-3 text-center">Given By</th>
                      <th class="col-md-3 text-center">Given To</th>
                      <th class="col-md-1 text-center no-sort">Action</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>

          </div>
    </div>

</div>

<script src="<?php echo JS_DIR ?>components/supplies/allocation_history.js"></script>
