<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
    <div class="col-lg-12 text-right">
      <button id="allocate-supplies" class="btn btn-primary has-tooltip" type="button" name="save" title="Save"><i class="fa fa-file"></i></button>
    </div>
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3><?php echo ucfirst($action) ?> Allocate Supplies</h3>
        </div>
        <div class="panel-body">
          <form id="main-form" class="form-horizontal" method="POST" action="?">
            <div class="form-group">
              <div class="col-md-3">
                <label>User Type</label>
                <select id="user-type" class="form-control select2-basic">
                  <option value="1">Administrator</option>
                  <option value="2" selected>Resident</option>
                  <option value="3">Pharmacist</option>
                  <option value="4">Medical Officer</option>
                  <option value="5">Barangay Health Workers</option>
                </select>
              </div>
              <div class="col-md-9">
                <label class="type-description">Resident</label>
                <select name="user-id" class="form-control">
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12">
                <label>Notes</label>
                <textarea name="sa-summary" class="form-control" maxlength="512"></textarea>
              </div>
            </div>



            <div class="panel panel-success">
              <div class="panel-heading">
                <h5>Supplies</h5>
              </div>
              <div class="panel-body">
                  <input type="hidden" name="supplies-allocation-detail-json" value="">
                  <table id="supplies-allocation-detail" class="table table-striped table-bordered" data-content="[]">
                    <thead>
                      <tr>
                        <th class="col-md-8">Medical Supply Description</th>
                        <th class="col-md-3">Quantity</th>
                        <th class="col-md-1">Action</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot>
                      <td class="col-md-12 text-center gray-bg" colspan="4">
                        <button type="button" id="add-detail" class="btn btn-primary btn-sm add-attachment"><i class="fa fa-plus"></i> Add More Detail</button>
                      </td>
                    </tfoot>
                  </table>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo JS_DIR ?>components/supplies/allocate_supplies.js"></script>
