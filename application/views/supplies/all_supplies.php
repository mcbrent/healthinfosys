<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
      <div class="col-lg-12">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <div class="row">
                <div class="col-md-11">
                  <h5>Supplies List</h5>
                </div>
                <div class="col-md-1 text-right">
                  <button id="generate-excel" class="btn btn-primary has-tooltip" title="Generate Excel"><i class="fa fa-file-excel-o"></i></button>
                </div>
              </div>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table id="supplies-list" class="table table-striped table-bordered table-hover" data-config="{}">
                  <thead>
                    <tr>
                      <th class="col-md-7 text-center">Medical Supplies</th>
                      <th class="col-md-4 text-center">Current Stock</th>
                      <th class="col-md-1 text-center no-sort">Action</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>

          </div>
    </div>

</div>

<script src="<?php echo JS_DIR ?>components/supplies/all_supplies.js"></script>
