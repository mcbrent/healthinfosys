<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
    <div class="col-lg-12 text-right">
      <button class="btn btn-primary has-tooltip form-submit" name="save" title="Save" data-form="main-form"><i class="fa fa-file"></i></button>
    </div>
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3><?php echo ucfirst($action) ?> Supplies</h3>
        </div>
        <div class="panel-body">
          <form id="main-form" class="form-horizontal" method="POST" action="?">
            <div class="form-group">
              <label class="col-md-12">Supplies Description</label>
              <div class="col-md-12">
                <input type="text" name="ms-description" class="form-control" placeholder="Supplies Description" value="<?php echo $form_data['ms_description'] ?>">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3">
                <label>Supplies Actual Stock</label>
                <input type="number" name="ms-actual-stock" class="form-control" placeholder="Supplies Actual Stock" value="<?php echo $form_data['ms_actual_stock'] ?>">
              </div>
              <div class="col-md-3">
                <label>Supplies Stock Unit</label>
                <input type="text" name="ms-unit" class="form-control" placeholder="Supplies Stock Unit" value="<?php echo $form_data['ms_unit'] ?>">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
