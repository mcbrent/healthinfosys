<script src='https://www.google.com/recaptcha/api.js?render=6Ld1iHwUAAAAAJUlXa7GoHLsax5QOVAh89Yq8EIH'></script>
<div class="wrapper wrapper-content">
    <div class="container white-bg">
        <div class="row">
            <div class="col-lg-12">
                <h2>Register</h2>
                <hr class="hr-line-solid">
                <form id="main-form" action="" method="POST">
                    <div class="form-group col-md-12">
                        <label>Email</label>
                        <input type="email" name="reg-email" placeholder="Enter Email" class="form-control">
                    </div>

                    <div class="form-group">
                      <h4 class="col-sm-12">Name</h4>
                      <div class="col-sm-4"><input name="resident-first-name" type="text" class="form-control" placeholder="First Name"></div>
                      <div class="col-sm-4"><input name="resident-middle-name" type="text" class="form-control" placeholder="Middle Name"></div>
                      <div class="col-sm-4"><input name="resident-last-name" type="text" class="form-control" placeholder="Last Name"></div>

                    </div>
                    <div class="form-group col-md-12">
                        <label>Username</label>
                        <input type="username" name="reg-username" placeholder="Enter Username" class="form-control">
                    </div>
                    <div class="form-group col-md-12">
                        <label>Password</label>
                        <input type="password" name="reg-password" placeholder="Enter Password" class="form-control">
                    </div>
                    <div class="form-group col-md-12">
                        <label>Retype Password</label>
                        <input type="password" name="reg-re-password" placeholder="Reenter Password" class="form-control">
                    </div>
                    <div class="form-group col-md-12">
                      <div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="6LcAc3EUAAAAAFH489r8o8RhDeQ_xRCfR8IHeD0F"></div>
                    </div>
                </form>

            </div>
            <div class="col-lg-12">
              <div class="form-group">
                  <div class="col-sm-4 col-sm-offset-4">
                      <button disabled class="btn btn-primary form-submit col-sm-12 btn-outline" data-form="main-form">
                        <h3 class="font-bold"><i class="fa fa-user-o"></i> Register</h3>
                      </button>
                  </div>
              </div>
            </div>

        </div>

    </div>

</div>
<script>
grecaptcha.ready(function() {
grecaptcha.execute('6Ld1iHwUAAAAAJUlXa7GoHLsax5QOVAh89Yq8EIH', {action: 'action_name'})
.then(function(token) {
  $('.form-submit').removeAttr('disabled');
// Verify the token on the server.
});
});
</script>
<script>

$(document).ready(function(){
  function generateUsername(){
    let uname = (($('[name=resident-first-name]').val()).charAt(0) + $('[name=resident-last-name]').val()).toLowerCase();

    $('[name=reg-username]').val(uname)
  }

  $('[name=resident-first-name]').change(function(){
    generateUsername();
  });
  $('[name=resident-last-name]').change(function(){
    generateUsername();
  });

  $("#main-form").validate({
    rules :{
      'resident-first-name':{
        required  : true
      },
      'resident-last-name':{
        required  : true
      },
      'reg-email': {
        required : true,
        email : true
      },
      'reg-username':{
        required : true,
        minlength: 6
      },
      'reg-password':{
        required : true,
        minlength: 6
      },
      'reg-re-password':{
        required : true,
        equalTo : "[name=reg-password]"
      }

    }

  });
});
</script>
