<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
    <div class="col-lg-12 text-right">
      <button class="btn btn-primary has-tooltip form-submit" name="save" title="Save" data-form="main-form"><i class="fa fa-file"></i></button>
    </div>
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5><?php echo ucfirst($action) ?> Barangay Health Worker</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="fullscreen-link">
                        <i class="fa fa-expand"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
              <form id="main-form" method="POST" data-action="<?php echo $action ?>" class="form-horizontal" action="?">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <h3>Designated Barangay</h3>
                      <select class="form-control" name="designated-barangay" data-placeholder="Select Barangay">
                          <option></option>
                          <?php if($form_data['assign_brgy']['brgy_id']){ ?>
                          <option value="<?php echo $form_data['assign_brgy']['brgy_id'] ?>" selected><?php echo $form_data['assign_brgy']['brgy_name'] ?></option>
                          <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group">
                      <label class="col-sm-12">First Name, Middle Name, Last Name, Name Extension</label>
                      <div class="col-sm-3"><input name="admin-first-name" type="text" class="form-control" placeholder="First Name" value="<?php echo $form_data['admin_first_name'] ?>"></div>
                      <div class="col-sm-3"><input name="admin-middle-name" type="text" class="form-control" placeholder="Middle Name" value="<?php echo $form_data['admin_middle_name'] ?>"></div>
                      <div class="col-sm-3"><input name="admin-last-name" type="text" class="form-control" placeholder="Last Name" value="<?php echo $form_data['admin_last_name'] ?>"></div>
                      <div class="col-sm-3"><input name="admin-name-ext" type="text" class="form-control" placeholder="Name Extension" value="<?php echo $form_data['admin_name_ext'] ?>"></div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-12">Gender</label>
                    <div class="col-sm-12">
                      <label class="checkbox-inline i-checks"> <input type="radio" value="male" name="gender" <?php echo $form_data['admin_gender'] == 'male'? 'checked': '' ?>> <i></i> Male </label>
                      <label class="checkbox-inline i-checks"> <input type="radio" value="female" name="gender" <?php echo $form_data['admin_gender'] == 'female'? 'checked': '' ?>> <i></i> Female</label>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-3">
                      <label>Birthday</label>
                      <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="bc_birthdate" class="form-control datepicker" value="<?php echo $form_data['bc_birthdate'] ?>">
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <label>Contact Number 1</label>
                      <input name="bc_phone_num1" type="text" class="form-control" placeholder="Contact Number 1" value="<?php echo $form_data['bc_phone_num1'] ?>">
                    </div>
                    <div class="col-sm-3">
                      <label>Contact Number 2</label>
                      <input name="bc_phone_num2" type="text" class="form-control" placeholder="Contact Number 2" value="<?php echo $form_data['bc_phone_num2'] ?>">
                    </div>
                    <div class="col-sm-3">
                      <label>Contact Number 3</label>
                      <input name="bc_phone_num3" type="text" class="form-control" placeholder="Contact Number 3" value="<?php echo $form_data['bc_phone_num3'] ?>">
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group">
                      <label class="col-sm-12">Email</label>
                      <div class="col-lg-12">
                        <input type="email" name="user-email" placeholder="Email" class="form-control" value="<?php echo $form_data['admin_email'] ?>">
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-lg-12">Username</label>
                      <div class="col-lg-12">
                        <input type="text" name="user-name" placeholder="User name" class="form-control" value="<?php echo $form_data['user_name'] ?>">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-lg-12">Password</label>
                    <div class="col-lg-12">
                      <input type="password" name="user-password" placeholder="Password" class="form-control" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-lg-12">Retype Password</label>
                    <div class="col-lg-12">
                      <input type="password" name="user-repassword" placeholder="Retype Password" class="form-control">
                    </div>
                  </div>

                  <div class="hr-line-dashed"></div>
                </form>
            </div>
        </div>
    </div>
  </div>


</div>
<script src="<?php echo JS_DIR ?>components/user/add_bhws.js"></script>
