<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
    <div class="col-lg-12">
      <h3><?php echo ucfirst($action) ?> Pharmacist</h3>
    </div>
    <div class="col-lg-12 text-right">
      <button class="btn btn-primary has-tooltip form-submit" name="save" title="Save" data-form="main-form"><i class="fa fa-file"></i></button>
    </div>
    <div class="col-lg-12">
        <div class="tabs-container">
          <div class="tabs">
            <ul class="nav nav-tabs">
                <li class="active has-tooltip" title="Basic Information"><a data-toggle="tab" href="#tab-basic-info"> <i class="fa fa-address-card"></i></a></li>
                <li class="has-tooltip" title="Additional Info" ><a data-toggle="tab"href="#tab-additional-info"><i class="fa fa-folder"></i></a></li>
                <li class="has-tooltip" title="Qr Code"><a data-toggle="tab" href="#tab-qrcode"><i class="fa fa-qrcode"></i></a></li>
            </ul>
            <form id="main-form" method="POST" action="?">
              <div class="tab-content">
                  <div id="tab-basic-info" class="tab-pane active">
                    <div class="panel-body form-horizontal">
                      <div class="form-group">
                          <label class="col-sm-12">First Name, Middle Name, Last Name, Name Extension</label>
                          <div class="col-sm-3"><input name="pharmacist-first-name" type="text" class="form-control" placeholder="First Name" value="<?php echo $form_data['pharmacist_first_name'] ?>"></div>
                          <div class="col-sm-3"><input name="pharmacist-middle-name" type="text" class="form-control" placeholder="Middle Name" value="<?php echo $form_data['pharmacist_middle_name'] ?>"></div>
                          <div class="col-sm-3"><input name="pharmacist-last-name" type="text" class="form-control" placeholder="Last Name" value="<?php echo $form_data['pharmacist_last_name'] ?>"></div>
                          <div class="col-sm-3"><input name="pharmacist-name-ext" type="text" class="form-control" placeholder="Name Extension" value="<?php echo $form_data['pharmacist_name_ext'] ?>"></div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-12">Gender</label>
                        <div class="col-sm-12">
                          <label class="checkbox-inline i-checks"> <input type="radio" value="male" name="gender" <?php echo $form_data['pharmacist_gender'] == 'male'? 'checked': '' ?>> <i></i> Male </label>
                          <label class="checkbox-inline i-checks"> <input type="radio" value="female" name="gender" <?php echo $form_data['pharmacist_gender'] == 'female'? 'checked': '' ?>> <i></i> Female</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-3">
                          <label>Birthday</label>
                          <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="bc_birthdate" class="form-control datepicker" value="<?php echo $form_data['bc_birthdate'] ?>">
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <label>Contact Number 1</label>
                          <input name="bc_phone_num1" type="text" class="form-control" placeholder="Contact Number 1" value="<?php echo $form_data['bc_phone_num1'] ?>">
                        </div>
                        <div class="col-sm-3">
                          <label>Contact Number 2</label>
                          <input name="bc_phone_num2" type="text" class="form-control" placeholder="Contact Number 2" value="<?php echo $form_data['bc_phone_num2'] ?>">
                        </div>
                        <div class="col-sm-3">
                          <label>Contact Number 3</label>
                          <input name="bc_phone_num3" type="text" class="form-control" placeholder="Contact Number 3" value="<?php echo $form_data['bc_phone_num3'] ?>">
                        </div>
                      </div>
                      <div class="hr-line-dashed"></div>
                      <div class="form-group">
                          <label class="col-sm-12">Email</label>
                          <div class="col-lg-12">
                            <input type="email" name="user-email" placeholder="Email" class="form-control" value="<?php echo $form_data['pharmacist_email'] ?>">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-lg-12">Username</label>
                          <div class="col-lg-12">
                            <input type="text" name="user-name" placeholder="User name" class="form-control" value="<?php echo $form_data['user_name'] ?>">
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-12">Password</label>
                        <div class="col-lg-12">
                          <input type="password" name="user-password" placeholder="Password" class="form-control" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-12">Retype Password</label>
                        <div class="col-lg-12">
                          <input type="password" name="user-repassword" placeholder="Retype Password" class="form-control">
                        </div>
                      </div>

                      <div class="hr-line-dashed"></div>
                    </div>
                  </div>
                  <div id="tab-additional-info" class="tab-pane">
                      <div class="panel-body form-horizontal">
                          <h4>Additional Info</h4>
                          <div class="form-group">
                            <div class="col-sm-12">
                              <textarea name="pharmacist-additional-info" class="summernote">
                                <?php echo $form_data['pharmacist_add_info'] ?>
                              </textarea>
                            </div>
                          </div>
                      </div>
                  </div>
                  <div id="tab-qrcode" class="tab-pane">
                      <div class="panel-body">
                        <div class="row">
                          <div class="col-md-12 text-center">
                            <img class="img-responsive" src="<?php echo UPLOAD?>qrcode/<?php echo $form_data['qrcode_url'] ?>" alt="QR Code here" style="margin: 0 auto; width: 345px"/>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>


    </div>
  </div>


</div>
<script src="<?php echo JS_DIR ?>components/user/add_pharmacist.js"></script>
