<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
      <div class="col-lg-12">
        <h4>Morbidity Report</h4>
          <div class="panel panel-primary">
            <div class="panel-heading">
              <div class="row">
                <div class="col-md-11">
                  <h5>Filter</h5>
                </div>
              </div>
            </div>
            <div class="panel-body">
              <form method="GET" action="">
                <div class="row">
                  <div class="col-md-4">
                    <h4>Date From</h4>
                    <input type="text" class="form-control datepicker" name="date-from" autocomplete="off" value="<?php echo date('m/d/Y', strtotime("-6 months")) ?>">
                    <h4>Date To</h4>
                    <input type="text" class="form-control datepicker" name="date-to" autocomplete="off" value="<?php echo date('m/d/Y') ?>">
                  </div>
                  <div class="col-md-4">
                      <h4>Barangay</h4>
                      <select class="form-control select2-basic" name="barangay-filter" data-placeholder="Barangay Filter">
                          <option value="all">All Barangay</option>
                          <?php foreach($barangay_list as $barangay){ ?>
                              <option value="<?php echo $barangay['brgy_id']?>" <?php echo ($barangay['brgy_id'] == $params['barangay-filter'])? 'selected' : '' ?>><?php echo $barangay['brgy_name']?></option>
                          <?php }?>
                      </select>
                  </div>
                  <div class="col-md-4">
                    <h4>&nbsp;</h4>
                    <button type="submit" class="btn btn-block btn-success">Generate Report</button>
                  </div>
                </div>
              </form>
            </div>
          </div>

      </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
          <div class="ibox-title">
              <h5>Morbidity Report</h5>
          </div>
          <div class="ibox-content">
              <div>
                  <canvas id="barChart" height="140"></canvas>
              </div>
          </div>
      </div>
    </div>
  </div>

</div>

<script>
$(function () {

    // var lineData = {
    //     labels: ["January", "February", "March", "April", "May", "June", "July"],
    //     datasets: [
    //
    //         {
    //             label: "Data 1",
    //             backgroundColor: 'rgba(26,179,148,0.5)',
    //             borderColor: "rgba(26,179,148,0.7)",
    //             pointBackgroundColor: "rgba(26,179,148,1)",
    //             pointBorderColor: "#fff",
    //             data: [28, 48, 40, 19, 86, 27, 90]
    //         },{
    //             label: "Data 2",
    //             backgroundColor: 'rgba(220, 220, 220, 0.5)',
    //             pointBorderColor: "#fff",
    //             data: [65, 59, 80, 81, 56, 55, 40]
    //         }
    //     ]
    // };
    //
    // var lineOptions = {
    //     responsive: true
    // };
    //
    //
    // var ctx = document.getElementById("lineChart").getContext("2d");
    // new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
<?php if($bar_graph){ ?>
    var barData = {
        labels: <?php echo json_encode($bar_graph['label']) ?>,
        datasets: [
            {
                label: "Data 1",
                backgroundColor: 'rgba(26,179,148,0.5)',
                borderColor: "rgba(26,179,148,0.7)",
                pointBackgroundColor: "rgba(26,179,148,1)",
                pointBorderColor: "#fff",
                data: <?php echo json_encode($bar_graph['value']) ?>
            }
            //,
            //{
            //    label: "Data 2",
            //    backgroundColor: 'rgba(26,179,148,0.5)',
            //    borderColor: "rgba(26,179,148,0.7)",
            //    pointBackgroundColor: "rgba(26,179,148,1)",
            //    pointBorderColor: "#fff",
            //    data: [28, 48, 40, 19, 86, 27, 90]
            //}
        ]
    };

    var barOptions = {
        responsive: true,
        scales: {
            yAxes: [{
                display: true,
                ticks: {
                    beginAtZero: true,
                    min: 0
                }
            }]
        },
    };
    var ctx2 = document.getElementById("barChart").getContext("2d");
    new Chart(ctx2, {type: 'bar', data: barData, options:barOptions});
<?php }?>
    // var polarData = {
    //     datasets: [{
    //         data: [
    //             300,140,200
    //         ],
    //         backgroundColor: [
    //             "#a3e1d4", "#dedede", "#b5b8cf"
    //         ],
    //         label: [
    //             "My Radar chart"
    //         ]
    //     }],
    //     labels: [
    //         "App","Software","Laptop"
    //     ]
    // };
    //
    // var polarOptions = {
    //     segmentStrokeWidth: 2,
    //     responsive: true
    //
    // };
    //
    // var ctx3 = document.getElementById("polarChart").getContext("2d");
    // new Chart(ctx3, {type: 'polarArea', data: polarData, options:polarOptions});
    //
    // var doughnutData = {
    //     labels: ["App","Software","Laptop" ],
    //     datasets: [{
    //         data: [300,50,100],
    //         backgroundColor: ["#a3e1d4","#dedede","#b5b8cf"]
    //     }]
    // } ;
    //
    //
    // var doughnutOptions = {
    //     responsive: true
    // };
    //
    //
    // var ctx4 = document.getElementById("doughnutChart").getContext("2d");
    // new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});
    //
    //
    // var radarData = {
    //     labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],
    //     datasets: [
    //         {
    //             label: "My First dataset",
    //             backgroundColor: "rgba(220,220,220,0.2)",
    //             borderColor: "rgba(220,220,220,1)",
    //             data: [65, 59, 90, 81, 56, 55, 40]
    //         },
    //         {
    //             label: "My Second dataset",
    //             backgroundColor: "rgba(26,179,148,0.2)",
    //             borderColor: "rgba(26,179,148,1)",
    //             data: [28, 48, 40, 19, 96, 27, 100]
    //         }
    //     ]
    // };
    //
    // var radarOptions = {
    //     responsive: true
    // };
    //
    // var ctx5 = document.getElementById("radarChart").getContext("2d");
    // new Chart(ctx5, {type: 'radar', data: radarData, options:radarOptions});

});
</script>
