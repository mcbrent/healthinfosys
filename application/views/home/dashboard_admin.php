<input type="hidden" id="user-id" value="<?php echo $user_id ?>">
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-lg-12">
      <div id="reg-app" class="ibox float-e-margins">
          <div class="ibox-title">
              <span class="label label-success pull-right">As of Today</span>
              <a href="<?php echo DOMAIN ?>appointment/request-appointment-list"><h5><i class="fa fa-stethoscope"></i> Appointments Today</h5></a>
          </div>
          <div class="ibox-content">
              <div class="row">
                <div class="col-md-4 text-center">
                  <h4 id="scheduled">0</h4>
                  <h5>Scheduled</h5>
                </div>
                <div class="col-md-4 text-center">
                  <h4 id="waiting">0</h4>
                  <h5>Waiting</h5>
                </div>
                <div class="col-md-4 text-center">
                  <h4 id="completed">0</h4>
                  <h5>Completed</h5>
                </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo JS_DIR ?>components/home/dashboard_admin.js"></script>
