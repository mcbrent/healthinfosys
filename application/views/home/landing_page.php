<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Taysan Rural Health Unit - Health Information System</title>
    <meta name="description" content="Web Based Health Information System - Taysan Batangas">
    <meta name="keywords" content="Web Based Health Information System, healthinfosys, Taysan Batangas">

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal">
    <link rel="stylesheet" type="text/css" href="<?php echo CSS_DIR; ?>font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo CSS_DIR; ?>bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo CSS_DIR; ?>style_landing_page.css">
  </head>

  <body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
    <!--banner-->
    <section id="banner" class="banner">
      <div class="bg-color">
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container">
            <div class="col-md-12">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
  				        <span class="icon-bar"></span>
  				        <span class="icon-bar"></span>
  				        <span class="icon-bar"></span>
  				      </button>
                <!-- <a class="navbar-brand" href="#"><img src="<?php echo IMG_DIR ?>logo.png" class="img-responsive" style="width: 140px; margin-top: -16px;"></a> -->
              </div>
              <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                <ul class="nav navbar-nav">
                  <li class="active"><a href="#banner">Home</a></li>
                  <li class=""><a href="#mission">Mission</a></li>
                  <li class=""><a href="#vision">Vision</a></li>
                  <li class=""><a href="#description">Description</a></li>
                  <li class=""><a href="#announcement">Annoucement</a></li>
                  <li class=""><a href="#calendar-activities">Calendar Activities</a></li>
                  <li class=""><a href="#gallery">Gallery</a></li>
                  <li class=""><a href="<?php echo DOMAIN ?>login">login</a></li>
                </ul>
              </div>
            </div>
          </div>
        </nav>
        <div class="container">
          <div class="row">
            <div class="banner-info">
              <div class="banner-logo text-center">
                <!-- <img src="<?php echo IMG_DIR ?>logo.png" class="img-responsive"> -->
              </div>
              <div class="banner-text text-center">
                  <img src="<?php echo IMG_DIR?>Taysan_Seal.png" style="height:20vh">
                  <h1 class="white">Taysan Health Unit - Health Information System</h1>
                  <p>Republic of the Philippines<br>Province of Batangas<br>Taysan, Batangas</p>
                  <br>

                  <?php /*
                <h1 class="white">Taysan Health Unit - Health Information System</h1>
                <p>A Health unit based on Taysan Batangas<br></p>
                */ ?>
                <!-- <a href="#contact" class="btn btn-appoint">Make an Appointment.</a> -->
              </div>
              <div class="overlay-detail text-center">
                <a href="#gallery"><i class="fa fa-angle-down"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--/ banner-->
    <!--mission-->
    <section id="mission" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <h2 class="ser-title">Our Mission</h2>
            <hr class="botm-line">
            <p>Taysan Batangas, a community of healthy citizens by ensuring available and accessible health services to the people, through a dedicated and compassionate health force, and a strong and committed leadership of LGU officials.</p>
          </div>
        </div>
      </div>
    </section>
    <!--/ mission-->
    <!--vision-->
    <section id="vision" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <h2 class="ser-title">Our Vision</h2>
            <hr class="botm-line">
            <p>Taysan Batangas, a home strong and healthy citizens by working together for a healthy and progressive community</p>
          </div>
        </div>
      </div>
    </section>
    <!--/ vision-->
    <!--description-->
    <section id="description" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-4">
            <h2 class="ser-title">Description</h2>
            <hr class="botm-line">
            <p>Its supports the barangay health station and rural health unit that is devoted to providing primary health care service of patients</p>
            <p>Provides a systematic way to manage patient records and generates standardized reporting requirements.</p>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="service-info">
              <div class="icon">
                <i class="fa fa-stethoscope"></i>
              </div>
              <div class="icon-info">
                <h4>Efficiency Gains</h4>
                <p>- Enhanced health workforce productivity in retrieving patient information, record keeping, administration and referrals</p>
              </div>
            </div>
            <div class="service-info">
              <div class="icon">
                <i class="fa fa-ambulance"></i>
              </div>
              <div class="icon-info">
                <h4>Improved Quality of Care</h4>
                <p>- Reduced instances of medically avoidable adverse events</p>
                <p>- Improved ability to monitor compliance to medications and other treatment regimes</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="service-info">
              <div class="icon">
                <i class="fa fa-user-md"></i>
              </div>
              <div class="icon-info">
                <h4>Improved Operations Planning and Management</h4>
                <p>- Improved access to quality data to inform healthcare service and workforce planning and development</p>
              </div>
            </div>
            <div class="service-info">
              <div class="icon">
                <i class="fa fa-medkit"></i>
              </div>
              <div class="icon-info">
                <h4>Improved Health Monitoring & Reporting</h4>
                <p>- Track patients’ data over time</p>
                <p>- Monitor how patients measure up to certain parameters like vital signs (e.g. blood pressure readings, respiratory rate readings), vaccinations, and others</p>
                <p>- Identify patients who are scheduled for visits</p>
                <p>- Improved ability to support surveillance and management of public health interventions</p>
                <p>- Improved ability to report and analyze health outcomes</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--/ description-->
    <!--announcement-->
    <section id="announcement" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <h2 class="ser-title">Annoucement</h2>
            <hr class="botm-line">
          </div>
          <div class="col-md-12 col-sm-12">
              <div class="row">
                  <?php foreach($annoucement_list as $annoucement){ ?>
                      <div class="col-md-12">
                          <div class="panel panel-info">
                              <div class="panel-body">
                                  <small class="small"><?php echo date('F d, Y - g:i A', strtotime($annoucement['announcement_date'])) ?></small>
                                  <br>
                                  <div><?php echo $annoucement['announcement_content'] ?></div>
                              </div>
                          </div>
                      </div>
                  <?php }?>
              </div>
          </div>
        </div>
      </div>
    </section>
    <!--/ announcement-->
    <!--calendar-activities-->
    <section id="calendar-activities" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2 class="ser-title">Calendar Of Activities</h2>
            <hr class="botm-line">
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4">
            <div class="panel panel-info">
              <div class="panel-heading text-center">
                <span class="fa fa-calendar"></span> January
              </div>
              <div class="panel-body text-center">
                <h3><?php echo $act_jan ?></h3>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="panel panel-primary">
              <div class="panel-heading text-center">
                <span class="fa fa-calendar"></span> February
              </div>
              <div class="panel-body text-center">
                <h3><?php echo $act_feb ?></h3>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="panel panel-info">
              <div class="panel-heading text-center">
                <span class="fa fa-calendar"></span> March
              </div>
              <div class="panel-body text-center">
                <h3><?php echo $act_mar ?></h3>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="panel panel-primary">
              <div class="panel-heading text-center">
                <span class="fa fa-calendar"></span> April
              </div>
              <div class="panel-body text-center">
                <h3><?php echo $act_apr ?></h3>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="panel panel-info">
              <div class="panel-heading text-center">
                <span class="fa fa-calendar"></span> May
              </div>
              <div class="panel-body text-center">
                <h3><?php echo $act_may ?></h3>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="panel panel-primary">
              <div class="panel-heading text-center">
                <span class="fa fa-calendar"></span> June
              </div>
              <div class="panel-body text-center">
                <h3><?php echo $act_jun ?></h3>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="panel panel-info">
              <div class="panel-heading text-center">
                <span class="fa fa-calendar"></span> July
              </div>
              <div class="panel-body text-center">
                <h3><?php echo $act_jul ?></h3>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="panel panel-primary">
              <div class="panel-heading text-center">
                <span class="fa fa-calendar"></span> August
              </div>
              <div class="panel-body text-center">
                <h3><?php echo $act_aug ?></h3>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="panel panel-info">
              <div class="panel-heading text-center">
                <span class="fa fa-calendar"></span> September
              </div>
              <div class="panel-body text-center">
                <h3><?php echo $act_sep ?></h3>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="panel panel-primary">
              <div class="panel-heading text-center">
                <span class="fa fa-calendar"></span> October
              </div>
              <div class="panel-body text-center">
                <h3><?php echo $act_oct ?></h3>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="panel panel-info">
              <div class="panel-heading text-center">
                <span class="fa fa-calendar"></span> November
              </div>
              <div class="panel-body text-center">
                <h3><?php echo $act_nov ?></h3>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="panel panel-primary">
              <div class="panel-heading text-center">
                <span class="fa fa-calendar"></span> December
              </div>
              <div class="panel-body text-center">
                <h3><?php echo $act_dec ?></h3>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>
    <!--/ calendar-activities -->

    <!--gallery-->
    <section id="gallery" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2 class="ser-title">What We Do?</h2>
            <hr class="botm-line">
          </div>
          <div class="col-md-12">
            <div class="container">
              <div id="main_area">
                  <!-- Slider -->
                  <div class="row">
                      <div class="col-sm-6" id="slider-thumbs">
                          <!-- Bottom switcher of slider -->
                          <ul class="hide-bullets">
                              <li class="col-sm-3">
                                  <a class="thumbnail" id="carousel-selector-0">
                                      <img src="<?php echo IMG_DIR?>gallery-1.jpg">
                                  </a>
                              </li>
                              <li class="col-sm-3">
                                  <a class="thumbnail" id="carousel-selector-1"><img src="<?php echo IMG_DIR?>gallery-2.jpg"></a>
                              </li>

                              <li class="col-sm-3">
                                  <a class="thumbnail" id="carousel-selector-2"><img src="<?php echo IMG_DIR?>gallery-3.jpg"></a>
                              </li>

                              <li class="col-sm-3">
                                  <a class="thumbnail" id="carousel-selector-3"><img src="<?php echo IMG_DIR?>gallery-4.jpg"></a>
                              </li>

                              <li class="col-sm-3">
                                  <a class="thumbnail" id="carousel-selector-4"><img src="<?php echo IMG_DIR?>gallery-5.jpg"></a>
                              </li>

                              <li class="col-sm-3">
                                  <a class="thumbnail" id="carousel-selector-5"><img src="<?php echo IMG_DIR?>gallery-6.jpg"></a>
                              </li>
                              <li class="col-sm-3">
                                  <a class="thumbnail" id="carousel-selector-6"><img src="<?php echo IMG_DIR?>gallery-7.jpg"></a>
                              </li>

                              <li class="col-sm-3">
                                  <a class="thumbnail" id="carousel-selector-7"><img src="<?php echo IMG_DIR?>gallery-8.jpg"></a>
                              </li>

                              <li class="col-sm-3">
                                  <a class="thumbnail" id="carousel-selector-8"><img src="<?php echo IMG_DIR?>gallery-9.jpg"></a>
                              </li>

                              <li class="col-sm-3">
                                  <a class="thumbnail" id="carousel-selector-9"><img src="<?php echo IMG_DIR?>gallery-10.jpg"></a>
                              </li>
                              <li class="col-sm-3">
                                  <a class="thumbnail" id="carousel-selector-10"><img src="<?php echo IMG_DIR?>gallery-11.jpg"></a>
                              </li>

                              <li class="col-sm-3">
                                  <a class="thumbnail" id="carousel-selector-11"><img src="<?php echo IMG_DIR?>gallery-12.jpg"></a>
                              </li>

                              <li class="col-sm-3">
                                  <a class="thumbnail" id="carousel-selector-12"><img src="<?php echo IMG_DIR?>gallery-13.jpg"></a>
                              </li>

                              <li class="col-sm-3">
                                  <a class="thumbnail" id="carousel-selector-13"><img src="<?php echo IMG_DIR?>gallery-14.jpg"></a>
                              </li>
                              <li class="col-sm-3">
                                  <a class="thumbnail" id="carousel-selector-14"><img src="<?php echo IMG_DIR?>gallery-15.jpg"></a>
                              </li>

                              <li class="col-sm-3">
                                  <a class="thumbnail" id="carousel-selector-15"><img src="<?php echo IMG_DIR?>gallery-16.jpg"></a>
                              </li>
                          </ul>
                      </div>
                      <div class="col-sm-6">
                          <div class="col-xs-12" id="slider">
                              <!-- Top part of the slider -->
                              <div class="row">
                                  <div class="col-sm-12" id="carousel-bounding-box">
                                      <div class="carousel slide" id="myCarousel">
                                          <!-- Carousel items -->
                                          <div class="carousel-inner">
                                              <div class="active item" data-slide-number="0">
                                                  <img src="<?php echo IMG_DIR?>gallery-1.jpg"></div>

                                              <div class="item" data-slide-number="1">
                                                  <img src="<?php echo IMG_DIR?>gallery-2.jpg"></div>

                                              <div class="item" data-slide-number="2">
                                                  <img src="<?php echo IMG_DIR?>gallery-3.jpg"></div>

                                              <div class="item" data-slide-number="3">
                                                  <img src="<?php echo IMG_DIR?>gallery-4.jpg"></div>

                                              <div class="item" data-slide-number="4">
                                                  <img src="<?php echo IMG_DIR?>gallery-5.jpg"></div>

                                              <div class="item" data-slide-number="5">
                                                  <img src="<?php echo IMG_DIR?>gallery-6.jpg"></div>

                                              <div class="item" data-slide-number="6">
                                                  <img src="<?php echo IMG_DIR?>gallery-7.jpg"></div>

                                              <div class="item" data-slide-number="7">
                                                  <img src="<?php echo IMG_DIR?>gallery-8.jpg"></div>

                                              <div class="item" data-slide-number="8">
                                                  <img src="<?php echo IMG_DIR?>gallery-9.jpg"></div>

                                              <div class="item" data-slide-number="9">
                                                  <img src="<?php echo IMG_DIR?>gallery-10.jpg"></div>

                                              <div class="item" data-slide-number="10">
                                                  <img src="<?php echo IMG_DIR?>gallery-11.jpg"></div>

                                              <div class="item" data-slide-number="11">
                                                  <img src="<?php echo IMG_DIR?>gallery-12.jpg"></div>

                                              <div class="item" data-slide-number="12">
                                                  <img src="<?php echo IMG_DIR?>gallery-13.jpg"></div>

                                              <div class="item" data-slide-number="13">
                                                  <img src="<?php echo IMG_DIR?>gallery-14.jpg"></div>

                                              <div class="item" data-slide-number="14">
                                                  <img src="<?php echo IMG_DIR?>gallery-15.jpg"></div>

                                              <div class="item" data-slide-number="15">
                                                  <img src="<?php echo IMG_DIR?>gallery-16.jpg"></div>
                                          </div>
                                          <!-- Carousel nav -->
                                          <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                              <span class="glyphicon glyphicon-chevron-left"></span>
                                          </a>
                                          <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                              <span class="glyphicon glyphicon-chevron-right"></span>
                                          </a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <!--/Slider-->
                  </div>

              </div>
          </div>
          </div>
        </div>
      </div>
    </section>
    <!--/ gallery-->
    <!--footer-->
    <footer id="footer">
      <div class="top-footer">
        <div class="container">
          <div class="row">
              <div class="col-md-4 col-md-offset-2 ">
                <h3>Contact Info</h3>
                <div class="space"></div>
                <p><i class="fa fa-map-marker fa-fw pull-left fa-2x"></i>Poblacion West, Taysan<br> Batangas, Philippines</p>
                <div class="space"></div>
                <p><i class="fa fa-phone fa-fw pull-left fa-2x"></i>+63 917 524 8645</p>
              </div>
          </div>
        </div>
      </div>
      <div class="footer-line">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-center">
              © Copyright Taysan Rural Health Unit<br>All Rights Reserved
              <div class="credits">
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!--/ footer-->

    <script src="<?php echo JS_DIR; ?>components/home/jquery.min.js"></script>
    <script src="<?php echo JS_DIR; ?>components/home/jquery.easing.min.js"></script>
    <script src="<?php echo JS_DIR; ?>components/home/bootstrap.min.js"></script>
    <script src="<?php echo JS_DIR; ?>components/home/custom.js"></script>

  </body>

</html>
