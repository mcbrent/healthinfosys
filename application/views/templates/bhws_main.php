<body class="md-skin mini-navbar">
  <div id="wrapper">
    <?php $this->load->view('components/left_nav_bhws'); ?>
    <div id="page-wrapper">
      <?php $this->load->view('components/head_nav_admin', $my_info); ?>
      <?php $this->load->view('components/alert'); ?>
      <?php echo $content; ?>

    </div>
  </div>
  <?php $this->load->view('components/javascript'); ?>
</body>
