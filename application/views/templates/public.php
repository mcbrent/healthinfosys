<body class="md-skin mini-navbar">
  <div id="wrapper">
      <div id="page-wrapper" style="margin:0px;" >
          <!-- <div class="row border-bottom">



          <nav class="navbar navbar-static-top white-" role="navigation">
              <nav id="navigation" class="navbar" role="navigation">
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="<?php echo DOMAIN?>login">
                            <i class="fa fa-sign-in"></i> Log in
                        </a>
                    </li>
                </ul>
              </nav>
          </nav>
          </div> -->
          <div class="row border-bottom">
            <nav id="navigation" class="navbar" role="navigation" style="margin-bottom: 0">
              <ul class="nav navbar-top-links navbar-right" >
                <li>
                    <a href="<?php echo DOMAIN?>login" style="color:black">
                        <i class="fa fa-sign-in"></i> Log in
                    </a>
                </li>

              </ul>

            </nav>
          </div>
          <?php $this->load->view('components/alert'); ?>
          <?php echo $content; ?>
      </div>
  </div>
  <?php $this->load->view('components/javascript'); ?>
</body>
