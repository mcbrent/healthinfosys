<div class="wrapper wrapper-content">
  <div class="row">

    <div class="col-lg-10">
    </div>

    <div class="col-lg-2 text-right">
      <button class="btn btn-primary has-tooltip form-submit" name="save" title="Save Resident" data-form="main-form"><i class="fa fa-address-book-o"></i> SAVE</button>
    </div>
  </div>
  <div class="ibox">
    <div class="ibox-content">
      <div>Resident No: <strong><?php echo htmlspecialchars($form_data['resident_no']) ?></strong></div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="tabs-container">
        <div class="tabs">
          <ul class="nav nav-tabs">
              <li class="active has-tooltip" title="Basic Information"><a data-toggle="tab" href="#tab-basic-info"> <i class="fa fa-address-card"></i></a></li>
              <?php if($action != 'add'){ ?>
                <li class="has-tooltip" title="Additional Info" ><a data-toggle="tab"href="#tab-additional-info"><i class="fa fa-folder"></i></a></li>
                <li class="has-tooltip" title="Health Records"><a data-toggle="tab" href="#tab-records"><i class="fa fa-medkit"></i></a></li>
                <li class="has-tooltip" title="Attachment"><a data-toggle="tab" href="#tab-attachment"><i class="fa fa-cloud"></i></a></li>
                <li class="has-tooltip" title="Qr Code"><a data-toggle="tab" href="#tab-qrcode"><i class="fa fa-qrcode"></i></a></li>
              <?php }?>
          </ul>
          <form id="main-form" method="POST" enctype="multipart/form-data" action="?">
            <div class="tab-content">
                <div id="tab-basic-info" class="tab-pane active">
                    <div class="panel-body form-horizontal">
                      <h5>First Name, Middle Name, Last Name, Suffix, (Maiden Name for married Women)</h5>
                      <div class="form-group">
                          <div class="col-sm-4"><input name="resident-first-name" type="text" class="form-control" placeholder="First Name" value="<?php echo $form_data['resident_first_name'] ?>"></div>
                          <div class="col-sm-4"><input name="resident-middle-name" type="text" class="form-control" placeholder="Middle Name" value="<?php echo $form_data['resident_middle_name'] ?>"></div>
                          <div class="col-sm-4"><input name="resident-last-name" type="text" class="form-control" placeholder="Last Name" value="<?php echo $form_data['resident_last_name'] ?>"></div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4"><input name="resident-name-ext" type="text" class="form-control" placeholder="Suffix" value="<?php echo $form_data['resident_name_ext'] ?>"></div>
                        <div class="col-sm-4"><input name="resident-maiden-name" type="text" class="form-control" placeholder="Maiden Name (If Married Women)" value="<?php echo $form_data['resident_name_ext'] ?>"></div>

                      </div>

                      <div class="hr-line-dashed"></div>
                      <h5>Current Address</h5>
                      <div class="form-group">
                          <div class="col-sm-12"><input type="text" name="add-desc" class="form-control" placeholder="(House/Unit No., Floor & Bldg./Street, Lot / Blk / Village)" value="<?php echo $form_data['add_desc'] ?>"></div>
                          <div class="col-sm-3 m-t-sm">
                            <input type="text" readonly class="form-control" value="Taysan"></input>
                          </div>
                          <div class="col-sm-9 m-t-sm">
                            <select class="form-control" name="current-barangay" data-placeholder="Select Barangay">
                                <option></option>
                                <?php if($form_data['address_brgy']['brgy_id']){ ?>
                                <option value="<?php echo $form_data['address_brgy']['brgy_id'] ?>" selected><?php echo $form_data['address_brgy']['brgy_name'] ?></option>
                                <?php } ?>
                            </select>
                          </div>
                      </div>
                      <div class="hr-line-dashed"></div>
                      <div class="form-group">
                        <div class="col-sm-2">
                          <h5>Sex</h5>
                          <label class="checkbox-inline i-checks"> <input type="radio" value="male" name="resident-gender" checked> <i></i> Male </label>
                          <label class="checkbox-inline i-checks"> <input type="radio" value="female" name="resident-gender" <?php echo $form_data['resident_gender'] == 'female'? 'checked': '' ?>> <i></i> Female</label>
                        </div>
                        <div class="col-sm-1">
                          <h5>Blood Type</h5>
                          <select class="form-control select2-basic" name="resident-blood" data-placeholder="Blood Type">
                              <option></option>
                              <option value="O" <?php echo $form_data['resident_blood_type'] == 'O'? 'selected' : '' ?>>O</option>
                              <option value="O-" <?php echo $form_data['resident_blood_type'] == 'O-'? 'selected' : '' ?>>O-</option>
                              <option value="A" <?php echo $form_data['resident_blood_type'] == 'A'? 'selected' : '' ?>>A</option>
                              <option value="A-" <?php echo $form_data['resident_blood_type'] == 'A-'? 'selected' : '' ?>>A-</option>
                              <option value="B" <?php echo $form_data['resident_blood_type'] == 'B'? 'selected' : '' ?>>B</option>
                              <option value="B-" <?php echo $form_data['resident_blood_type'] == 'B-'? 'selected' : '' ?>>B-</option>
                              <option value="AB" <?php echo $form_data['resident_blood_type'] == 'AB'? 'selected' : '' ?>>AB</option>
                              <option value="AB-" <?php echo $form_data['resident_blood_type'] == 'AB-'? 'selected' : '' ?>>AB-</option>
                          </select>
                        </div>
                        <div class="col-sm-4">
                          <h5>Birthday <span id="resident-age"></span></h5>
                          <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="resident-birthday" autocomplete="off" placeholder="mm/dd/yyyy" class="form-control datepicker" value="<?php echo $form_data['resident_birthday'] ?>">
                          </div>
                        </div>
                        <div class="col-sm-5">
                          <h5>Civil Status</h5>
                          <select class="form-control select2-basic" name="resident-civil-status" data-placeholder="Select Civil Status">
                              <option></option>
                              <option value="single" <?php echo $form_data['resident_civil_status'] == 'single'? 'selected' : '' ?>>Single</option>
                              <option value="married" <?php echo $form_data['resident_civil_status'] == 'married'? 'selected' : '' ?>>Married</option>
                              <option value="widowed" <?php echo $form_data['resident_civil_status'] == 'widowed'? 'selected' : '' ?>>Widowed</option>
                          </select>
                        </div>
                      </div>
                      <div class="hr-line-dashed"></div>
                      <h4>Parent Information</h4>
                      <h5>Resident's Father</h5>
                      <div class="form-group">
                          <div class="col-sm-3"><input name="resident-father-first-name" type="text" class="form-control" placeholder="Father First Name" value="<?php echo $form_data['resident_father_first_name'] ?>"></div>
                          <div class="col-sm-3"><input name="resident-father-middle-name" type="text" class="form-control" placeholder="Father Middle Name" value="<?php echo $form_data['resident_father_middle_name'] ?>"></div>
                          <div class="col-sm-3"><input name="resident-father-last-name" type="text" class="form-control" placeholder="Father Last Name" value="<?php echo $form_data['resident_father_last_name'] ?>"></div>
                          <div class="col-sm-3"><input name="resident-father-name-ext" type="text" class="form-control" placeholder="Father Suffix" value="<?php echo $form_data['resident_father_name_ext'] ?>"></div>
                      </div>
                      <h5>Resident's Mother</h5>
                      <div class="form-group">
                          <div class="col-sm-3"><input name="resident-mother-first-name" type="text" class="form-control" placeholder="Mother First Name" value="<?php echo $form_data['resident_mother_first_name'] ?>"></div>
                          <div class="col-sm-3"><input name="resident-mother-middle-name" type="text" class="form-control" placeholder="Mother Middle Name" value="<?php echo $form_data['resident_mother_middle_name'] ?>"></div>
                          <div class="col-sm-3"><input name="resident-mother-last-name" type="text" class="form-control" placeholder="Mother Last Name" value="<?php echo $form_data['resident_mother_last_name'] ?>"></div>
                          <div class="col-sm-3"><input name="resident-mother-name-ext" type="text" class="form-control" placeholder="Mother Suffix" value="<?php echo $form_data['resident_mother_name_ext'] ?>"></div>
                      </div>
                      <div class="hr-line-dashed"></div>
                      <h4>Other Information</h4>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <h5>Employment</h5>
                          <select class="form-control select2-basic" name="resident-employment-status" data-placeholder="Select Employment Status">
                              <option></option>
                              <option value="Employed" <?php echo $form_data['resident_employment_status'] == 'Employed'? 'selected' : '' ?>>Employed</option>
                              <option value="Unemployed" <?php echo $form_data['resident_employment_status'] == 'Unemployed'? 'selected' : '' ?>>Unemployed</option>
                          </select>
                        </div>
                        <div class="col-sm-4">
                          <h5>Occupation</h5>
                          <input name="resident-occupation" type="text" class="form-control" placeholder="Enter Occupation" value="<?php echo $form_data['resident_occupation']?>">
                        </div>
                        <div class="col-sm-4">
                          <h4>Religion</h4>
                          <input name="resident-religion" type="text" class="form-control" placeholder="Enter Religion" value="<?php echo $form_data['resident_religion']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-12">
                          <h4>Birth Place (Country / Province / City)</h4>
                          <input name="resident-birthplace" type="text" class="form-control" placeholder="Enter Birthplace" value="<?php echo $form_data['resident_birthplace']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-3">
                          <h4>Household No.</h4>
                          <input name="resident-household-no" type="text" class="form-control" placeholder="Enter Household No." value="<?php echo $form_data['resident_householdno']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-3">
                          <h4>Philhealth Member</h4>
                          <label class="checkbox-inline i-checks"> <input type="radio" value="1" name="resident-philhealth" <?php echo $form_data['resident_philhealth'] == '1'? 'checked': '' ?>> <i></i> Yes </label>
                          <label class="checkbox-inline i-checks"> <input type="radio" value="0" name="resident-philhealth" <?php echo (!$form_data['resident_philhealth'])? 'checked': '' ?>> <i></i> No</label>
                        </div>
                        <div class="col-sm-3">
                          <h4>Philhealth Status</h4>
                          <input name="resident-philhealth-status" type="text" class="form-control" placeholder="Enter Philhealth Status" value="<?php echo $form_data['resident_philhealth_status']?>">
                        </div>
                        <div class="col-sm-3">
                          <h4>Philhealth No</h4>
                          <input name="resident-philhealth-no" type="text" class="form-control" placeholder="Enter Philhealth No" value="<?php echo $form_data['resident_philhealthno']?>">
                        </div>
                        <div class="col-sm-3">
                          <h4>Philhealth Category</h4>
                          <input name="resident-philhealth-cat" type="text" class="form-control" placeholder="Enter Philhealth Category" value="<?php echo $form_data['resident_philhealthcat']?>">
                        </div>
                      </div>
                      <div class="hr-line-dashed"></div>
                      <h4>Contact Details</h4>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <h5>Contact Number 1</h5>
                          <input name="phone-number-1" type="text" class="form-control" placeholder="Enter Main Contact Number" value="<?php echo $form_data['phone_number_1'] ?>">
                        </div>
                        <div class="col-sm-4">
                          <h5>Contact Number 2</h5>
                          <input name="phone-number-2" type="text" class="form-control" placeholder="Enter Mobile Number" value="<?php echo $form_data['phone_number_2'] ?>">
                        </div>
                      </div>
                      <div class="hr-line-dashed"></div>
                      <div class="form-group">
                        <div class="col-md-12">
                          <h5>Allergies</h5>
                        </div>
                        <div class="col-md-12">
                          <textarea class="form-control" name="resident-allergies" placeholder="Enter Resident Allergies"><?php echo $form_data['resident_allergies']?></textarea>
                        </div>
                        <div class="col-md-12">
                          <h5>Family History</h5>
                        </div>
                        <div class="col-md-12">
                          <textarea class="form-control" name="resident-family-history" placeholder="Enter Resident Family History"><?php echo $form_data['resident_family_history']?></textarea>
                        </div>
                      </div>

                    </div>
                </div>
                <div id="tab-additional-info" class="tab-pane">
                    <div class="panel-body form-horizontal">
                        <h4>Additional Info</h4>
                        <div class="form-group">
                          <div class="col-sm-12">
                            <textarea name="resident-additional-info" class="summernote">
                              <?php echo $form_data['resident_add_info'] ?>
                            </textarea>
                          </div>
                        </div>
                    </div>
                </div>
                <div id="tab-records" class="tab-pane">
                    <div class="panel-body">
                      <h3>Medical Checkup History</h3>
                      <div class="table-responsive">
                        <table id="attachment-table" class="table table-striped table-bordered table-hover" data-config="{}">
                          <thead>
                            <tr>
                              <th class="col-md-3 text-center">Date</th>
                              <th class="col-md-5 text-center">Medical Officer</th>
                              <th class="col-md-3 text-center">Result</th>
                              <th class="col-md-1 text-center no-sort">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach($form_data['appointment_list'] as $appointment){ ?>
                            <tr>
                              <td><?php echo date('m-d-Y h:i A', strtotime($appointment['appointment_diagnosed_datetime']));  ?></td>
                              <td><?php echo $appointment['medical_officer'] ?></td>
                              <td><?php echo $appointment['dt_code'].'-'.$appointment['dt_name'] ?></td>
                              <td class="text-center">
                                <a class="btn btn-info" href="<?php echo DOMAIN.'appointment/read-medical-treatment/'.$appointment['appointment_id'] ?>"><i class="fa fa-search"></i></a>
                              </td>
                            </tr>
                            <?php }?>
                          </tbody>

                        </table>
                      </div>
                    </div>
                </div>
                <div id="tab-attachment" class="tab-pane">
                    <div class="panel-body">
                      <div class="table-responsive">
                        <table id="attachment-table" class="table table-striped table-bordered table-hover" data-config="{}">
                          <thead>
                            <tr>
                              <th class="col-md-6 text-center">File Name</th>
                              <th class="col-md-4 text-center">Date</th>
                              <th class="col-md-2 text-center no-sort">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                              foreach($form_data['resident_attachment'] as $attachment){ ?>
                            <tr>
                              <td class="col-md-6"><?php echo $attachment['rattachment_name']?></td>
                              <td class="col-md-4"><?php echo date('M d Y', strtotime($attachment['rattachment_date']))?></td>
                              <td class="col-md-2 text-center">
                                  <button type="button" class="btn btn-danger has-tooltip delete-row" title="Delete" value="<?php echo $attachment['rattachment_id'] ?>"><i class="fa fa-trash"></i></button>
                                  <button type="button" class="btn blue-bg has-tooltip save-file" title="Download" value="<?php echo $attachment['rattachment_id'] ?>"><i class="fa fa-download"></i></button>
                              </td>
                            </tr>
                            <?php
                              }
                            ?>
                            <tr>
                              <td class="col-md-6 text-center">
                                <input name="file-name[]" type="text" class="form-control" placeholder="Enter File Tag Name" value=""/>
                              </td>
                              <td class="col-md-4 text-center"></td>
                              <td class="col-md-2 text-center">
                                <input type="file" name="file-attachment[]" />
                              </td>
                            </tr>
                          </tbody>
                          <tfoot>
                            <td class="col-md-12 text-center gray-bg" colspan="3">
                              <button type="button" class="btn btn-primary btn-sm add-attachment"><i class="fa fa-plus"></i> Add More</button>
                            </td>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                </div>
                <div id="tab-qrcode" class="tab-pane">
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12 text-center">
                          <img class="img-responsive" src="<?php echo UPLOAD?>qrcode/<?php echo $form_data['qrcode_url'] ?>" alt="QR Code here" style="margin: 0 auto; width: 345px"/>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


</div>
<script src="<?php echo JS_DIR ?>components/resident/resident_profile.js"></script>
