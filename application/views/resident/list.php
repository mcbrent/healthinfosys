<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
      <div class="col-lg-12">
              <div class="panel panel-primary">
                  <div class="panel-heading">
                    <div class="row">
                      <div class="col-md-1">
                        <h5>Resident List</h5>
                      </div>
                      <div class="col-md-6">
                        <select class="form-control select2-basic" name="barangay-list">
                          <?php if(empty($brgy)){ ?>
                            <option value="0">All Barangay</option>
                            <?php foreach($brgy_list as $brgy){ ?>
                            <option value="<?php echo $brgy['brgy_id'] ?>"><?php echo $brgy['brgy_name'] ?></option>
                            <?php } ?>
                          <?php }
                          else{ ?>
                            <option value="<?php echo $brgy['brgy_id'] ?>"><?php echo $brgy['brgy_name'] ?></option>
                          <?php }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="panel-body">

                    <div class="table-responsive">
                      <table id="resident-list" class="table table-striped table-bordered table-hover" data-config="{}">
                        <thead>
                          <tr>
                            <th class="col-md-2 text-center">Barangay</th>
                            <th class="col-md-4 text-center">Resident</th>
                            <th class="col-md-2 text-center">Gender / Age</th>
                            <th class="col-md-1 text-center">Civil Status</th>
                            <th class="col-md-1 text-center">Contact Info</th>
                            <th class="col-md-1 text-center no-sort">Action</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </div>

                  </div>
              </div>
          </div>
    </div>

</div>

<script src="<?php echo JS_DIR ?>components/resident/resident_list.js"></script>
