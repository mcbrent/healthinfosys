<div class="wrapper wrapper-content">
  <div class="row">

    <div class="col-lg-10">
      <h5>View Resident</h5>
    </div>

  </div>
  <div class="ibox">
    <div class="ibox-content">
      <div>Resident No: <strong><?php echo htmlspecialchars($form_data['resident_no']) ?></strong> - <u><?php echo $form_data['resident_first_name'].' '.$form_data['resident_middle_name'].' '.$form_data['resident_last_name'].' '.$form_data['resident_name_ext'] ?></u></div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="tabs-container">
          <ul class="nav nav-tabs">
            <li class="active has-tooltip" title="Basic Information"><a data-toggle="tab" href="#tab-basic-info"> <i class="fa fa-address-card"></i></a></li>
            <li class="has-tooltip" title="Additional Info" ><a data-toggle="tab"href="#tab-additional-info"><i class="fa fa-folder"></i></a></li>
            <li class="has-tooltip" title="Health Records"><a data-toggle="tab" href="#tab-records"><i class="fa fa-medkit"></i></a></li>
            <li class="has-tooltip" title="Attachment"><a data-toggle="tab" href="#tab-attachment"><i class="fa fa-cloud"></i></a></li>
            <li class="has-tooltip" title="Qr Code"><a data-toggle="tab" href="#tab-qrcode"><i class="fa fa-qrcode"></i></a></li>
          </ul>
            <div class="tab-content">
                <div id="tab-basic-info" class="tab-pane active">
                    <div class="panel-body form-horizontal">
                      <h5>First Name, Middle Name, Last Name, Suffix, (Maiden Name for married Women)</h5>
                      <div class="form-group">
                          <div class="col-sm-4"><input readonly type="text" class="form-control" value="<?php echo $form_data['resident_first_name'] ?>"></div>
                          <div class="col-sm-4"><input readonly type="text" class="form-control" value="<?php echo $form_data['resident_middle_name'] ?>"></div>
                          <div class="col-sm-4"><input readonly type="text" class="form-control" value="<?php echo $form_data['resident_last_name'] ?>"></div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4"><input readonly type="text" class="form-control" value="<?php echo $form_data['resident_name_ext'] ?>"></div>
                        <div class="col-sm-4"><input readonly type="text" class="form-control" value="<?php echo $form_data['resident_name_ext'] ?>"></div>

                      </div>

                      <div class="hr-line-dashed"></div>
                      <h5>Current Address</h5>
                      <div class="form-group">
                          <div class="col-sm-12"><input type="text" readonly class="form-control" value="<?php echo $form_data['add_desc'] ?>"></div>
                          <div class="col-sm-3 m-t-sm">
                            <input type="text" readonly class="form-control" value="Taysan"></input>
                          </div>
                          <div class="col-sm-9 m-t-sm">
                            <input type="text" readonly class="form-control" value="<?php echo $form_data['address_brgy']['brgy_name'] ?>"></input>
                          </div>
                      </div>
                      <div class="hr-line-dashed"></div>
                      <div class="form-group">
                        <div class="col-sm-2">
                          <h5>Sex</h5>
                          <input type="text" readonly class="form-control" value="<?php echo $form_data['resident_gender'] ?>"></input>
                        </div>
                        <div class="col-sm-1">
                          <h5>Blood Type</h5>
                          <input type="text" readonly class="form-control" value="<?php echo $form_data['resident_blood_type'] ?>"></input>
                        </div>
                        <div class="col-sm-4">
                          <h5>Birthday <span id="resident-age">( <?php echo date_diff(date_create($form_data['resident_birthday']), date_create('now'))->y  ?> Years Old )</span></h5>
                          <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" readonly class="form-control" value="<?php echo $form_data['resident_birthday'] ?>">
                          </div>
                        </div>
                        <div class="col-sm-5">
                          <h5>Civil Status</h5>
                          <input type="text" readonly class="form-control" value="<?php echo $form_data['resident_civil_status'] ?>"></input>
                        </div>
                      </div>
                      <div class="hr-line-dashed"></div>
                      <h4>Parent Information</h4>
                      <h5>Resident's Father</h5>
                      <div class="form-group">
                          <div class="col-sm-3"><input readonly type="text" class="form-control" value="<?php echo $form_data['resident_father_first_name'] ?>"></div>
                          <div class="col-sm-3"><input readonly type="text" class="form-control" value="<?php echo $form_data['resident_father_middle_name'] ?>"></div>
                          <div class="col-sm-3"><input readonly type="text" class="form-control" value="<?php echo $form_data['resident_father_last_name'] ?>"></div>
                          <div class="col-sm-3"><input readonly type="text" class="form-control" value="<?php echo $form_data['resident_father_name_ext'] ?>"></div>
                      </div>
                      <h5>Resident's Mother</h5>
                      <div class="form-group">
                          <div class="col-sm-3"><input readonly type="text" class="form-control" value="<?php echo $form_data['resident_mother_first_name'] ?>"></div>
                          <div class="col-sm-3"><input readonly type="text" class="form-control" value="<?php echo $form_data['resident_mother_middle_name'] ?>"></div>
                          <div class="col-sm-3"><input readonly type="text" class="form-control" value="<?php echo $form_data['resident_mother_last_name'] ?>"></div>
                          <div class="col-sm-3"><input readonly type="text" class="form-control" value="<?php echo $form_data['resident_mother_name_ext'] ?>"></div>
                      </div>
                      <div class="hr-line-dashed"></div>
                      <h4>Other Information</h4>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <h5>Employment</h5>
                          <input type="text" readonly class="form-control" value="<?php echo $form_data['resident_employment_status'] ?>"></input>
                        </div>
                        <div class="col-sm-4">
                          <h5>Occupation</h5>
                          <input readonly type="text" class="form-control" value="<?php echo $form_data['resident_occupation']?>">
                        </div>
                        <div class="col-sm-4">
                          <h4>Religion</h4>
                          <input readonly type="text" class="form-control" value="<?php echo $form_data['resident_religion']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-12">
                          <h4>Birth Place (Country / Province / City)</h4>
                          <input readonly type="text" class="form-control" value="<?php echo $form_data['resident_birthplace']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-3">
                          <h4>Household No.</h4>
                          <input readonly type="text" class="form-control" value="<?php echo $form_data['resident_householdno']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-3">
                          <h4>Philhealth Member</h4>
                          <input readonly type="text" class="form-control" value="<?php echo $form_data['resident_philhealth'] == '1'? 'YES': 'NO' ?>">
                        </div>
                        <div class="col-sm-3">
                          <h4>Philhealth Status</h4>
                          <input readonly type="text" class="form-control" value="<?php echo $form_data['resident_philhealth_status']?>">
                        </div>
                        <div class="col-sm-3">
                          <h4>Philhealth No</h4>
                          <input readonly type="text" class="form-control" value="<?php echo $form_data['resident_philhealthno']?>">
                        </div>
                        <div class="col-sm-3">
                          <h4>Philhealth Category</h4>
                          <input readonly type="text" class="form-control" value="<?php echo $form_data['resident_philhealthcat']?>">
                        </div>
                      </div>
                      <div class="hr-line-dashed"></div>
                      <h4>Contact Details</h4>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <h5>Contact Number 1</h5>
                          <input readonly type="text" class="form-control" value="<?php echo $form_data['phone_number_1'] ?>">
                        </div>
                        <div class="col-sm-4">
                          <h5>Contact Number 2</h5>
                          <input readonly type="text" class="form-control" value="<?php echo $form_data['phone_number_2'] ?>">
                        </div>
                      </div>
                      <div class="hr-line-dashed"></div>
                      <div class="form-group">
                        <div class="col-md-12">
                          <h5>Allergies</h5>
                        </div>
                        <div class="col-md-12"><blockquote><?php echo $form_data['resident_allergies']?></blockquote></div>
                        <div class="col-md-12">
                          <h5>Family History</h5>
                        </div>
                        <div class="col-md-12"><blockquote><?php echo $form_data['resident_family_history']?></blockquote></div>
                      </div>

                    </div>
                </div>
                <div id="tab-additional-info" class="tab-pane">
                    <div class="panel-body form-horizontal">
                        <h4>Additional Info</h4>
                        <div class="form-group">
                          <div class="col-sm-12">
                            <?php echo $form_data['resident_add_info'] ?>
                          </div>
                        </div>
                    </div>
                </div>
                <div id="tab-records" class="tab-pane">
                  <div class="panel-body">
                    <h3>Medical Checkup History</h3>
                    <div class="table-responsive">
                      <table id="attachment-table" class="table table-striped table-bordered table-hover" data-config="{}">
                        <thead>
                          <tr>
                            <th class="col-md-3 text-center">Date</th>
                            <th class="col-md-5 text-center">Medical Officer</th>
                            <th class="col-md-3 text-center">Result</th>
                            <th class="col-md-1 text-center no-sort">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach($form_data['appointment_list'] as $appointment){ ?>
                          <tr>
                            <td><?php echo date('m-d-Y h:i A', strtotime($appointment['appointment_diagnosed_datetime']));  ?></td>
                            <td><?php echo $appointment['medical_officer'] ?></td>
                            <td><?php echo $appointment['dt_code'].'-'.$appointment['dt_name'] ?></td>
                            <td class="text-center">
                              <a class="btn btn-info" href="<?php echo DOMAIN.'appointment/read-medical-treatment/'.$appointment['appointment_id'] ?>"><i class="fa fa-search"></i></a>
                            </td>
                          </tr>
                          <?php }?>
                        </tbody>

                      </table>
                    </div>
                  </div>
                </div>
                <div id="tab-attachment" class="tab-pane">
                    <div class="panel-body">
                      <div class="table-responsive">
                        <table id="attachment-table" class="table table-striped table-bordered table-hover" data-config="{}">
                          <thead>
                            <tr>
                              <th class="col-md-6 text-center">File Name</th>
                              <th class="col-md-4 text-center">Date</th>
                              <th class="col-md-2 text-center no-sort">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                              foreach($form_data['resident_attachment'] as $attachment){ ?>
                            <tr>
                              <td class="col-md-6"><?php echo $attachment['rattachment_name']?></td>
                              <td class="col-md-4"><?php echo date('M d Y', strtotime($attachment['rattachment_date']))?></td>
                              <td class="col-md-2 text-center">
                                  <button type="button" class="btn blue-bg has-tooltip save-file" title="Download" value="<?php echo $attachment['rattachment_id'] ?>"><i class="fa fa-download"></i></button>
                              </td>
                            </tr>
                            <?php
                              }
                            ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                </div>
                <div id="tab-qrcode" class="tab-pane">
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12 text-center">
                          <img class="img-responsive" src="<?php echo UPLOAD?>qrcode/<?php echo $form_data['qrcode_url'] ?>" alt="QR Code here" style="margin: 0 auto; width: 345px"/>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
      </div>
    </div>
  </div>


</div>
<script src="<?php echo JS_DIR ?>components/resident/view.js"></script>
