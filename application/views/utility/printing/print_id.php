<link href="<?php echo THEME; ?>css/bootstrap.min.css" rel="stylesheet">
<style>
#main{
  height:100%;
  margin: 0 !important;
  padding: 0 !important;
}
#first-page, #second-page{
  height:49%;
  background-color: lightgray;
  margin: 0 !important;
  padding: 0 !important;
}
#first-page{
  border-bottom: 1px dashed black;
}
table{
  width:100%;
}
.main-font{
  font-size: 10px;
}
</style>
<div id="main" class="main-font">
  <div id="first-page">
    <div class="text-center">
      <h5>Municipality Health Unit of Taysan</h5>
    </div>
    <table>
      <tr style="height:70%">
        <td style="width:45%; text-align: right;">
          <img alt="image" style="height: 25%;" class="img-responsive" src="<?php echo UPLOAD.'qrcode/'.$qr_image['fm_encypted_name'] ?>">

        </td>
        <td style="width:10%"></td>
        <td style="width:45%">
          <div>
            <img alt="image" style=" height: 25%;" class="img-responsive" src="<?php echo UPLOAD.'profile/'.$dp_image ?>">
          </div>
        </td>
      </tr>
      <tr style="height:30%">
        <td class="text-center" colspan="3">
          <p>
            <br>
            <strong><?php echo $basic_contact['bc_first_name'].' '.$basic_contact['bc_middle_name'].' '.$basic_contact['bc_last_name'].' '.$basic_contact['bc_name_ext']?></strong>
            <br>
            <small><?php echo $user_type ?></small>
            <br>
            <small>Birthdate: <?php echo date('m-d-Y', strtotime($basic_contact['bc_birthdate'])); ?></small>
          </p>
        </td>
      </tr>
    </table>
  </div>
  <div id="second-page">
    <table>
      <tr><td>&nbsp;</td></tr>
      <tr>
        <td class="text-center" colspan="2">
          <h6>Philhealth Information</h6>
        </td>
      </tr>
      <tr>
        <td class="text-right" style="width:25%">
          <h6>No:</h6>
          <h6>Status:</h6>
          <h6>Category:</h6>
        </td>
        <td class="text-left" style="width:75%">
          <h6></h6>
          <h6></h6>
          <h6></h6>
        </td>
      </tr>
      <tr>
        <td class="text-center" colspan="2">
          <h6>&nbsp;</h6>
        </td>
      </tr>
      <tr>
        <td class="text-center" colspan="2">
          <h6>&nbsp;</h6>
        </td>
      </tr>
      <tr>
        <td class="text-center" colspan="2">
          <h6>&nbsp;</h6>
        </td>
      </tr>
      <tr>
        <td class="text-center" colspan="2">
          <h6>&nbsp;</h6>
        </td>
      </tr>
      <tr>
        <td class="text-center" colspan="2">
          <h6>&nbsp;</h6>
        </td>
      </tr>
      <tr>
        <td class="text-center" colspan="2">
          <h6>&nbsp;</h6>
        </td>
      </tr>
      <tr>
        <td class="text-center" colspan="2">
          <h6>____________________</h6>
        </td>
      </tr>
      <tr>
        <td class="text-center" colspan="2">
          <h6>Signature</h6>
        </td>
      </tr>
    </table>
  </div>
</div>
