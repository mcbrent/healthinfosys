<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
      <div class="col-lg-12">
              <div class="panel panel-primary">
                  <div class="panel-heading">
                      <h5>All User List</h5>
                  </div>
                  <div class="panel-body">

                    <div class="table-responsive">
                      <table id="all-user-list" class="table table-striped table-bordered table-hover" data-config="{}">
                        <thead>
                          <tr>
                            <th class="col-md-6 text-center">Name</th>
                            <th class="col-md-5 text-center">User Type</th>
                            <th class="col-md-1 text-center no-sort">Action</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </div>

                  </div>
              </div>
          </div>
    </div>

</div>

<script src="<?php echo JS_DIR ?>components/utility/id_list.js"></script>
