<link href="<?php echo THEME; ?>css/bootstrap.min.css" rel="stylesheet">
<style>
#main{
  height:100%;
  margin: 0 !important;
  padding: 0 !important;
}
#first-page, #second-page{
  height:49%;
  background-color: lightgray;
  margin: 0 !important;
  padding: 0 !important;
}
#first-page{
  border-bottom: 1px dashed black;
}
table{
  width:100%;
}
.main-font{
  font-size: 10px;
}
.bold{
  font-weight: bold;
}
.hr{
  border-top: 2px solid black;
}
</style>
<div id="main" class="main-font">
  <div class="container">

    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:20%">

          </td>
          <td style="width:10%">
            <img src="<?php echo IMG_DIR?>Taysan_Seal.png" height="65">
          </td>
          <td class="text-center" style="width:45%;">
            <div >
              <h4>Republic of the Philippines</h4>
              <h4>Province of Batangas</h4>
              <h4 class="bold">TAYSAN RURAL HEALTH UNIT</h4>
              <h4>Taysan, Batangas</h4>
            </div>

          </td>
          <td style="width:25%">&nbsp;</td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:20%">

          </td>
          <td class="text-center" style="width:60%;">
            <div>
              <h4 class="bold">BATANGAS SERVICE DELIVERY NETWORK</h4>
              <br>
              <h4 class="bold"><u>R E F E R R A L &nbsp; S L I P</u></h4>
            </div>
          </td>
          <td style="width:20%">&nbsp;</td>
        </tr>
      </tbody>
    </table><br><br>
    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:30%">Name of Referring Health Facility:</td>
          <td class="text-center bold" style="width:70%; border-bottom: 1px solid black">
            TAYSAN RURAL HEALTH UNIT
          </td>
        </tr>
        <tr style="width:100%">
          <td style="width:40%">Address, contact number and email address</td>
          <td class="text-center bold" style="width:60%; border-bottom: 1px solid black">
            POBLACION WEST, TAYSAN, BATANGAS
          </td>
        </tr>
        <tr style="width:100%">
          <td class="bold" style="width:20%"><h4>Referral Category:</h4></td>
          <td class="" style="width:80%">
            A1( ) A2( ) B1( ) B2( ) C1( ) C2( ) Unclassified( )
          </td>
        </tr>
      </tbody>
    </table>
    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:15%">To:</td>
          <td class="bold text-uppercase" style="width:50%; border-bottom: 1px solid black">

          </td>
          <td style="width:5%">Date:</td>
          <td class="text-center bold" style="width:30%; border-bottom: 1px solid black">
            <?php echo date('m-d-Y')?>
          </td>
        </tr>
        <tr style="width:100%">
          <td style="width:15%">Family No:</td>
          <td class="bold text-uppercase" style="width:50%; border-bottom: 1px solid black">
            <?php echo $resident['resident_householdno']?>
          </td>
          <td style="width:5%">Time:</td>
          <td class="text-center bold" style="width:30%; border-bottom: 1px solid black">
            <?php echo date('h:i A')?>
          </td>
        </tr>
        <tr style="width:100%">
          <td style="width:15%">Case No.:</td>
          <td class="bold text-uppercase" style="width:50%; border-bottom: 1px solid black">

          </td>
          <td style="width:5%">Health Card:</td>
          <td class="text-center bold" style="width:30%; border-bottom: 1px solid black">
            <?php echo $resident['resident_philhealthno'] ?>
          </td>
        </tr>

      </tbody>
    </table>
    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:15%">Patient's Name:</td>
          <td class="bold text-uppercase" style="width:50%; border-bottom: 1px solid black">
            <?php echo $basic_contact['bc_first_name'].' '.$basic_contact['bc_middle_name'].' '.$basic_contact['bc_last_name'] ?>
          </td>
          <td style="width:5%">Age:</td>
          <td class="text-center bold" style="width:5%; border-bottom: 1px solid black">
            <?php echo date_diff(date_create($basic_contact['bc_birthdate']), date_create('now'))->y; ?>
          </td>
          <td style="width:5%">Sex:</td>
          <td class="text-center bold text-uppercase" style="width:8%; border-bottom: 1px solid black">
            <?php echo $basic_contact['bc_gender']; ?>
          </td>
          <td style="width:5%">CS:</td>
          <td class="text-center bold text-uppercase" style="width:7%; border-bottom: 1px solid black">

          </td>
        </tr>
      </tbody>
    </table>
    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:15%">Address:</td>
          <td class="bold text-uppercase" style="width:50%; border-bottom: 1px solid black">
            <?php echo $address['address_desc'].', Barangay '.$address['brgy_name'].', '.$address['city_name'].' '.$address['province_name'] ?>
          </td>
          <td style="width:15%">Contact Number:</td>
          <td class="text-center bold" style="width:25%; border-bottom: 1px solid black">
            <?php echo $basic_contact['bc_phone_num1'].' / '.$basic_contact['bc_phone_num2'].' / '.$basic_contact['bc_phone_num3'] ?>
          </td>
        </tr>
      </tbody>
    </table>
    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:10%">Religion:</td>
          <td class="bold text-uppercase" style="width:30%; border-bottom: 1px solid black">
            <?php echo $resident['resident_religion'] ?>
          </td>
          <td style="width:15%">Blood Type:</td>
          <td class="text-center bold" style="width:15%; border-bottom: 1px solid black">
            <?php echo $resident['resident_blood_type'] ?>
          </td>
          <td style="width:10%">(RH + or -)</td>
          <td class="text-center bold" style="width:20%; border-bottom: 1px solid black">

          </td>
        </tr>
      </tbody>
    </table>
    <br><br><div class="hr"></div><br><br>
    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:35%">Clinical Diagnosis / Impression: </td>
          <td class="bold text-uppercase" style="width:65%; border-bottom: 1px solid black">
          </td>
        </tr>
        <tr style="width:100%">
          <td colspan="2" class="bold text-uppercase" style="border-bottom: 1px solid black">&nbsp;</td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:25%">Clinical History: </td>
          <td class="bold text-uppercase" style="width:75%; border-bottom: 1px solid black">
          </td>
        </tr>
        <tr style="width:100%">
          <td colspan="2" class="bold text-uppercase" style="border-bottom: 1px solid black">&nbsp;</td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:20%">Pertinent P.E. : </td>
          <td class="bold text-uppercase" style="width:80%; border-bottom: 1px solid black">
          </td>
        </tr>
        <tr style="width:100%">
          <td colspan="2" class="bold text-uppercase" style="border-bottom: 1px solid black">&nbsp;</td>
        </tr>
        <tr style="width:100%">
          <td colspan="2" class="bold text-uppercase" style="border-bottom: 1px solid black">&nbsp;</td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:35%">Treatment/ Medicals Given </td>
          <td class="bold text-uppercase" style="width:65%; border-bottom: 1px solid black">
          </td>
        </tr>
        <tr style="width:100%">
          <td colspan="2" class="bold text-uppercase" style="border-bottom: 1px solid black">&nbsp;</td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:75%">Reasons for Referral : Consultation ( ), Diagnostic test ( ), Transfer Service ( ), Other ( ) </td>
          <td class="bold text-uppercase" style="width:25%; border-bottom: 1px solid black">
          </td>
        </tr>
        <tr style="width:100%">
          <td colspan="2" class="bold text-uppercase" style="border-bottom: 1px solid black">&nbsp;</td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:20%">Remarks: </td>
          <td class="bold text-uppercase" style="width:80%; border-bottom: 1px solid black">
          </td>
        </tr>
        <tr style="width:100%">
          <td colspan="2" class="bold text-uppercase" style="border-bottom: 1px solid black">&nbsp;</td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr style="width:100%">
          <td>Name, Designation, Signature and contact number of Referring Health Provider: </td>
        </tr>
        <tr style="width:100%">
          <td class="bold text-uppercase" style="border-bottom: 1px solid black">&nbsp;</td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr style="width:100%">
          <td>Name and Designation ofg Contact Person in Receiving Hospital: </td>
        </tr>
        <tr style="width:100%">
          <td class="bold text-uppercase" style="border-bottom: 1px solid black">&nbsp;</td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:75%">Receiving Hospital Telephone Number Utilized to Facilitate Transfer:</td>
          <td class="bold text-uppercase" style="width:25%; border-bottom: 1px solid black">
          </td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr style="width:100%">
          <td>RPRH Referral:_____ Yes _____ No, If Yes, method of choice __________ Counseled: _____ Yes _____ No _____</td>
        </tr>
      </tbody>
    </table>
    <br><br><br><div class="hr"></div><br>
  </div>
  <div class="text-center">
    <h4 class="bold"><u>RETURN SLIP</u></h4>
    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:75%"></td>
          <td class="bold text-right text-uppercase" style="width:25%;">
            ___________, 20____
          </td>
        </tr>
      </tbody>
    </table>
    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:20%">Patient's Name:</td>
          <td class="bold text-right text-uppercase" style="width:60%; border-bottom: 1px solid black">
          </td>
          <td style="width:20%">Age:_______ Sex:_______</td>
        </tr>
      </tbody>
    </table>
    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:35%">Initial Diagnosis / Admitting Impression:</td>
          <td class="bold text-right text-uppercase" style="width:65%; border-bottom: 1px solid black">
          </td>
        </tr>
        <tr style="width:100%">
          <td colspan="2" style="width:100%; border-bottom: 1px solid black">&nbsp;</td>
        </tr>
        <tr style="width:100%">
          <td colspan="2">Action Taken: (Admitted, Under Observation, Managed and Discharged, Discharged for Follow-up</td>
        </tr>
        <tr style="width:100%">
          <td style="width:35%">Referred to another Facility, etc.)</td>
          <td style="width:65%; border-bottom: 1px solid black">
          </td>
        </tr>
        <tr style="width:100%">
          <td style="width:25%">Initial Plan</td>
          <td style="width:75%; border-bottom: 1px solid black">
          </td>
        </tr>
      </tbody>
    </table>
    <br>
    <br>
    <br>
    <br>
    <br>
    <table>
      <tbody>
        <tr style="width:100%">
          <td style="width:60%"></td>
          <td class="bold text-right text-uppercase" style="width:40%; border-bottom: 1px solid black">
          </td>
        </tr>
        <tr style="width:100%">
          <td style="width:60%"></td>
          <td class="text-center" style="width:40%;">Signature Over Printed Name</td>
        </tr>
        <tr style="width:100%">
          <td style="width:60%"></td>
          <td class="text-center" style="width:40%;">Attending Health Provider / Department /</td>
        </tr>
        <tr style="width:100%">
          <td style="width:60%"></td>
          <td class="text-center" style="width:40%;">Facility Contact Number
          </td>
        </tr>

      </tbody>
    </table>
  </div>
</div>
