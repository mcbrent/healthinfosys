<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
    <div class="col-lg-12 text-right">
      <button class="btn btn-primary has-tooltip form-submit" title="Save" data-form="main-form"><i class="fa fa-file"></i></button>
    </div>
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Calender</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="fullscreen-link">
                        <i class="fa fa-expand"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
              <form id="main-form" action="" method="POST" class="form-horizontal">
                  <div class="form-group">
                      <div class="col-sm-6">
                          <h5>January</h5>
                          <input type="text" name="month-desc-jan" class="form-control" placeholder="Enter Activities for January" value="<?php echo $form_data['act_jan']['ref_value']?>">
                      </div>
                      <div class="col-sm-6">
                          <h5>February</h5>
                          <input type="text" name="month-desc-feb" class="form-control" placeholder="Enter Activities for February" value="<?php echo $form_data['act_feb']['ref_value']?>">
                      </div>
                      <div class="col-sm-6">
                          <h5>March</h5>
                          <input type="text" name="month-desc-mar" class="form-control" placeholder="Enter Activities for March" value="<?php echo $form_data['act_mar']['ref_value']?>">
                      </div>
                      <div class="col-sm-6">
                          <h5>April</h5>
                          <input type="text" name="month-desc-apr" class="form-control" placeholder="Enter Activities for April" value="<?php echo $form_data['act_apr']['ref_value']?>">
                      </div>
                      <div class="col-sm-6">
                          <h5>May</h5>
                          <input type="text" name="month-desc-may" class="form-control" placeholder="Enter Activities for May" value="<?php echo $form_data['act_may']['ref_value']?>">
                      </div>
                      <div class="col-sm-6">
                          <h5>June</h5>
                          <input type="text" name="month-desc-jun" class="form-control" placeholder="Enter Activities for June" value="<?php echo $form_data['act_jun']['ref_value']?>">
                      </div>
                      <div class="col-sm-6">
                          <h5>July</h5>
                          <input type="text" name="month-desc-jul" class="form-control" placeholder="Enter Activities for July" value="<?php echo $form_data['act_jul']['ref_value']?>">
                      </div>
                      <div class="col-sm-6">
                          <h5>August</h5>
                          <input type="text" name="month-desc-aug" class="form-control" placeholder="Enter Activities for August" value="<?php echo $form_data['act_aug']['ref_value']?>">
                      </div>
                      <div class="col-sm-6">
                          <h5>September</h5>
                          <input type="text" name="month-desc-sep" class="form-control" placeholder="Enter Activities for September" value="<?php echo $form_data['act_sep']['ref_value']?>">
                      </div>
                      <div class="col-sm-6">
                          <h5>October</h5>
                          <input type="text" name="month-desc-oct" class="form-control" placeholder="Enter Activities for October" value="<?php echo $form_data['act_oct']['ref_value']?>">
                      </div>
                      <div class="col-sm-6">
                          <h5>November</h5>
                          <input type="text" name="month-desc-nov" class="form-control" placeholder="Enter Activities for November" value="<?php echo $form_data['act_nov']['ref_value']?>">
                      </div>
                      <div class="col-sm-6">
                          <h5>December</h5>
                          <input type="text" name="month-desc-dec" class="form-control" placeholder="Enter Activities for December" value="<?php echo $form_data['act_dec']['ref_value']?>">
                      </div>
                  </div>
                </form>
            </div>
        </div>
    </div>
  </div>


</div>
