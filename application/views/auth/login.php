<div class="container wrapper wrapper-content">
  <div class="row">

    <div class="col-lg-6 col-lg-offset-3">
      <div class="text-center">
          <h1 class="logo"><img class="responsive" src="<?php echo IMG_DIR?>Taysan_Seal.png"  style="width: 50%"></h1>
      </div>
      <div class="tabs-container">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-basic-login"> <i class="fa fa-unlock-alt"></i> Login Information</a></li>
            <li class=""><a data-toggle="tab" href="#tab-qr-login"><i class="fa fa-qrcode"></i> Login With QR</a></li>
        </ul>
        <div class="tab-content">
          <div id="tab-basic-login" class="tab-pane active">
            <form class="m-t" role="form" method="POST" action="<?php echo $action_url ?>">
                <input type="hidden" name="method" value="login">
                <div class="form-group">
                    <input type="text" name="user_email" class="form-control" placeholder="Email / Username" required="">
                </div>
                <div class="form-group">
                    <input type="password" name="user_password" class="form-control" placeholder="Password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                <a href="<?php echo DOMAIN?>users/register-resident" class="btn btn-success block full-width m-b">Register</a>
            </form>
          </div>
          <div id="tab-qr-login" class="tab-pane">
            <form class="m-t" role="form" method="POST" action="?">
              <input type="hidden" name="method" value="qrcode">
              <div class="form-group">
                <div class="well">
                  <i class="fa fa-search"></i> Place the cursor on the field and use qrcode reader to Scan ID to login
                </div>
                <input type="text" name="qr-code" class="form-control" placeholder="Scan QR Code Here" autocomplete="off">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
  $('[name=qr-code]').on('keyup', function (e) {
    if (e.keyCode == 13) {
        // alert($('[name=qr-code]').val())
    }
});
});
</script>
