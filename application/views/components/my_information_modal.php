<div class="modal inmodal fade" id="modal-my-info" role="dialog" data-no="" aria-hidden="true">
    <div class="modal-dialog modal-fs">
        <div class="modal-content">
            <div class="modal-body">
              <div class="row">
                <div class="col-md-4">
                  <img class="img-responsive" alt="image" src="<?php echo UPLOAD.'qrcode/'.$qr_image['fm_encypted_name'] ?>" style="width:100%">
                </div>
                <div class="col-md-8">
                  <h3><?php echo $basic_contact['bc_first_name'].' '.$basic_contact['bc_middle_name'].' '.$basic_contact['bc_last_name']; ?></h3>
                  <h4><?php echo $user_type; ?></h4>
                  <h4 class="text-capitalize"><?php echo $basic_contact['bc_gender']; ?></h4>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
