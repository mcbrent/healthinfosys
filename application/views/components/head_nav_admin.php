<div class="row border-bottom">
  <nav class="navbar" role="navigation" style="margin-bottom: 0">
  <div class="navbar-header">
      <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
  </div>
      <ul class="nav navbar-top-links navbar-right">

           <li class="dropdown">
               <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                   <img alt="image" class="text-image img-circle" src="<?php echo UPLOAD.'profile/'.(!empty($dp_image)? $dp_image['fm_encypted_name']: 'img_default.png') ?>">
               </a>
               <ul class="dropdown-menu dropdown-alerts nopadding">
                   <li>
                     <a class="text-dark" href="<?php echo DOMAIN?>home/dashboard">
                       <div class="text-center link-block">
                             <i class="fa fa-dashboard"></i> Dashboard
                       </div>
                     </a>
                   </li>
                   <li>
                     <a id="my-info" class="text-dark" href="javascript:void(0)" data-toggle="modal" data-target="#modal-my-info">
                       <div class="text-center link-block">
                             <i class="fa fa-id-card"></i> My Information
                       </div>
                     </a>
                   </li>
                   <li>
                     <a class="text-dark" href="<?php echo DOMAIN?>users/upload-image">
                       <div class="text-center link-block">
                             <i class="fa fa-file-image-o"></i> Change My Display Picture
                       </div>
                     </a>
                   </li>
                   <li>
                     <a class="text-dark" href="<?php echo DOMAIN?>logout">
                       <div class="text-center link-block">
                             <i class="fa fa-sign-out"></i> Change user
                       </div>
                     </a>
                   </li>
               </ul>
           </li>
       </ul>

  </nav>
</div>
<?php $this->load->view('components/my_information_modal') ?>
