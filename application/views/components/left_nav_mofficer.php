<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
          <li>
              <a href=""><i class="fa fa-user-md"></i> <span class="nav-label"> Appointment</span> <span class="fa arrow"></span></a>
              <ul class="nav nav-second-level collapse">
                  <li><a href="<?php echo DOMAIN; ?>appointment/create-appointment/walk-in">Schedule an appointment</a></li>
                  <li><a href="<?php echo DOMAIN; ?>appointment/request-appointment-list">Request Appointment List</a></li>
              </ul>
          </li>
          <li>
              <a href=""><i class="fa fa-user"></i> <span class="nav-label"> Profile</span> <span class="fa arrow"></span></a>
              <ul class="nav nav-second-level collapse">
                  <li><a href="<?php echo DOMAIN; ?>resident/my-profile">Edit Information</a></li>
              </ul>
          </li>
        </ul>

    </div>
</nav>
