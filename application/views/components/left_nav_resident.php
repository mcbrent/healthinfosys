<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
          <li>
              <a href=""><i class="fa fa-user-md"></i> <span class="nav-label"> Appointment</span> <span class="fa arrow"></span></a>
              <ul class="nav nav-second-level collapse">
                  <li><a href="<?php echo DOMAIN; ?>appointment/create-appointment/resident-sched">Request an appointment</a></li>
                  <li><a href="<?php echo DOMAIN; ?>appointment/resident-appointment-list">View Pending Appointment</a></li>
              </ul>
          </li>
          <li>
              <a href=""><i class="fa fa-user"></i> <span class="nav-label"> Profile</span> <span class="fa arrow"></span></a>
              <ul class="nav nav-second-level collapse">
                  <li><a href="<?php echo DOMAIN; ?>resident/my-profile">Edit Information</a></li>
              </ul>
          </li>
        </ul>

    </div>
</nav>
