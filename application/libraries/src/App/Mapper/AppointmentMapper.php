<?php
namespace App\Mapper;
use Sys\Mapper\Mapper;

class AppointmentMapper extends Mapper{

  protected $_table = 'tbl_appointment ';

  public function selectDataTable($filter, $columns, $limit, $offset, $order){
    $result = array(
      'data'  => array()
    , 'total_count'=>0
    , 'count'=>0
    );

    $order_str_query = "ORDER BY ";
    $limit_str_query = "LIMIT :limit OFFSET :offset";
    $column_str_query = "";
    $where_str_query = "";
    $params = array();

    $column_str_query = ' CONCAT(bc_last_name, \', \', bc_first_name, \' \', bc_middle_name, \' \', bc_name_ext) as health_worker,
                        bc_gender, TIMESTAMPDIFF(YEAR, 	bc_birthdate, CURDATE()) AS age,
                        user_name, brgy_name, user_email, bhws_id ';
    if(!empty($filter)){
        $where_str_query .= " user_type = 5 AND (bc_first_name LIKE :bc_first_name OR bc_middle_name LIKE :bc_middle_name
                            OR bc_last_name LIKE :bc_last_name OR bc_name_ext LIKE :bc_name_ext OR
                            brgy_name LIKE :brgy_name OR bc_gender LIKE :bc_gender OR TIMESTAMPDIFF(YEAR, resident_birthdate, CURDATE()) LIKE :age
                            OR username LIKE :username OR user_email LIKE :user_email ) ";
        $params[':bc_first_name'] = '%'.$filter.'%';
        $params[':bc_middle_name'] = '%'.$filter.'%';
        $params[':bc_last_name'] = '%'.$filter.'%';
        $params[':bc_name_ext'] = '%'.$filter.'%';
        $params[':brgy_name'] = '%'.$filter.'%';
        $params[':bc_gender'] = '%'.$filter.'%';
        $params[':age'] = '%'.$filter.'%';
        $params[':username'] = '%'.$filter.'%';
        $params[':user_email'] = '%'.$filter.'%';
    }

    foreach($order as $i=>$_order){
      $order_str_query .= $_order['col']." ".$_order['type'];
      if(next($order)){
        $order_str_query .= ", ";
      }
    }

    if(strlen($where_str_query)>0){
      $where_str_query = ' WHERE '.$where_str_query;
    }

    $sql_statement = "SELECT COUNT(1) as 'num'
                      FROM `tbl_bhws`
                      INNER JOIN `tbl_basic_contact`
                      ON `bhws_bc_id` = `bc_id`
                      INNER JOIN `tbl_user`
                      ON `bhws_user_id` = `user_id`
                      INNER JOIN `tbl_barangay`
                      ON `bhws_brgy_id` = `brgy_id` " . $where_str_query;
		$stmt = $this->prepare($sql_statement);
		$stmt->execute($params);
		$result['count'] = $stmt->fetch(\PDO::FETCH_ASSOC)['num'];

    $sql_statement = "SELECT ".$column_str_query."
                      FROM `tbl_bhws`
                      INNER JOIN `tbl_basic_contact`
                      ON `bhws_bc_id` = `bc_id`
                      INNER JOIN `tbl_user`
                      ON `bhws_user_id` = `user_id`
                      INNER JOIN `tbl_barangay`
                      ON `bhws_brgy_id` = `brgy_id` " . $where_str_query . " " . $order_str_query. " ".$limit_str_query;
		$stmt = $this->prepare($sql_statement);
    $params[':limit'] = $limit;
    $params[':offset'] = $offset;

		$stmt->execute($params);
		$result['data'] = $stmt->fetchAll(\PDO::FETCH_ASSOC);


    $result['total_count'] = $this->getAllCount();

		return $result;
  }


  public function getMobilityReportByDate($params){

    $sql_statement = '';
    if(isset($params['brgy'])){
        $sql_statement = "SELECT *
                          FROM
                          (SELECT `dt_id`, `dt_code`, `dt_name`, COUNT(`dt_id`) as total
                          FROM `tbl_appointment`
                          INNER JOIN `tbl_diagnosis_type`
                          ON `appointment_diagnosis_type` = `dt_id`
                          INNER JOIN `tbl_resident`
                          ON `appointment_resident_id` = `resident_id`
                          INNER JOIN `tbl_address`
                          ON `resident_address_id` = `address_id`
                          WHERE `appointment_status` = '5'
                          AND appointment_diagnosed_datetime BETWEEN :start_date AND :end_date
                          AND address_brgy_id = :address_brgy_id
                          GROUP BY `dt_id`, `dt_code`, `dt_name`, `address_brgy_id`
                          ORDER BY `total` DESC, `dt_name` ASC
                          ) tbl
                          ORDER BY tbl.`dt_name` ";
    }
    else{
        $sql_statement = "SELECT *
                          FROM
                          (SELECT `dt_id`, `dt_code`, `dt_name`, COUNT(`dt_id`) as total
                          FROM `tbl_appointment`
                          INNER JOIN `tbl_diagnosis_type`
                          ON `appointment_diagnosis_type` = `dt_id`
                          INNER JOIN `tbl_resident`
                          ON `appointment_resident_id` = `resident_id`
                          INNER JOIN `tbl_address`
                          ON `resident_address_id` = `address_id`
                          WHERE `appointment_status` = '5'
                          AND appointment_diagnosed_datetime BETWEEN :start_date AND :end_date
                          GROUP BY `dt_id`, `dt_code`, `dt_name`
                          ORDER BY `total` DESC, `dt_name` ASC
                          ) tbl
                          ORDER BY tbl.`dt_name` ";
    }


    $stmt = $this->prepare($sql_statement);
    $parameter = array(
          ':start_date' => $params['date_from']
        , ':end_date'   => $params['date_to']
    );
    if(isset($params['brgy'])){
        $parameter['address_brgy_id'] = $params['brgy'];
    }
	$stmt->execute(
$parameter
    );
		$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }

  public function getRequestedAppointmentList($params){

    $param_status = implode(", ",$params['type']);

    $sql_statement = "SELECT tbl_appointment.*, CONCAT(bc_last_name, ', ', bc_first_name, ' ', bc_middle_name, ' ', bc_name_ext) as resident,
                        bc_gender, TIMESTAMPDIFF(YEAR, 	bc_birthdate, CURDATE()) AS age,
                        brgy_name, user_name, resident_no
                      FROM `tbl_appointment`
                      INNER JOIN `tbl_resident`
                      ON `appointment_resident_id` = `resident_id`
                      INNER JOIN `tbl_basic_contact`
                      ON `resident_bc_id` = `bc_id`
                      INNER JOIN `tbl_address`
                      ON `resident_address_id` = `address_id`
                      INNER JOIN `tbl_barangay`
                      ON `address_brgy_id` = `brgy_id`
                      INNER JOIN `tbl_user`
                      ON `resident_user_id` = `user_id`
                      WHERE appointment_status IN (".$param_status.") ";

    $stmt = $this->prepare($sql_statement);
		$stmt->execute();
		$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }

  public function getDashboardStatus($date){
    $sql_statement = "SELECT
                        SUM(
                            CASE
                                WHEN appointment_status = '3' THEN 1
                            	ELSE 0
                        				END
                        ) as `Scheduled`,
                        SUM(
                            CASE
                                WHEN appointment_status = '4' THEN 1
                            	ELSE 0
                        				END
                        ) as `Waiting`,
                        SUM(
                            CASE
                                WHEN appointment_status = '5' THEN 1
                            	ELSE 0
                        				END
                        ) as `Completed`
                        FROM `tbl_appointment`
                        INNER JOIN `tbl_resident`
                        ON `appointment_resident_id` = `resident_id`
                        INNER JOIN `tbl_basic_contact`
                        ON `resident_bc_id` = `bc_id`
                        INNER JOIN `tbl_address`
                        ON `resident_address_id` = `address_id`
                        INNER JOIN `tbl_barangay`
                        ON `address_brgy_id` = `brgy_id`
                        INNER JOIN `tbl_user`
                        ON `resident_user_id` = `user_id`
                        WHERE appointment_status IN ('3', '4', '5') AND DATE(`appointment_approved_sched`) = :date_sched ";

    $stmt = $this->prepare($sql_statement);
		$stmt->execute(array(
      ':date_sched' => date('Y-m-d', strtotime($date))
    ));
		$result = $stmt->fetch(\PDO::FETCH_ASSOC);
    return $result;
  }

  public function getResidentAppointmentList($resident_id){
    $sql_statement = "SELECT *
                      FROM `tbl_appointment`
                      WHERE appointment_resident_id = :appointment_resident_id
                      ORDER BY `appointment_id` DESC";
    $params = array(
      'appointment_resident_id'  =>$resident_id
    );
		$stmt = $this->prepare($sql_statement);
		$stmt->execute($params);
		$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }

  public function getMedicalHistory($resident_id){
    $sql_statement = "SELECT appointment_id, appointment_diagnosed_datetime, dt_code, dt_name,
                      (
                      CASE
                      	WHEN mo_user.`user_type` = '1' THEN CONCAT(to_bc_admin.`bc_first_name`, ' ', to_bc_admin.`bc_middle_name`)
                          WHEN mo_user.`user_type` = '2' THEN CONCAT(to_bc_res.`bc_first_name`, ' ', to_bc_res.`bc_middle_name`)
                          WHEN mo_user.`user_type` = '3' THEN CONCAT(to_bc_pharmacist.`bc_first_name`, ' ', to_bc_pharmacist.`bc_middle_name`)
                          WHEN mo_user.`user_type` = '4' THEN CONCAT(to_bc_mo.`bc_first_name`, ' ', to_bc_mo.`bc_middle_name`)
                          WHEN mo_user.`user_type` = '5' THEN CONCAT(to_bc_bhws.`bc_first_name`, ' ', to_bc_bhws.`bc_middle_name`)
                      END
                      ) as 'medical_officer'
                      FROM `tbl_appointment`
                      INNER JOIN `tbl_resident` resident
                      ON resident.`resident_id` = `appointment_resident_id`
                      INNER JOIN `tbl_basic_contact` bc_resident
                      ON `resident_bc_id` = bc_resident.`bc_id`
                      INNER JOIN `tbl_diagnosis_type`
                      ON `appointment_diagnosis_type` = `dt_id`

                      INNER JOIN `tbl_user` mo_user
                      ON mo_user.`user_id` = `appointment_mo_id`

                      LEFT JOIN `tbl_admin` to_admin
                      ON to_admin.`admin_user_id` = mo_user.`user_id`
                      LEFT JOIN `tbl_basic_contact` to_bc_admin
                      ON to_admin.`admin_bc_id` = to_bc_admin.`bc_id`

                      LEFT JOIN `tbl_pharmacist` to_pharmacist
                      ON to_pharmacist.`pharmacist_user_id` = mo_user.`user_id`
                      LEFT JOIN `tbl_basic_contact` to_bc_pharmacist
                      ON to_pharmacist.`pharmacist_bc_id` = to_bc_pharmacist.`bc_id`

                      LEFT JOIN `tbl_medical_officer` to_mo
                      ON to_mo.`mo_user_id` = mo_user.`user_id`
                      LEFT JOIN `tbl_basic_contact` to_bc_mo
                      ON to_mo.`mo_bc_id` = to_bc_mo.`bc_id`

                      LEFT JOIN `tbl_resident` to_res
                      ON to_res.`resident_user_id` = mo_user.`user_id`
                      LEFT JOIN `tbl_basic_contact` to_bc_res
                      ON to_res.`resident_bc_id` = to_bc_res.`bc_id`

                      LEFT JOIN `tbl_bhws` to_bhws
                      ON to_bhws.`bhws_user_id` = mo_user.`user_id`
                      LEFT JOIN `tbl_basic_contact` to_bc_bhws
                      ON to_bhws.`bhws_bc_id` = to_bc_bhws.`bc_id`
                      WHERE appointment_resident_id = :appointment_resident_id
                      AND appointment_status = '5'
                      ORDER BY `appointment_id` DESC";
    $params = array(
      ':appointment_resident_id'  =>$resident_id
    );
		$stmt = $this->prepare($sql_statement);
		$stmt->execute($params);
		$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }


}
