<?php
namespace App\Mapper;
use Sys\Mapper\Mapper;

class UserMapper extends Mapper{

  protected $_table = 'tbl_user';

  public function selectByType($type, $filter){
    $sql_statement = '';
    $where_statement = 'WHERE (`bc_first_name` LIKE :bc_first_name OR `bc_middle_name` LIKE :bc_middle_name OR `bc_last_name` LIKE :bc_last_name OR `bc_name_ext` LIKE :bc_name_ext)';
    $params = array(
      ':bc_first_name'  => '%'.$filter.'%'
    , ':bc_middle_name'  => '%'.$filter.'%'
    , ':bc_last_name'  => '%'.$filter.'%'
    , ':bc_name_ext'  => '%'.$filter.'%'
    );
    switch($type){
      case "1": //Admin
      $sql_statement = "SELECT user_id, CONCAT(`bc_first_name`, ' ', `bc_middle_name`, ' ', `bc_last_name`, ' ', `bc_name_ext`) as user_fullname
                        FROM tbl_user
                        INNER JOIN `tbl_admin`
                        ON `admin_user_id` = `user_id`
                        INNER JOIN `tbl_basic_contact`
                        ON `admin_bc_id` = `bc_id`";
      break;
      case "2": //Resident
      $sql_statement = "SELECT user_id, CONCAT(`bc_first_name`, ' ', `bc_middle_name`, ' ', `bc_last_name`, ' ', `bc_name_ext`) as user_fullname
                        FROM tbl_user
                        INNER JOIN `tbl_resident`
                        ON `resident_user_id` = `user_id`
                        INNER JOIN `tbl_basic_contact`
                        ON `resident_bc_id` = `bc_id`";
      break;
      case "3": //Pharmacist
      $sql_statement = "SELECT user_id, CONCAT(`bc_first_name`, ' ', `bc_middle_name`, ' ', `bc_last_name`, ' ', `bc_name_ext`) as user_fullname
                        FROM tbl_user
                        INNER JOIN `tbl_pharmacist`
                        ON `pharmacist_user_id` = `user_id`
                        INNER JOIN `tbl_basic_contact`
                        ON `pharmacist_bc_id` = `bc_id`";
      break;
      case "4": //Medical Officer
      $sql_statement = "SELECT user_id, CONCAT(`bc_first_name`, ' ', `bc_middle_name`, ' ', `bc_last_name`, ' ', `bc_name_ext`) as user_fullname
                        FROM tbl_user
                        INNER JOIN `tbl_medical_officer`
                        ON `mo_user_id` = `user_id`
                        INNER JOIN `tbl_basic_contact`
                        ON `mo_bc_id` = `bc_id`";
      break;
      case "5": //Barangay Health Worker
      $sql_statement = "SELECT user_id, CONCAT(`bc_first_name`, ' ', `bc_middle_name`, ' ', `bc_last_name`, ' ', `bc_name_ext`) as user_fullname
                        FROM tbl_user
                        INNER JOIN `tbl_bhws`
                        ON `bhws_user_id` = `user_id`
                        INNER JOIN `tbl_basic_contact`
                        ON `bhws_bc_id` = `bc_id`";
      break;
    }

    $sql_statement .= ' '.$where_statement. ' ORDER BY user_fullname LIMIT 25 ';
		$stmt = $this->prepare($sql_statement);
		$stmt->execute($params);
		$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		return $result;
  }

  public function selectByLoginPassword($username_email, $password){
    $sql_statement = "SELECT *
                      FROM tbl_user
                      WHERE (user_email = :user_email OR user_name = :user_name) AND  user_password = :user_password";
		$stmt = $this->prepare($sql_statement);
		$stmt->execute(array(
      ':user_email'   => $username_email
    , ':user_name'   => $username_email
    , ':user_password'=> $password
    ));
		$result = $stmt->fetch(\PDO::FETCH_ASSOC);
		return $result;
  }

  public function selectByID($user_id){
    $sql_statement = "SELECT *
                      FROM tbl_user
                      WHERE user_id = :user_id";
		$stmt = $this->prepare($sql_statement);
		$stmt->execute(array(
      ':user_id'   => $user_id
    ));
		$result = $stmt->fetch(\PDO::FETCH_ASSOC);
		return $result;
  }

  public function generateKey(){
    return hash('crc32', bin2hex(openssl_random_pseudo_bytes(4)));
  }



}
