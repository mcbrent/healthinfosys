<?php
namespace App\Mapper;
use Sys\Mapper\Mapper;

class PharmacistMapper extends Mapper{

  protected $_table = 'tbl_pharmacist';

  public function selectDataTable($filter, $columns, $limit, $offset, $order){
    $result = array(
      'data'  => array()
    , 'total_count'=>0
    , 'count'=>0
    );

    $order_str_query = "ORDER BY ";
    $limit_str_query = "LIMIT :limit OFFSET :offset";
    $column_str_query = "";
    $where_str_query = "";
    $params = array();

    $column_str_query = ' CONCAT(bc_last_name, \', \', bc_first_name, \' \', bc_middle_name, \' \', bc_name_ext) as pharmacist,
                        bc_gender, TIMESTAMPDIFF(YEAR, 	bc_birthdate, CURDATE()) AS age,
                        user_name, user_email, pharmacist_id ';
    if(!empty($filter)){
        $where_str_query .= " user_type = 5 AND (bc_first_name LIKE :bc_first_name OR bc_middle_name LIKE :bc_middle_name
                            OR bc_last_name LIKE :bc_last_name OR bc_name_ext LIKE :bc_name_ext OR
                            bc_gender LIKE :bc_gender OR TIMESTAMPDIFF(YEAR, resident_birthdate, CURDATE()) LIKE :age
                            OR username LIKE :username OR user_email LIKE :user_email ) ";
        $params[':bc_first_name'] = '%'.$filter.'%';
        $params[':bc_middle_name'] = '%'.$filter.'%';
        $params[':bc_last_name'] = '%'.$filter.'%';
        $params[':bc_name_ext'] = '%'.$filter.'%';
        $params[':bc_gender'] = '%'.$filter.'%';
        $params[':age'] = '%'.$filter.'%';
        $params[':username'] = '%'.$filter.'%';
        $params[':user_email'] = '%'.$filter.'%';
    }

    foreach($order as $i=>$_order){
      $order_str_query .= $_order['col']." ".$_order['type'];
      if(next($order)){
        $order_str_query .= ", ";
      }
    }

    if(strlen($where_str_query)>0){
      $where_str_query = ' WHERE '.$where_str_query;
    }

    $sql_statement = "SELECT COUNT(1) as 'num'
                      FROM `tbl_pharmacist`
                      INNER JOIN `tbl_basic_contact`
                      ON `pharmacist_bc_id` = `bc_id`
                      INNER JOIN `tbl_user`
                      ON `pharmacist_user_id` = `user_id` " . $where_str_query;
		$stmt = $this->prepare($sql_statement);
		$stmt->execute($params);
		$result['count'] = $stmt->fetch(\PDO::FETCH_ASSOC)['num'];

    $sql_statement = "SELECT ".$column_str_query."
                      FROM `tbl_pharmacist`
                      INNER JOIN `tbl_basic_contact`
                      ON `pharmacist_bc_id` = `bc_id`
                      INNER JOIN `tbl_user`
                      ON `pharmacist_user_id` = `user_id` " . $where_str_query . " " . $order_str_query. " ".$limit_str_query;
		$stmt = $this->prepare($sql_statement);
    $params[':limit'] = $limit;
    $params[':offset'] = $offset;

		$stmt->execute($params);
		$result['data'] = $stmt->fetchAll(\PDO::FETCH_ASSOC);


    $result['total_count'] = $this->getAllCount();

		return $result;
  }

}
