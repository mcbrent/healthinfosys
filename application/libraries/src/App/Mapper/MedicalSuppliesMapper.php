<?php
namespace App\Mapper;
use Sys\Mapper\Mapper;

class MedicalSuppliesMapper extends Mapper{

  protected $_table = 'tbl_medical_supplies';

  public function getCurrentInventory(){
    $sql_statement = "SELECT *
                      FROM `tbl_medical_supplies`
                      ORDER BY `ms_description` ASC, ms_unit ASC ";
		$stmt = $this->prepare($sql_statement);
    $params = array();
		$stmt->execute($params);
		$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }

  public function selectDataTable($filter, $columns, $limit, $offset, $order){
    $result = array(
      'data'  => array()
    , 'total_count'=>0
    , 'count'=>0
    );

    $order_str_query = "ORDER BY ";
    $limit_str_query = "LIMIT :limit OFFSET :offset";
    $column_str_query = "";
    $where_str_query = "";
    $params = array();
    $column_str_query = ' 	ms_id, ms_description, ms_actual_stock, ms_unit ';
    if(!empty($filter)){
        $where_str_query .= " ms_description LIKE :ms_description OR ms_actual_stock LIKE :ms_actual_stock OR ms_unit LIKE :ms_unit ";
        $params[':ms_description'] = '%'.$filter.'%';
        $params[':ms_actual_stock'] = '%'.$filter.'%';
        $params[':ms_unit'] = '%'.$filter.'%';
    }

    foreach($order as $i=>$_order){
      $order_str_query .= $_order['col']." ".$_order['type'];
      if(next($order)){
        $order_str_query .= ", ";
      }
    }

    if(strlen($where_str_query)>0){
      $where_str_query = ' WHERE '.$where_str_query;
    }

    $sql_statement = "SELECT COUNT(1) as 'num'
                      FROM `tbl_medical_supplies` " . $where_str_query;
		$stmt = $this->prepare($sql_statement);
		$stmt->execute($params);
		$result['count'] = $stmt->fetch(\PDO::FETCH_ASSOC)['num'];

    $sql_statement = "SELECT ".$column_str_query."
                      FROM `tbl_medical_supplies` " . $where_str_query . " " . $order_str_query. " ".$limit_str_query;
		$stmt = $this->prepare($sql_statement);
    $params[':limit'] = $limit;
    $params[':offset'] = $offset;

		$stmt->execute($params);
		$result['data'] = $stmt->fetchAll(\PDO::FETCH_ASSOC);


    $result['total_count'] = $this->getAllCount();

		return $result;
  }

  public function selectDataTableHistory($filter, $columns, $limit, $offset, $order){
    $result = array(
      'data'  => array()
    , 'total_count'=>0
    , 'count'=>0
    );

    $order_str_query = "ORDER BY ";
    $limit_str_query = "LIMIT :limit OFFSET :offset";
    $column_str_query = "";
    $where_str_query = "";
    $params = array();
    $column_str_query = ' 	ms_id, ms_description, ms_actual_stock, ms_unit ';
    if(!empty($filter)){
        // $where_str_query .= " From LIKE :From OR To LIKE :To OR sa_date LIKE :sa_date ";
        // $params[':From'] = '%'.$filter.'%';
        // $params[':To'] = '%'.$filter.'%';
        // $params[':sa_date'] = '%'.$filter.'%';
    }

    foreach($order as $i=>$_order){
      $order_str_query .= $_order['col']." ".$_order['type'];
      if(next($order)){
        $order_str_query .= ", ";
      }
    }

    if(strlen($where_str_query)>0){
      $where_str_query = ' WHERE '.$where_str_query;
    }

    $sql_statement = "SELECT COUNT(1) as 'num'
                      FROM `tbl_supplies_allocation` " . $where_str_query;
		$stmt = $this->prepare($sql_statement);
		$stmt->execute($params);
		$result['count'] = $stmt->fetch(\PDO::FETCH_ASSOC)['num'];

    $sql_statement = "SELECT sa_id, sa_date,
                      (
                      CASE
                          WHEN from_user.`user_type` = '1' THEN CONCAT(from_bc_admin.`bc_first_name`, ' ', from_bc_admin.`bc_middle_name`)
                          WHEN from_user.`user_type` = '3' THEN CONCAT(from_bc_pharmacist.`bc_first_name`, ' ', from_bc_pharmacist.`bc_middle_name`)
                          WHEN from_user.`user_type` = '4' THEN CONCAT(from_bc_mo.`bc_first_name`, ' ', from_bc_mo.`bc_middle_name`)
                      END
                      ) as 'from_name',
                      (
                      CASE
                        WHEN from_user.`user_type` = '1' THEN 'Administrator'
                        WHEN from_user.`user_type` = '2' THEN 'Resident'
                        WHEN from_user.`user_type` = '3' THEN 'Pharmacist'
                        WHEN from_user.`user_type` = '4' THEN 'Medical Officer'
                        WHEN from_user.`user_type` = '5' THEN 'Barangay Health Worker'
                      END
                      ) as 'from_type',
                      (
                      CASE
                          WHEN to_user.`user_type` = '1' THEN CONCAT(to_bc_admin.`bc_first_name`, ' ', to_bc_admin.`bc_middle_name`)
                          WHEN to_user.`user_type` = '2' THEN CONCAT(to_bc_res.`bc_first_name`, ' ', to_bc_res.`bc_middle_name`)
                          WHEN to_user.`user_type` = '3' THEN CONCAT(to_bc_pharmacist.`bc_first_name`, ' ', to_bc_pharmacist.`bc_middle_name`)
                          WHEN to_user.`user_type` = '4' THEN CONCAT(to_bc_mo.`bc_first_name`, ' ', to_bc_mo.`bc_middle_name`)
                          WHEN to_user.`user_type` = '5' THEN CONCAT(to_bc_mo.`bc_first_name`, ' ', to_bc_mo.`bc_middle_name`)
                      END
                    ) as 'to_name',
                      (
                      CASE
                        WHEN to_user.`user_type` = '1' THEN 'Administrator'
                        WHEN to_user.`user_type` = '2' THEN 'Resident'
                        WHEN to_user.`user_type` = '3' THEN 'Pharmacist'
                        WHEN to_user.`user_type` = '4' THEN 'Medical Officer'
                        WHEN to_user.`user_type` = '5' THEN 'Barangay Health Worker'
                      END
                    ) as 'to_type'
                      FROM `tbl_supplies_allocation`
                      INNER JOIN `tbl_user` from_user
                      ON from_user.`user_id` = `sa_from_user_id`

                      LEFT JOIN `tbl_admin` from_admin
                      ON from_admin.`admin_user_id` = from_user.`user_id`
                      LEFT JOIN `tbl_basic_contact` from_bc_admin
                      ON from_admin.`admin_bc_id` = from_bc_admin.`bc_id`

                      LEFT JOIN `tbl_pharmacist` from_pharmacist
                      ON from_pharmacist.`pharmacist_user_id` = from_user.`user_id`
                      LEFT JOIN `tbl_basic_contact` from_bc_pharmacist
                      ON from_pharmacist.`pharmacist_bc_id` = from_bc_pharmacist.`bc_id`

                      LEFT JOIN `tbl_medical_officer` from_mo
                      ON from_mo.`mo_user_id` = from_user.`user_id`
                      LEFT JOIN `tbl_basic_contact` from_bc_mo
                      ON from_mo.`mo_bc_id` = from_bc_mo.`bc_id`

                      LEFT JOIN `tbl_bhws` from_bhws
                      ON from_bhws.`bhws_user_id` = from_user.`user_id`
                      LEFT JOIN `tbl_basic_contact` from_bc_bhws
                      ON from_bhws.`bhws_bc_id` = from_bc_bhws.`bc_id`


                      INNER JOIN `tbl_user` to_user
                      ON to_user.`user_id` = `sa_to_user_id`

                      LEFT JOIN `tbl_admin` to_admin
                      ON to_admin.`admin_user_id` = to_user.`user_id`
                      LEFT JOIN `tbl_basic_contact` to_bc_admin
                      ON to_admin.`admin_bc_id` = to_bc_admin.`bc_id`

                      LEFT JOIN `tbl_pharmacist` to_pharmacist
                      ON to_pharmacist.`pharmacist_user_id` = to_user.`user_id`
                      LEFT JOIN `tbl_basic_contact` to_bc_pharmacist
                      ON to_pharmacist.`pharmacist_bc_id` = to_bc_pharmacist.`bc_id`

                      LEFT JOIN `tbl_medical_officer` to_mo
                      ON to_mo.`mo_user_id` = to_user.`user_id`
                      LEFT JOIN `tbl_basic_contact` to_bc_mo
                      ON to_mo.`mo_bc_id` = to_bc_mo.`bc_id`

                      LEFT JOIN `tbl_resident` to_res
                      ON to_res.`resident_user_id` = to_user.`user_id`
                      LEFT JOIN `tbl_basic_contact` to_bc_res
                      ON to_res.`resident_bc_id` = to_bc_res.`bc_id`

                      LEFT JOIN `tbl_bhws` to_bhws
                      ON to_bhws.`bhws_user_id` = to_user.`user_id`
                      LEFT JOIN `tbl_basic_contact` to_bc_bhws
                      ON to_bhws.`bhws_bc_id` = to_bc_bhws.`bc_id`
                       " . $where_str_query . " " . $order_str_query. " ".$limit_str_query;
		$stmt = $this->prepare($sql_statement);
    $params[':limit'] = $limit;
    $params[':offset'] = $offset;

		$stmt->execute($params);
		$result['data'] = $stmt->fetchAll(\PDO::FETCH_ASSOC);


    $result['total_count'] = $this->getAllCount();

		return $result;
  }

}
