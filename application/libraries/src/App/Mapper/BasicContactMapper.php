<?php
namespace App\Mapper;
use Sys\Mapper\Mapper;

class BasicContactMapper extends Mapper{

  protected $_table = 'tbl_basic_contact';

  public function getByID($bc_id){
    $sql_statement = "SELECT *
                      FROM tbl_basic_contact
                      WHERE bc_id = :bc_id";
		$stmt = $this->prepare($sql_statement);
		$stmt->execute(array(
      ':bc_id'   => $bc_id
    ));
		$result = $stmt->fetch(\PDO::FETCH_ASSOC);
		return $result;
  }

  public function selectDataTable($filter, $columns, $limit, $offset, $order){
    $result = array(
      'data'  => array()
    , 'total_count'=>0
    , 'count'=>0
    );

    $order_str_query = "ORDER BY ";
    $limit_str_query = "LIMIT :limit OFFSET :offset";
    $column_str_query = "";
    $where_str_query = " user_type = 2 ";
    $params = array();

    $column_str_query = "user.*,
                        (CASE
                        	WHEN admin.bc_first_name IS NOT NULL THEN admin.bc_first_name
                         	WHEN resident.bc_first_name IS NOT NULL THEN resident.bc_first_name
                        	WHEN pharmacist.bc_first_name IS NOT NULL THEN pharmacist.bc_first_name
                        	WHEN medical_officer.bc_first_name IS NOT NULL THEN medical_officer.bc_first_name
                        	WHEN bhws.bc_first_name IS NOT NULL THEN bhws.bc_first_name
                        	ELSE ''
                        END) AS `first_name`,
                        (CASE
                        	WHEN admin.bc_middle_name IS NOT NULL THEN admin.bc_middle_name
                         	WHEN resident.bc_middle_name IS NOT NULL THEN resident.bc_middle_name
                        	WHEN pharmacist.bc_middle_name IS NOT NULL THEN pharmacist.bc_middle_name
                        	WHEN medical_officer.bc_middle_name IS NOT NULL THEN medical_officer.bc_middle_name
                        	WHEN bhws.bc_middle_name IS NOT NULL THEN bhws.bc_middle_name
                        	ELSE ''
                        END) AS `middle_name`,
                        (CASE
                        	WHEN admin.bc_last_name IS NOT NULL THEN admin.bc_last_name
                         	WHEN resident.bc_last_name IS NOT NULL THEN resident.bc_last_name
                        	WHEN pharmacist.bc_last_name IS NOT NULL THEN pharmacist.bc_last_name
                        	WHEN medical_officer.bc_last_name IS NOT NULL THEN medical_officer.bc_last_name
                        	WHEN bhws.bc_last_name IS NOT NULL THEN bhws.bc_last_name
                        	ELSE ''
                        END) AS `last_name`,
                        (CASE
                        	WHEN user_type = 1 THEN 'Administrator'
                         	WHEN user_type = 2 THEN 'Resident'
                         	WHEN user_type = 3 THEN 'Pharmacist'
                         	WHEN user_type = 4 THEN 'Medical Officer'
                         	WHEN user_type = 5 THEN 'Barangay Health Worker'
                        	ELSE ''
                        END) AS `user_type`, user_id ";
    if(!empty($filter)){
        $where_str_query .= " AND (
                            admin.bc_first_name LIKE :filter_1 OR
                            resident.bc_first_name LIKE :filter_2 OR
                            pharmacist.bc_first_name LIKE :filter_3 OR
                            medical_officer.bc_first_name LIKE :filter_4 OR
                            admin.bc_first_name LIKE :filter_5 OR

                            admin.bc_middle_name LIKE :filter_6 OR
                            resident.bc_middle_name LIKE :filter_7 OR
                            pharmacist.bc_middle_name LIKE :filter_8 OR
                            medical_officer.bc_middle_name LIKE :filter_9 OR
                            admin.bc_middle_name LIKE :filter_10 OR

                            admin.bc_last_name LIKE :filter_11 OR
                            resident.bc_last_name LIKE :filter_12 OR
                            pharmacist.bc_last_name LIKE :filter_13 OR
                            medical_officer.bc_last_name LIKE :filter_14 OR
                            admin.bc_last_name LIKE :filter_15
                            )";
        $params[':filter_1'] = '%'.$filter.'%';
        $params[':filter_2'] = '%'.$filter.'%';
        $params[':filter_3'] = '%'.$filter.'%';
        $params[':filter_4'] = '%'.$filter.'%';
        $params[':filter_5'] = '%'.$filter.'%';

        $params[':filter_6'] = '%'.$filter.'%';
        $params[':filter_7'] = '%'.$filter.'%';
        $params[':filter_8'] = '%'.$filter.'%';
        $params[':filter_9'] = '%'.$filter.'%';
        $params[':filter_10'] = '%'.$filter.'%';

        $params[':filter_11'] = '%'.$filter.'%';
        $params[':filter_12'] = '%'.$filter.'%';
        $params[':filter_13'] = '%'.$filter.'%';
        $params[':filter_14'] = '%'.$filter.'%';
        $params[':filter_15'] = '%'.$filter.'%';
    }

    foreach($order as $i=>$_order){
      $order_str_query .= $_order['col']." ".$_order['type'];
      if(next($order)){
        $order_str_query .= ", ";
      }
    }

    if(strlen($where_str_query)>0){
      $where_str_query = ' WHERE '.$where_str_query;
    }

    $sql_statement = "SELECT COUNT(1) as 'num'
                      FROM `tbl_user` user
                      LEFT JOIN `tbl_admin`
                      ON `admin_user_id` = `user_id`
                      LEFT JOIN `tbl_basic_contact` admin
                      ON `admin_bc_id` = admin.`bc_id`
                      LEFT JOIN `tbl_resident`
                      ON `resident_user_id` = `user_id`
                      LEFT JOIN `tbl_basic_contact` resident
                      ON `resident_bc_id` = resident.`bc_id`
                      LEFT JOIN `tbl_pharmacist`
                      ON `pharmacist_user_id` = `user_id`
                      LEFT JOIN `tbl_basic_contact` pharmacist
                      ON `pharmacist_bc_id` = pharmacist.`bc_id`
                      LEFT JOIN `tbl_medical_officer`
                      ON `mo_user_id` = `user_id`
                      LEFT JOIN `tbl_basic_contact` medical_officer
                      ON `mo_bc_id` = medical_officer.`bc_id`
                      LEFT JOIN `tbl_bhws`
                      ON `bhws_user_id` = `user_id`
                      LEFT JOIN `tbl_basic_contact` bhws
                      ON `bhws_bc_id` = bhws.`bc_id` " . $where_str_query;
		$stmt = $this->prepare($sql_statement);
		$stmt->execute($params);
		$result['count'] = $stmt->fetch(\PDO::FETCH_ASSOC)['num'];

    $sql_statement = "SELECT ".$column_str_query."
                      FROM `tbl_user` user
                      LEFT JOIN `tbl_admin`
                      ON `admin_user_id` = `user_id`
                      LEFT JOIN `tbl_basic_contact` admin
                      ON `admin_bc_id` = admin.`bc_id`
                      LEFT JOIN `tbl_resident`
                      ON `resident_user_id` = `user_id`
                      LEFT JOIN `tbl_basic_contact` resident
                      ON `resident_bc_id` = resident.`bc_id`
                      LEFT JOIN `tbl_pharmacist`
                      ON `pharmacist_user_id` = `user_id`
                      LEFT JOIN `tbl_basic_contact` pharmacist
                      ON `pharmacist_bc_id` = pharmacist.`bc_id`
                      LEFT JOIN `tbl_medical_officer`
                      ON `mo_user_id` = `user_id`
                      LEFT JOIN `tbl_basic_contact` medical_officer
                      ON `mo_bc_id` = medical_officer.`bc_id`
                      LEFT JOIN `tbl_bhws`
                      ON `bhws_user_id` = `user_id`
                      LEFT JOIN `tbl_basic_contact` bhws
                      ON `bhws_bc_id` = bhws.`bc_id` " . $where_str_query . " " . $order_str_query. " ".$limit_str_query;
		$stmt = $this->prepare($sql_statement);
    $params[':limit'] = $limit;
    $params[':offset'] = $offset;

		$stmt->execute($params);
		$result['data'] = $stmt->fetchAll(\PDO::FETCH_ASSOC);


    $result['total_count'] = $this->getAllCount();

		return $result;
  }
}
