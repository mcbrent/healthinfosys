<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller extends CI_Controller {

	protected $_data = array();
	protected $sess = null;
	protected $is_secure = false;
	protected $_header = array(
		'title' => SITE_NAME
	);
	protected $_template = 'templates/public';
	public function __construct(){
		parent::__construct();
		$this->sess = new Sys\Sess\SessionLoad();
	}
	protected function set_alert($config){
		$_SESSION['alert_message'] = array(
			'message'	=> $config['message']
		,	'type'		=> $config['type']);
		if(isset($config['href'])){
			$_SESSION['alert_message']['link'] = array(
					'href'=>$config['href']
				,	'text'=>$config['text']
				);
		}

	}


	protected function view($file){
		if($this->is_secure){
			if($this->sess->isLogin()){
				$content = array(
					'content'=> $this->load->view($file, $this->_data, true)
				);
				switch($_SESSION['current_user']['type']){
					case 1:
						$this->_template = 'templates/admin_main';
					break;
					case 2:
						$this->_template = 'templates/resident_main';
					break;
					case 3:
						$this->_template = 'templates/pharmacist_main';
					break;
					case 4:
						$this->_template = 'templates/mofficer_main';
					break;
					case 5:
						$this->_template = 'templates/bhws_main';
					break;
					default:

				}
				$content['my_info'] = $this->load_my_information($_SESSION['current_user']['id']);
				$this->load->view('components/header', $this->_header);
				$this->load->view($this->_template, $content);
				$this->load->view('components/footer');
			}
			else{
				$this->redirect(DOMAIN.'login'."?redirect_url=".str_replace('/index.php', '', $_SERVER['REQUEST_URI']));
			}
		}
		else{
			$content = array(
				'content'=> $this->load->view($file, $this->_data, true)
			);
			$this->load->view('components/header', $this->_header);
			$this->load->view($this->_template, $content);
			$this->load->view('components/footer');
		}
	}

	public function redirect($url, $permanent = false){

		header('Location: ' . $url, true, $permanent ? 301 : 302);
		exit();
	}

	private function load_my_information($user_id){
		$my_info = array();
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$userMapper = new App\Mapper\UserMapper();
		$adminMapper = new App\Mapper\AdminMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$pharmacistMapper = new App\Mapper\PharmacistMapper();
		$medicalOfficerMapper = new App\Mapper\MedicalOfficerMapper();
		$barangayMapper = new App\Mapper\BarangayMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$barangayHealthWorkerMapper = new App\Mapper\BarangayHealthWorkerMapper();

		$user = $userMapper->getByFilter("user_id = '".$user_id."'", true);

		if($user['user_type'] == '1'){
			$admin = $adminMapper->getByFilter("admin_user_id = '".$user['user_id']."'", true);
			$basicContact = $basicContactMapper->getByFilter("bc_id = '".$admin['admin_bc_id']."'", true);
			$my_info['user_type'] = 'Administrator';
		}
		if($user['user_type'] == '2'){
			$resident = $residentMapper->getByFilter("resident_user_id = '".$user['user_id']."'", true);
			$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
			$my_info['user_type'] = 'Resident';
		}
		if($user['user_type'] == '3'){
			$pharmacist = $pharmacistMapper->getByFilter("pharmacist_user_id = '".$user['user_id']."'", true);
			$basicContact = $basicContactMapper->getByFilter("bc_id = '".$pharmacist['pharmacist_bc_id']."'", true);
			$my_info['user_type'] = 'Pharmacist';
		}
		if($user['user_type'] == '4'){
			$medicalOfficer = $medicalOfficerMapper->getByFilter("mo_user_id = '".$user['user_id']."'", true);
			$basicContact = $basicContactMapper->getByFilter("bc_id = '".$medicalOfficer['mo_bc_id']."'", true);
			$my_info['user_type'] = 'Medical Officer';
		}
		if($user['user_type'] == '5'){
			$barangayHealthWorker = $barangayHealthWorkerMapper->getByFilter("bhws_user_id = '".$user['user_id']."'", true);
			$basicContact = $basicContactMapper->getByFilter("bc_id = '".$barangayHealthWorker['bhws_bc_id']."'", true);
			$my_info['user_type'] = 'Barangay Health Worker';
		}


		// if()
		// //$resident = $residentMapper->getByFilter("resident_id = '".$resident_id."'", true);
		//
		// $basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
		// $address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
		// $brgy = $barangayMapper->getByFilter("brgy_id = '".$address['address_brgy_id']."'", true);
		$qrImage = $fileManagerMapper->getByFilter("fm_id = '".$user['user_qr_fm_id']."'", true);
		$dpImage = $fileManagerMapper->getByFilter("fm_id = '".$user['user_dp_fm_id']."'", true);
		$my_info['qr_image'] = $qrImage;
		$my_info['dp_image'] = (!empty($dpImage))? $dpImage : array();
		$my_info['basic_contact'] = $basicContact;

		return $my_info;
	}


	public function __destruct(){
	}

}
