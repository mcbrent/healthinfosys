<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Utility extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function id_list_ref()
    {
        $limit = $_POST['length'];
        $offset = $_POST['start'];
        $search = $_POST['search'];
        $columns = $_POST['columns'];
        $orders = array();

        foreach ($_POST['order'] as $_order) {
            array_push($orders, array(
                'col' => $_POST['columns'][$_order['column']]['data']
            ,	'type' => $_order['dir']
            ));
        }
        $mapper = new App\Mapper\BasicContactMapper();
        $result = $mapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders);

        echo json_encode($result);
    }

    public function print_id($user_id)
    {
        $data = array();

        $basicContactMapper = new App\Mapper\BasicContactMapper();
        $userMapper = new App\Mapper\UserMapper();
        $adminMapper = new App\Mapper\AdminMapper();
        $addressMapper = new App\Mapper\AddressMapper();
        $residentMapper = new App\Mapper\ResidentMapper();
        $pharmacistMapper = new App\Mapper\PharmacistMapper();
        $medicalOfficerMapper = new App\Mapper\MedicalOfficerMapper();
        $barangayMapper = new App\Mapper\BarangayMapper();
        $fileManagerMapper = new App\Mapper\FileManagerMapper();
        $barangayHealthWorkerMapper = new App\Mapper\BarangayHealthWorkerMapper();

        $user = $userMapper->getByFilter("user_id = '".$user_id."'", true);

        if ($user['user_type'] == '1') {
            $admin = $adminMapper->getByFilter("admin_user_id = '".$user['user_id']."'", true);
            $basicContact = $basicContactMapper->getByFilter("bc_id = '".$admin['admin_bc_id']."'", true);
            $data['user_type'] = 'Administrator';
        }
        if ($user['user_type'] == '2') {
            $resident = $residentMapper->getByFilter("resident_user_id = '".$user['user_id']."'", true);
            $basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
            $data['user_type'] = 'Resident';
        }
        if ($user['user_type'] == '3') {
            $pharmacist = $pharmacistMapper->getByFilter("pharmacist_user_id = '".$user['user_id']."'", true);
            $basicContact = $basicContactMapper->getByFilter("bc_id = '".$pharmacist['pharmacist_bc_id']."'", true);
            $data['user_type'] = 'Pharmacist';
        }
        if ($user['user_type'] == '4') {
            $medicalOfficer = $medicalOfficerMapper->getByFilter("mo_user_id = '".$user['user_id']."'", true);
            $basicContact = $basicContactMapper->getByFilter("bc_id = '".$medicalOfficer['mo_bc_id']."'", true);
            $data['user_type'] = 'Medical Officer';
        }
        if ($user['user_type'] == '5') {
            $barangayHealthWorker = $barangayHealthWorkerMapper->getByFilter("bhws_user_id = '".$user['user_id']."'", true);
            $basicContact = $basicContactMapper->getByFilter("bc_id = '".$barangayHealthWorker['bhws_bc_id']."'", true);
            $data['user_type'] = 'Barangay Health Worker';
        }

        $qrImage = $fileManagerMapper->getByFilter("fm_id = '".$user['user_qr_fm_id']."'", true);
        $dpImage = $fileManagerMapper->getByFilter("fm_id = '".$user['user_dp_fm_id']."'", true);

        $data['user'] = $user;
        $data['qr_image'] = $qrImage;
        $data['dp_image'] = ($dpImage)? $dpImage['fm_encypted_name'] : 'img_default.png';
        $data['basic_contact'] = $basicContact;
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // die();


        $html = $this->load->view('utility/printing/print_id', $data, true);
        $this->load->library('MPdf');
        $this->mpdf->generate(
            array(	'format' => 'Letter',
                    'orientation' => 'P',
                    'format' => array(86,110),
                    'margin_left' => 0,
                    'margin_right' => 0,
                    'margin_top' => 0,
                    'margin_bottom' => 0,
                    'margin_head' => 0,
                    'margin_foot' => 0,
                    'has_footer' => false,
                    'has_header' => false,
                    'title' => 'ID',
                    'html' => $html,
                    'is_create' => false
            )
        );
    }

    public function print_referral($user_id)
    {
        $data = array();

        $basicContactMapper = new App\Mapper\BasicContactMapper();
        $userMapper = new App\Mapper\UserMapper();
        $adminMapper = new App\Mapper\AdminMapper();
        $addressMapper = new App\Mapper\AddressMapper();
        $residentMapper = new App\Mapper\ResidentMapper();
        $pharmacistMapper = new App\Mapper\PharmacistMapper();
        $medicalOfficerMapper = new App\Mapper\MedicalOfficerMapper();
        $barangayMapper = new App\Mapper\BarangayMapper();
        $fileManagerMapper = new App\Mapper\FileManagerMapper();
        $barangayHealthWorkerMapper = new App\Mapper\BarangayHealthWorkerMapper();

        $user = $userMapper->getByFilter("user_id = '".$user_id."'", true);
        $resident = array();
        $basicContact = array();
        $address = array();
        if ($user['user_type'] == '2') {
            $resident = $residentMapper->getByFilter("resident_user_id = '".$user['user_id']."'", true);
            $basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
            $address = $addressMapper->getCompleteAddressByID($resident['resident_address_id']);
        }
        $data['resident'] = $resident;
        $data['basic_contact'] = $basicContact;
        $data['address'] = $address;

        $html = $this->load->view('utility/referral/referral_slip', $data, true);
        $this->load->library('MPdf');
        $this->mpdf->generate(
            array(	'format' => 'Legal',
                    'orientation' => 'P',
                    'title' => 'Referral Slip',
                    'html' => $html,
                    'is_create' => false
            )
        );
    }

    public function id_printing()
    {
        $this->is_secure = true;
        $this->view('utility/printing/list');
    }

    public function announcement_table()
    {
        $limit = isset($_POST['length'])? $_POST['length'] : '0';
        $offset = isset($_POST['start'])? $_POST['start'] : '0';
        $condition = isset($_POST['condition'])? $_POST['condition'] : array();
        $search = $_POST['search'];
        $columns = $_POST['columns'];
        $order = isset($_POST['order'])? $_POST['order'] : array();
        $orders = array();

        foreach ($order as $_order) {
            array_push($orders, array(
                'col' => $columns[$_order['column']]['data']
            ,	'type' => $_order['dir']
            ));
        }
        $announcementMapper = new App\Mapper\AnnouncementMapper();

        $result = $announcementMapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders, $condition);
        echo json_encode($result);
    }



    public function announcement()
    {
        $this->is_secure = true;
        $this->view('utility/announcement/list');
    }

    public function add_announcement()
    {
        $announcementMapper = new App\Mapper\AnnouncementMapper();
        $adminMapper = new App\Mapper\AdminMapper();
        $this->_template = 'templates/admin_main';
        $data = array(
                'announcement_title' => ''
            ,	'announcement_content' => ''
        );
        if (!empty($_POST)) {
            $admin = $adminMapper->getByID($_SESSION['current_user']['id']);
            $insert_data = array();
            $insert_data['announcement_title'] = $_POST['announcement-title'];
            $insert_data['announcement_content'] = $_POST['announcement-content'];
            $insert_data['announcement_admin_id'] = $admin['admin_id'];
            $announcementMapper->insert($insert_data);
        }
        $this->_data['action'] = 'add';
        $this->_data['form_data'] = $data;
		$this->is_secure = true;
        $this->view('utility/announcement/form');
    }

    public function edit_announcement($id)
    {
        $announcementMapper = new App\Mapper\AnnouncementMapper();

        if (!empty($_POST)) {
            $update_data = array();
            $update_data['announcement_title'] = $_POST['announcement-title'];
            $update_data['announcement_content'] = $_POST['announcement-content'];
            $announcementMapper->update($update_data, " annoucement_id ='".$id."'");
        }
        $announcement = $announcementMapper->getByID($id);
        if (empty($school));//Show 404
        $this->_data['action'] = 'edit';
        $this->_data['form_data'] = $announcement;

        $this->is_secure = true;
        $this->view('utility/announcement/form');
    }

    public function delete_announcement()
    {
        $announcementMapper = new App\Mapper\AnnouncementMapper();
        $id = $_POST['id'];
        $announcementMapper->delete("annoucement_id = '".$id."'");
        echo json_encode(array('success' => true));
    }

    public function calendar(){
        $referenceMapper = new App\Mapper\ReferenceMapper();
        if (!empty($_POST)) {
            $referenceMapper->update(array('ref_value'=>$_POST['month-desc-jan']), "ref_key = 'JAN'");
            $referenceMapper->update(array('ref_value'=>$_POST['month-desc-feb']), "ref_key = 'FEB'");
            $referenceMapper->update(array('ref_value'=>$_POST['month-desc-mar']), "ref_key = 'MAR'");
            $referenceMapper->update(array('ref_value'=>$_POST['month-desc-apr']), "ref_key = 'APR'");
            $referenceMapper->update(array('ref_value'=>$_POST['month-desc-may']), "ref_key = 'MAY'");
            $referenceMapper->update(array('ref_value'=>$_POST['month-desc-jun']), "ref_key = 'JUN'");
            $referenceMapper->update(array('ref_value'=>$_POST['month-desc-jul']), "ref_key = 'JUL'");
            $referenceMapper->update(array('ref_value'=>$_POST['month-desc-aug']), "ref_key = 'AUG'");
            $referenceMapper->update(array('ref_value'=>$_POST['month-desc-sep']), "ref_key = 'SEP'");
            $referenceMapper->update(array('ref_value'=>$_POST['month-desc-oct']), "ref_key = 'OCT'");
            $referenceMapper->update(array('ref_value'=>$_POST['month-desc-nov']), "ref_key = 'NOV'");
            $referenceMapper->update(array('ref_value'=>$_POST['month-desc-dec']), "ref_key = 'DEC'");
        }
        $form_data = array(
            'act_jan' => $referenceMapper->getByFilter("ref_key = 'JAN'", true),
            'act_feb' => $referenceMapper->getByFilter("ref_key = 'FEB'", true),
            'act_mar' => $referenceMapper->getByFilter("ref_key = 'MAR'", true),
            'act_apr' => $referenceMapper->getByFilter("ref_key = 'APR'", true),
            'act_may' => $referenceMapper->getByFilter("ref_key = 'MAY'", true),
            'act_jun' => $referenceMapper->getByFilter("ref_key = 'JUN'", true),
            'act_jul' => $referenceMapper->getByFilter("ref_key = 'JUL'", true),
            'act_aug' => $referenceMapper->getByFilter("ref_key = 'AUG'", true),
            'act_sep' => $referenceMapper->getByFilter("ref_key = 'SEP'", true),
            'act_oct' => $referenceMapper->getByFilter("ref_key = 'OCT'", true),
            'act_nov' => $referenceMapper->getByFilter("ref_key = 'NOV'", true),
            'act_dec' => $referenceMapper->getByFilter("ref_key = 'DEC'", true)
        );

        $this->_data['form_data'] = $form_data;
        $this->is_secure = true;
        $this->view('utility/calendar/form');
    }
}
