<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends Controller {
	private $_default_province_id = 24;
	private $_default_city_id = 474;
	public function __construct(){
		parent::__construct();
	}

	public function search($z){
		$usermapper = new App\Mapper\UserMapper();
	}

	public function upload_image(){
		$current_user = $_SESSION['current_user']['id'];
		$userMapper = new App\Mapper\UserMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$user = $userMapper->getByFilter("user_id = '". $current_user."' ", true);

		if(!empty($_FILES)){
			$this->load->model('FileManagement/Upload_Model');
			if($_FILES['applicant-img']["error"] == 0){
				$result = $this->Upload_Model->upload_profile_image($_FILES);

				if($user['user_dp_fm_id']>0){
					//Update
					$fileManager = $fileManagerMapper->getByFilter("fm_id = '".$user['user_dp_fm_id']."'", true);
					$file_path = 'upload/profile/'.$fileManager['fm_encypted_name'];
					if(file_exists($file_path)){
						$this->Upload_Model->delete_file($file_path);
					}
					$fileManagerMapper->update(array(
						'fm_encypted_name'	=> $result['image_name']
					), "fm_id = '".$user['user_dp_fm_id']."'");
				}
				else{
					//Insert
					$user['user_dp_fm_id'] = $fileManagerMapper->insert(array(
						'fm_encypted_name'	=> $result['image_name']
					));
				}
				$userMapper->update(array(
					'user_dp_fm_id' =>$user['user_dp_fm_id']
				), "user_id = '".$current_user."'");

				$this->set_alert(array(
					'message'=>'<i class="fa fa-check"></i> Successfully change display picture!'
				,	'type'=>'success'
				));
			}
			else{
				$this->set_alert(array(
					'message'=>'<i class="fa fa-exclamation"></i> Failed to upload!'
				,	'type'=>'danger'
				));
			}
		};
		$this->is_secure = true;
    $this->view('user/upload_image');
	}

	public function user_ref(){
		$params = $_POST;
		$usermapper = new App\Mapper\UserMapper();
		$user_list = $usermapper->selectByType($params['user-type'], $params['search']);

		echo json_encode($user_list);
	}

	public function check_email(){
		echo "true";
	}

	public function barangay_user_ref(){
		$limit = $_POST['length'];
		$offset = $_POST['start'];
		$search = $_POST['search'];
		$columns = $_POST['columns'];
		$orders = array();

		foreach($_POST['order'] as $_order){
			array_push($orders, array(
				'col'=> $_POST['columns'][$_order['column']]['data']
			,	'type'	=> $_order['dir']
			));
		}
		$mapper = new App\Mapper\BarangayUserMapper();

		$result = $mapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders);

		echo json_encode($result);
	}

	public function bhws_list(){
		$this->is_secure = true;
		$this->view('user/bhws_list');
	}

	public function add_bhws(){
		$barangayHealthWorkerMapper = new App\Mapper\BarangayHealthWorkerMapper();
		$adminMapper = new App\Mapper\AdminMapper();
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$userMapper = new App\Mapper\UserMapper();

		$data = array(
				'admin_first_name' => ''
			,	'admin_middle_name' => ''
			,	'admin_last_name' => ''
			,	'admin_name_ext' => ''
			,	'admin_gender' => 'male'
			,	'admin_email' => ''
			,	'bc_birthdate'=>''
			,	'bc_phone_num1'=>''
			,	'bc_phone_num2'=>''
			,	'bc_phone_num3'=>''
			,	'user_name' => ''
			,	'assign_brgy'=>array(
					'brgy_id'=>''
				,	'brgy_name'=>''
			)
		);

		if(!empty($_POST)){
				$insert_basic_contact = array(
					'bc_first_name'			=>$_POST['admin-first-name']
				,	'bc_middle_name'	=>$_POST['admin-middle-name']
				,	'bc_last_name'	=>$_POST['admin-last-name']
				,	'bc_name_ext'	=>$_POST['admin-name-ext']
				,	'bc_maiden_name'	=>''
				,	'bc_birthdate'	=>	date("Y-m-d", strtotime($_POST['bc_birthdate']))
				,	'bc_phone_num1'	=>$_POST['bc_phone_num1']
				,	'bc_phone_num2'	=>$_POST['bc_phone_num2']
				,	'bc_phone_num3'	=>$_POST['bc_phone_num3']
				,	'bc_gender'	=>$_POST['gender']
				);
				$bc_id = $basicContactMapper->insert($insert_basic_contact);

				$this->load->library('Phpqrcode');

				$generatedKey = $userMapper->generateKey();
				$fm_encrypted_name = $this->phpqrcode->generate(array(
					'message'	=>$generatedKey
				));

				$fm_id = $fileManagerMapper->insert(array(
					'fm_encypted_name' => $fm_encrypted_name
				));


				$insert_user = array(
					'user_name'			=>$_POST['user-name']
				,	'user_password'	=>Encrypt($_POST['user-password'])
				,	'user_email'	=>$_POST['user-email']
				,	'user_type'	=> 5
				,	'user_qr_msg'=>$generatedKey
				,	'user_qr_fm_id'=>$fm_id
				);
				$user_id = $userMapper->insert($insert_user);

				$insert_bhws = array(
					'bhws_user_id'	=> $user_id
				,	'bhws_bc_id'	=> $bc_id
				,	'bhws_brgy_id'	=> $_POST['designated-barangay']
				);
				$barangayHealthWorkerMapper->insert($insert_bhws);
				$this->set_alert(array(
					'message'=>	'Successfully added Barangay Health Worker.'
				,	'type'	=> 	'success'
				,	'href'	=> 	DOMAIN.'users/bhws_list'
				,	'text'	=>	'Barangay Health Worker List'
				));
		}
		$this->_data['action'] = 'add';
		$this->_data['form_data'] = $data;

		$this->is_secure = true;
		$this->view('user/bhws_form');
	}

	public function edit_bhws($bhws_id){
		$barangayHealthWorkerMapper = new App\Mapper\BarangayHealthWorkerMapper();
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$barangayMapper = new App\Mapper\BarangayMapper();
		$userMapper = new App\Mapper\UserMapper();

		$barangayHealthWorker = $barangayHealthWorkerMapper->getByFilter("bhws_id = '".$bhws_id."'", true);
		$user = $userMapper->getByFilter("user_id = '".$barangayHealthWorker['bhws_user_id']."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$barangayHealthWorker['bhws_bc_id']."'", true);
		$barangay = $barangayMapper->getByFilter("brgy_id = '".$barangayHealthWorker['bhws_brgy_id']."'", true);

		if(!empty($_POST)){
				$update_basic_contact = array(
					'bc_first_name'			=>$_POST['admin-first-name']
				,	'bc_middle_name'	=>$_POST['admin-middle-name']
				,	'bc_last_name'	=>$_POST['admin-last-name']
				,	'bc_name_ext'	=>$_POST['admin-name-ext']
				,	'bc_maiden_name'	=>''
				,	'bc_birthdate'	=>	date("Y-m-d", strtotime($_POST['bc_birthdate']))
				,	'bc_phone_num1'	=>$_POST['bc_phone_num1']
				,	'bc_phone_num2'	=>$_POST['bc_phone_num2']
				,	'bc_phone_num3'	=>$_POST['bc_phone_num3']
				,	'bc_gender'	=>$_POST['gender']
				);
				$basicContactMapper->update($update_basic_contact, "bc_id = '".$basicContact['bc_id']."'");

				$update_user = array(
					'user_name'			=>$_POST['user-name']
				,	'user_email'	=>$_POST['user-email']
				);
				if(!empty($_POST['user-password'])){
					$update_user['user_password'] = Encrypt($_POST['user-password']);
				}
				$userMapper->update($update_user, "user_id = '".$user['user_id']."'");

				$this->set_alert(array(
					'message'=>	'Successfully updated Barangay Health Worker.'
				,	'type'	=> 	'success'
				,	'href'	=> 	DOMAIN.'users/bhws_list'
				,	'text'	=>	'Barangay Health Worker List'
				));
		}

		$barangayHealthWorker = $barangayHealthWorkerMapper->getByFilter("bhws_id = '".$bhws_id."'", true);
		$user = $userMapper->getByFilter("user_id = '".$barangayHealthWorker['bhws_user_id']."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$barangayHealthWorker['bhws_bc_id']."'", true);
		$barangay = $barangayMapper->getByFilter("brgy_id = '".$barangayHealthWorker['bhws_brgy_id']."'", true);

		$data = array(
				'admin_first_name' => $basicContact['bc_first_name']
			,	'admin_middle_name' => $basicContact['bc_middle_name']
			,	'admin_last_name' => $basicContact['bc_last_name']
			,	'admin_name_ext' => $basicContact['bc_name_ext']
			,	'admin_gender' => $basicContact['bc_gender']
			,	'admin_email' => $user['user_email']
			,	'bc_birthdate'=> date("m/d/Y", strtotime($basicContact['bc_birthdate']))
			,	'bc_phone_num1'=>$basicContact['bc_phone_num1']
			,	'bc_phone_num2'=>$basicContact['bc_phone_num2']
			,	'bc_phone_num3'=>$basicContact['bc_phone_num3']
			,	'user_name' => $user['user_name']
			,	'assign_brgy'=>array(
					'brgy_id'=>$barangay['brgy_id']
				,	'brgy_name'=>$barangay['brgy_name']
			)
		);

		$this->_data['action'] = 'edit';
		$this->_data['form_data'] = $data;

		$this->is_secure = true;
		$this->view('user/bhws_form');
	}

	public function delete_bhws(){
		$params = $_POST;
		$bhws_id = $params['id'];
		$userMapper = new App\Mapper\UserMapper();
		$barangayHealthWorkerMapper = new App\Mapper\BarangayHealthWorkerMapper();
		$basicContactMapper = new App\Mapper\BasicContactMapper();

		$barangayHealthWorker = $barangayHealthWorkerMapper->getByFilter("bhws_id = '".$bhws_id."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '". $barangayHealthWorker['bhws_bc_id']."' ", true);
		$user = $userMapper->getByFilter("user_id = '". $barangayHealthWorker['bhws_user_id']."' ", true);
		$barangayHealthWorkerMapper->delete("bhws_id = '". $bhws_id."' ");
		$basicContactMapper->delete("bc_id = '". $basicContact['bc_id']."' ");
		$userMapper->delete("user_id = '". $user['user_id']."' ");

		echo "1";
	}

	public function bhws_ref(){
		$limit = $_POST['length'];
		$offset = $_POST['start'];
		$search = $_POST['search'];
		$columns = $_POST['columns'];
		$orders = array();

		foreach($_POST['order'] as $_order){
			array_push($orders, array(
				'col'=> $_POST['columns'][$_order['column']]['data']
			,	'type'	=> $_order['dir']
			));
		}
		$mapper = new App\Mapper\BarangayHealthWorkerMapper();

		$result = $mapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders);

		echo json_encode($result);
	}

	//////////////
	public function pharmacist_list(){
		$this->is_secure = true;
		$this->view('user/pharmacist_list');
	}

	public function add_pharmacist(){
		$pharmacistMapper = new App\Mapper\PharmacistMapper();
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$userMapper = new App\Mapper\UserMapper();

		$data = array(
				'pharmacist_first_name' => ''
			,	'pharmacist_middle_name' => ''
			,	'pharmacist_last_name' => ''
			,	'pharmacist_name_ext' => ''
			,	'pharmacist_gender' => 'male'
			,	'pharmacist_email' => ''
			,	'pharmacist_add_info'=>''
			,	'bc_birthdate'=>''
			,	'bc_phone_num1'=>''
			,	'bc_phone_num2'=>''
			,	'bc_phone_num3'=>''
			,	'user_name' => ''
			,	'qrcode_url'=>''
		);

		if(!empty($_POST)){
				$insert_basic_contact = array(
					'bc_first_name'			=>$_POST['pharmacist-first-name']
				,	'bc_middle_name'	=>$_POST['pharmacist-middle-name']
				,	'bc_last_name'	=>$_POST['pharmacist-last-name']
				,	'bc_name_ext'	=>$_POST['pharmacist-name-ext']
				,	'bc_maiden_name'	=>''
				,	'bc_birthdate'	=>	date("Y-m-d", strtotime($_POST['bc_birthdate']))
				,	'bc_phone_num1'	=>$_POST['bc_phone_num1']
				,	'bc_phone_num2'	=>$_POST['bc_phone_num2']
				,	'bc_phone_num3'	=>$_POST['bc_phone_num3']
				,	'bc_gender'	=>$_POST['gender']
				);
				$bc_id = $basicContactMapper->insert($insert_basic_contact);

				$this->load->library('Phpqrcode');

				$generatedKey = $userMapper->generateKey();
				$fm_encrypted_name = $this->phpqrcode->generate(array(
					'message'	=>$generatedKey
				));

				$fm_id = $fileManagerMapper->insert(array(
					'fm_encypted_name' => $fm_encrypted_name
				));


				$insert_user = array(
					'user_name'			=>$_POST['user-name']
				,	'user_password'	=>Encrypt($_POST['user-password'])
				,	'user_email'	=>$_POST['user-email']
				,	'user_type'	=> 3
				,	'user_qr_msg'=>$generatedKey
				,	'user_qr_fm_id'=>$fm_id
				);
				$user_id = $userMapper->insert($insert_user);

				$insert_pharmacist = array(
					'pharmacist_user_id'	=> $user_id
				,	'pharmacist_bc_id'	=> $bc_id
				,	'pharmacist_add_info'	=> $_POST['pharmacist-additional-info']
				);
				$pharmacistMapper->insert($insert_pharmacist);
				$this->set_alert(array(
					'message'=>	'Successfully added Pharmacist.'
				,	'type'	=> 	'success'
				,	'href'	=> 	DOMAIN.'users/pharmacist_list'
				,	'text'	=>	'Pharmacist List'
				));
		}
		$this->_data['action'] = 'add';
		$this->_data['form_data'] = $data;

		$this->is_secure = true;
		$this->view('user/pharmacist_form');
	}

	public function edit_pharmacist($pharmacist_id){
		$pharmacistMapper = new App\Mapper\PharmacistMapper();
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$userMapper = new App\Mapper\UserMapper();

		$pharmacist = $pharmacistMapper->getByFilter("pharmacist_id = '".$pharmacist_id."'", true);
		$user = $userMapper->getByFilter("user_id = '".$pharmacist['pharmacist_user_id']."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$pharmacist['pharmacist_bc_id']."'", true);
		$fileManager = $fileManagerMapper->getByFilter("fm_id = '".$user['user_qr_fm_id']."'", true);

		if(!empty($_POST)){
				$update_basic_contact = array(
					'bc_first_name'			=>$_POST['pharmacist-first-name']
				,	'bc_middle_name'	=>$_POST['pharmacist-middle-name']
				,	'bc_last_name'	=>$_POST['pharmacist-last-name']
				,	'bc_name_ext'	=>$_POST['pharmacist-name-ext']
				,	'bc_maiden_name'	=>''
				,	'bc_birthdate'	=>	date("Y-m-d", strtotime($_POST['bc_birthdate']))
				,	'bc_phone_num1'	=>$_POST['bc_phone_num1']
				,	'bc_phone_num2'	=>$_POST['bc_phone_num2']
				,	'bc_phone_num3'	=>$_POST['bc_phone_num3']
				,	'bc_gender'	=>$_POST['gender']
				);
				$basicContactMapper->update($update_basic_contact, "bc_id = '".$pharmacist['pharmacist_bc_id']."'");

				$update_user = array(
					'user_name'			=>$_POST['user-name']
				,	'user_email'	=>$_POST['user-email']
				);
				if(!empty($_POST['user-password'])){
					$update_user['user_password'] = Encrypt($_POST['user-password']);
				}
				$userMapper->update($update_user, "user_id = '".$pharmacist['pharmacist_user_id']."'");

				$update_pharmacist = array(
					'pharmacist_add_info'	=> $_POST['pharmacist-additional-info']
				);
				$pharmacistMapper->update($update_pharmacist, "pharmacist_id = '".$pharmacist['pharmacist_id']."'");
				$this->set_alert(array(
					'message'=>	'Successfully update Pharmacist.'
				,	'type'	=> 	'success'
				,	'href'	=> 	DOMAIN.'users/pharmacist_list'
				,	'text'	=>	'Pharmacist List'
				));
		}

		$pharmacist = $pharmacistMapper->getByFilter("pharmacist_id = '".$pharmacist_id."'", true);
		$user = $userMapper->getByFilter("user_id = '".$pharmacist['pharmacist_user_id']."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$pharmacist['pharmacist_bc_id']."'", true);
		$fileManager = $fileManagerMapper->getByFilter("fm_id = '".$user['user_qr_fm_id']."'", true);

		$data = array(
				'pharmacist_first_name' => $basicContact['bc_first_name']
			,	'pharmacist_middle_name' => $basicContact['bc_middle_name']
			,	'pharmacist_last_name' => $basicContact['bc_last_name']
			,	'pharmacist_name_ext' => $basicContact['bc_name_ext']
			,	'pharmacist_gender' => $basicContact['bc_gender']
			,	'pharmacist_email' => $user['user_email']
			,	'pharmacist_add_info'=>$pharmacist['pharmacist_add_info']
			,	'bc_birthdate'=>date("m/d/Y", strtotime($basicContact['bc_birthdate']))
			,	'bc_phone_num1'=>$basicContact['bc_phone_num1']
			,	'bc_phone_num2'=>$basicContact['bc_phone_num2']
			,	'bc_phone_num3'=>$basicContact['bc_phone_num3']
			,	'user_name' => $user['user_name']
			,	'qrcode_url'=>$fileManager['fm_encypted_name']
		);

		$this->_data['action'] = 'edit';
		$this->_data['form_data'] = $data;

		$this->is_secure = true;
		$this->view('user/pharmacist_form');
	}

	public function delete_pharmacist(){
		$params = $_POST;
		$pharmacist_id = $params['id'];
		$pharmacistMapper = new App\Mapper\PharmacistMapper();
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$userMapper = new App\Mapper\UserMapper();

		$pharmacist = $pharmacistMapper->getByFilter("pharmacist_id = '".$pharmacist_id."'", true);
		$user = $userMapper->getByFilter("user_id = '".$pharmacist['pharmacist_user_id']."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$pharmacist['pharmacist_bc_id']."'", true);
		$fileManager = $fileManagerMapper->getByFilter("fm_id = '".$user['user_qr_fm_id']."'", true);

		$pharmacistMapper->delete("pharmacist_id = '".$pharmacist['pharmacist_id']."'");
		$userMapper->delete("user_id = '".$user['user_id']."'");
		$basicContactMapper->delete("bc_id = '".$basicContact['bc_id']."'");
		$fileManagerMapper->delete("fm_id = '".$fileManager['fm_id']."'");

		echo "1";
	}

	public function pharmacist_ref(){
		$limit = $_POST['length'];
		$offset = $_POST['start'];
		$search = $_POST['search'];
		$columns = $_POST['columns'];
		$orders = array();

		foreach($_POST['order'] as $_order){
			array_push($orders, array(
				'col'=> $_POST['columns'][$_order['column']]['data']
			,	'type'	=> $_order['dir']
			));
		}
		$mapper = new App\Mapper\PharmacistMapper();

		$result = $mapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders);

		echo json_encode($result);
	}

	///////////////

	public function medical_officer_list(){
		$this->is_secure = true;
		$this->view('user/medical_officer_list');
	}

	public function add_medical_officer(){
		$medicalOfficerMapper = new App\Mapper\MedicalOfficerMapper();
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$userMapper = new App\Mapper\UserMapper();

		if(!empty($_POST)){
				$insert_basic_contact = array(
					'bc_first_name'			=>$_POST['mo-first-name']
				,	'bc_middle_name'	=>$_POST['mo-middle-name']
				,	'bc_last_name'	=>$_POST['mo-last-name']
				,	'bc_name_ext'	=>$_POST['mo-name-ext']
				,	'bc_maiden_name'	=>''
				,	'bc_birthdate'	=>	date("Y-m-d", strtotime($_POST['bc_birthdate']))
				,	'bc_phone_num1'	=>$_POST['bc_phone_num1']
				,	'bc_phone_num2'	=>$_POST['bc_phone_num2']
				,	'bc_phone_num3'	=>$_POST['bc_phone_num3']
				,	'bc_gender'	=>$_POST['gender']
				);
				$bc_id = $basicContactMapper->insert($insert_basic_contact);

				$this->load->library('Phpqrcode');

				$generatedKey = $userMapper->generateKey();
				$fm_encrypted_name = $this->phpqrcode->generate(array(
					'message'	=>$generatedKey
				));

				$fm_id = $fileManagerMapper->insert(array(
					'fm_encypted_name' => $fm_encrypted_name
				));

				$insert_user = array(
					'user_name'			=>$_POST['user-name']
				,	'user_password'	=>Encrypt($_POST['user-password'])
				,	'user_email'	=>$_POST['user-email']
				,	'user_type'	=> 4
				,	'user_qr_msg'=>$generatedKey
				,	'user_qr_fm_id'=>$fm_id
				);
				$user_id = $userMapper->insert($insert_user);

				$insert_mo = array(
					'mo_user_id'	=> $user_id
				,	'mo_bc_id'	=> $bc_id
				,	'mo_add_info'	=> $_POST['mo-additional-info']
				);
				$medicalOfficerMapper->insert($insert_mo);
				$this->set_alert(array(
					'message'=>	'Successfully added Medical Officer.'
				,	'type'	=> 	'success'
				,	'href'	=> 	DOMAIN.'users/medical-officer-list'
				,	'text'	=>	'Medical Officer List'
				));
		}

		$data = array(
				'mo_first_name' => ''
			,	'mo_middle_name' => ''
			,	'mo_last_name' => ''
			,	'mo_name_ext' => ''
			,	'mo_gender' => 'male'
			,	'mo_email' => ''
			,	'mo_add_info'=>''
			,	'bc_birthdate'=>''
			,	'bc_phone_num1'=>''
			,	'bc_phone_num2'=>''
			,	'bc_phone_num3'=>''
			,	'user_name' => ''
			,	'qrcode_url'=>''
		);

		$this->_data['action'] = 'add';
		$this->_data['form_data'] = $data;

		$this->is_secure = true;
		$this->view('user/medical_officer_form');
	}

	public function edit_medical_officer($medical_officer_id){
		$medicalOfficerMapper = new App\Mapper\MedicalOfficerMapper();
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$userMapper = new App\Mapper\UserMapper();

		$medicalOfficer = $medicalOfficerMapper->getByFilter("mo_id = '".$medical_officer_id."'", true);
		$user = $userMapper->getByFilter("user_id = '".$medicalOfficer['mo_user_id']."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$medicalOfficer['mo_bc_id']."'", true);

		if(!empty($_POST)){
				$update_basic_contact = array(
					'bc_first_name'			=>$_POST['mo-first-name']
				,	'bc_middle_name'	=>$_POST['mo-middle-name']
				,	'bc_last_name'	=>$_POST['mo-last-name']
				,	'bc_name_ext'	=>$_POST['mo-name-ext']
				,	'bc_birthdate'	=>	date("Y-m-d", strtotime($_POST['bc_birthdate']))
				,	'bc_phone_num1'	=>$_POST['bc_phone_num1']
				,	'bc_phone_num2'	=>$_POST['bc_phone_num2']
				,	'bc_phone_num3'	=>$_POST['bc_phone_num3']
				,	'bc_gender'	=>$_POST['gender']
				);
				$basicContactMapper->update($update_basic_contact, "bc_id = '".$basicContact['bc_id']."'");

				$update_user = array(
					'user_name'			=>$_POST['user-name']
				,	'user_email'	=>$_POST['user-email']
				);
				if(!empty($_POST['user-password'])){
					$update_user['user_password'] = Encrypt($_POST['user-password']);
				}
				$userMapper->update($update_user, "user_id = '".$user['user_id']."'");

				$update_mo = array(
					'mo_add_info'	=> $_POST['mo-additional-info']
				);
				$medicalOfficerMapper->update($update_mo, "mo_id = '".$medicalOfficer['mo_id']."'");
				$this->set_alert(array(
					'message'=>	'Successfully update Medical Officer.'
				,	'type'	=> 	'success'
				,	'href'	=> 	DOMAIN.'users/medical-officer-list'
				,	'text'	=>	'Medical Officer List'
				));
		}
		$medicalOfficer = $medicalOfficerMapper->getByFilter("mo_id = '".$medical_officer_id."'", true);
		$user = $userMapper->getByFilter("user_id = '".$medicalOfficer['mo_user_id']."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$medicalOfficer['mo_bc_id']."'", true);
		$fileManager = $fileManagerMapper->getByFilter("fm_id = '".$user['user_qr_fm_id']."'", true);

		$data = array(
				'mo_first_name' => $basicContact['bc_first_name']
			,	'mo_middle_name' => $basicContact['bc_middle_name']
			,	'mo_last_name' => $basicContact['bc_last_name']
			,	'mo_name_ext' => $basicContact['bc_name_ext']
			,	'mo_gender' => $basicContact['bc_gender']
			,	'mo_email' => $user['user_email']
			,	'mo_add_info'=>$medicalOfficer['mo_add_info']
			,	'bc_birthdate'=>date("m/d/Y", strtotime($basicContact['bc_birthdate']))
			,	'bc_phone_num1'=>$basicContact['bc_phone_num1']
			,	'bc_phone_num2'=>$basicContact['bc_phone_num2']
			,	'bc_phone_num3'=>$basicContact['bc_phone_num3']
			,	'user_name' => $user['user_name']
			,	'qrcode_url'=>$fileManager['fm_encypted_name']
		);

		$this->_data['action'] = 'add';
		$this->_data['form_data'] = $data;

		$this->is_secure = true;
		$this->view('user/medical_officer_form');
	}

	public function delete_medical_officer(){
		$params = $_POST;
		$mo_id = $params['id'];
		$medicalOfficerMapper = new App\Mapper\MedicalOfficerMapper();
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$userMapper = new App\Mapper\UserMapper();

		$medicalOfficer = $medicalOfficerMapper->getByFilter("mo_id = '".$mo_id."'", true);
		$user = $userMapper->getByFilter("user_id = '".$medicalOfficer['mo_user_id']."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$medicalOfficer['mo_bc_id']."'", true);
		$fileManager = $fileManagerMapper->getByFilter("fm_id = '".$user['user_qr_fm_id']."'", true);

		$medicalOfficerMapper->delete("mo_id = '".$medicalOfficer['mo_id']."'");
		$userMapper->delete("user_id = '".$user['user_id']."'");
		$basicContactMapper->delete("bc_id = '".$basicContact['bc_id']."'");
		$fileManagerMapper->delete("fm_id = '".$fileManager['fm_id']."'");

		echo "1";
	}

	public function medical_officer_ref(){
		$limit = $_POST['length'];
		$offset = $_POST['start'];
		$search = $_POST['search'];
		$columns = $_POST['columns'];
		$orders = array();

		foreach($_POST['order'] as $_order){
			array_push($orders, array(
				'col'=> $_POST['columns'][$_order['column']]['data']
			,	'type'	=> $_order['dir']
			));
		}
		$mapper = new App\Mapper\MedicalOfficerMapper();

		$result = $mapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders);

		echo json_encode($result);
	}

	//Registering from public
	public function register_resident(){

		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$userMapper = new App\Mapper\UserMapper();
		$this->_secure = false;
		$this->_template = 'templates/public';
		if(!empty($_POST)){
			$bc_id = $basicContactMapper->insert(array(
					'bc_first_name' => $_POST['resident-first-name']
				,	'bc_middle_name'=> $_POST['resident-middle-name']
				,	'bc_last_name'=> $_POST['resident-last-name']
				,	'bc_name_ext'=> ''
				,	'bc_maiden_name'=>''
				,	'bc_birthdate'=> null
				,	'bc_phone_num1'=> ''
				,	'bc_phone_num2'=> ''
				,	'bc_phone_num3'=> ''
				,	'bc_gender'=>	'male'
			));

			$parent_father_bc_id = $basicContactMapper->insert(array(
					'bc_first_name' => ''
				,	'bc_middle_name'=> ''
				,	'bc_last_name'=> ''
				,	'bc_name_ext'=> ''
				,	'bc_maiden_name'=>''
				,	'bc_birthdate'=> null
				,	'bc_phone_num1'=> ''
				,	'bc_phone_num2'=> ''
				,	'bc_phone_num3'=> ''
				,	'bc_gender'=>	'male'
			));

			$parent_mother_bc_id = $basicContactMapper->insert(array(
					'bc_first_name' => ''
				,	'bc_middle_name'=> ''
				,	'bc_last_name'=> ''
				,	'bc_name_ext'=> ''
				,	'bc_maiden_name'=>''
				,	'bc_birthdate'=> null
				,	'bc_phone_num1'=> ''
				,	'bc_phone_num2'=> ''
				,	'bc_phone_num3'=> ''
				,	'bc_gender'=>	'female'
			));

			$resident_address_id = $addressMapper->insert(array(
				'address_city_id' 		=> $this->_default_city_id
			,	'address_province_id' => $this->_default_province_id
			,	'address_brgy_id' => '0'
			,	'address_desc' => ''
			));

			$generatedKey = $userMapper->generateKey();
			// $user_id = $userMapper->insert(array(
			// 	'user_name'=>$_POST['reg-username']
			// ,	'user_password'	=>encrypt($_POST['reg-password'])
			// ,	'user_email'	=>$_POST['reg-email']
			// ,	'user_type'		=>'2'
			// ,	'user_qr_msg'	=>$generatedKey
			// ));






			$this->load->library('Phpqrcode');

			$fm_encrypted_name = $this->phpqrcode->generate(array(
				'message'	=>$generatedKey
			));

			$fm_id = $fileManagerMapper->insert(array(
				'fm_encypted_name' => $fm_encrypted_name
			));

			$user_id = $userMapper->insert(array(
				'user_name'=>strtolower(substr($_POST['resident-first-name'],0, 2).$_POST['resident-last-name'])
			,	'user_password'	=>encrypt('123456')
			,	'user_email'	=>''
			,	'user_type'		=>'2'
			,	'user_qr_msg'	=>$generatedKey
			,	'user_qr_fm_id'		=>$fm_id
			));

			$resident_id = $residentMapper->insert(array(
				'resident_bc_id'	=> $bc_id
			,	'resident_no'	=> ''
			,	'resident_user_id'=>$user_id
			,	'resident_address_id'	=> $resident_address_id
			,	'resident_father_bc_id'	=> $parent_father_bc_id
			,	'resident_mother_bc_id'	=> $parent_mother_bc_id
			,	'resident_emp_status'	=>	''
			,	'resident_civil_status'	=> ''
			,	'resident_birthplace'	=>	''
			,	'resident_blood_type'	=>	''
			,	'resident_occupation'	=>	''
			,	'resident_religion'=> ''
			,	'resident_householdno'=>''
			,	'resident_philhealth'=>'0'
			,	'resident_philhealth_status'=>''
			,	'resident_philhealthno'=>''
			,	'resident_philhealthcat'=>''
			,	'resident_family_history'=>''
			,	'resident_add_info'=> ''
			));
			$residentMapper->update(array(
				'resident_no'=>$residentMapper->generate_no($resident_id)
			), "resident_id = '".$resident_id."'");




			$user = $userMapper->getByFilter("user_qr_msg = '".$generatedKey."'", true);

			//Resident
			$residentMapper = new App\Mapper\ResidentMapper();
			$resident = $residentMapper->getByFilter("resident_user_id = '".$user_id."'", true);

			$basicContact = $basicContactMapper->getByID($resident['resident_bc_id']);
			$displayname = $basicContact['bc_first_name'] . ' ' . $basicContact['bc_middle_name'] . ' ' . $basicContact['bc_last_name'] . ' ' . $basicContact['bc_name_ext'];
			$user_details = array(
					'displayname'=>	$displayname
				,	'id'=>	$user['user_id']
				,	'email'		=>$user['user_email']
				,	'type'		=>$user['user_type']
			);
			$this->sess->setLogin($user_details);

			$this->set_alert(array(
				'message'=>'Created an account'
			,	'type'=>'success'
			));
			$this->redirect(DOMAIN.'resident/my_profile/');
		}
		$this->view('user/registration');
	}

}
