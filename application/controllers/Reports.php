<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reports extends Controller {

	private $_default_province_id = 24;
	private $_default_city_id = 474;

	public function __construct(){
		parent::__construct();
	}

	public function mobility_report(){
		$params = $_GET;
		$appointmentReport = array();
		$barangayMapper = new App\Mapper\BarangayMapper();
		$barangayList = $barangayMapper->getAll();
		if(!empty($params)){
			$appointmentMapper = new App\Mapper\AppointmentMapper();
			$filter = array(
					'date_from'=>date('Y-m-d', strtotime($params['date-from']))
				,	'date_to'=>date('Y-m-d', strtotime($params['date-to']))
			);
			if($params['barangay-filter'] != 'all'){
				$filter['brgy'] = $params['barangay-filter'];
			}
			$appointmentReport = $appointmentMapper->getMobilityReportByDate($filter);
		}

		$barGraph = array();


		if(!empty($appointmentReport)){
			$barGraph = array(
				'label'=>array()
			,	'value'=>array()
			);
			foreach($appointmentReport as $appointment){
				$barGraph['label'][] = $appointment['dt_code'].' - '.$appointment['dt_name'];
				$barGraph['value'][] = $appointment['total'];
			}
		}
		$this->_data['params'] = $params;
		$this->_data['barangay_list'] = $barangayList;
		$this->_data['bar_graph'] = $barGraph;
		$this->is_secure = true;
		$this->view('reports/mobility_report');

	}

	// public function all_supplies(){
	// 	$this->is_secure = true;
	// 	$this->view('supplies/all_supplies');
	// }
	//
	// public function generate_supply_excel(){
	// 	$filter = $_POST;
	// 	$medicalSuppliesMapper = new App\Mapper\MedicalSuppliesMapper();
	//
	// 	$medicalSuppliesList = $medicalSuppliesMapper->getCurrentInventory();
	//
	// 	$this->load->library('Phpspreadsheet');
	//
	// 	$spreadSheetObj = $this->phpspreadsheet->getSpreadObj();
	//
	// 	//Autofits
	// 	foreach (range('A','C') as $col) {
	// 	  $spreadSheetObj->getColumnDimension($col)->setAutoSize(true);
	// 	}
	//
	// 	$spreadSheetObj->setCellValueByColumnAndRow(1, 1, 'Supply');
	// 	$spreadSheetObj->setCellValueByColumnAndRow(2, 1, 'Metric Unit');
	// 	$spreadSheetObj->setCellValueByColumnAndRow(3, 1, 'Quantity');
	//
	// 	$row_index = 2;
	// 	foreach($medicalSuppliesList as $medicalSupplies){
	// 		$spreadSheetObj->setCellValueByColumnAndRow(1, $row_index, $medicalSupplies['ms_description']);
	// 		$spreadSheetObj->setCellValueByColumnAndRow(2, $row_index, $medicalSupplies['ms_unit']);
	// 		$spreadSheetObj->setCellValueByColumnAndRow(3, $row_index, $medicalSupplies['ms_actual_stock']);
	//
	// 		$row_index++;
	// 	}
	//
	// 	$output = array('file_name'=>$this->phpspreadsheet->write());
	//
	// 	echo json_encode($output);
	// }
	//
	// public function supplies_ref(){
	// 	$limit = $_POST['length'];
	// 	$offset = $_POST['start'];
	// 	$search = $_POST['search'];
	// 	$columns = $_POST['columns'];
	// 	$orders = array();
	//
	// 	foreach($_POST['order'] as $_order){
	// 		array_push($orders, array(
	// 			'col'=> $_POST['columns'][$_order['column']]['data']
	// 		,	'type'	=> $_order['dir']
	// 		));
	// 	}
	// 	$mapper = new App\Mapper\MedicalSuppliesMapper();
	//
	// 	$result = $mapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders);
	//
	// 	echo json_encode($result);
	// }
	//
	// public function supplies_history_ref(){
	// 	$limit = $_POST['length'];
	// 	$offset = $_POST['start'];
	// 	$search = $_POST['search'];
	// 	$columns = $_POST['columns'];
	// 	$orders = array();
	//
	// 	foreach($_POST['order'] as $_order){
	// 		array_push($orders, array(
	// 			'col'=> $_POST['columns'][$_order['column']]['data']
	// 		,	'type'	=> $_order['dir']
	// 		));
	// 	}
	// 	$mapper = new App\Mapper\MedicalSuppliesMapper();
	//
	// 	$result = $mapper->selectDataTableHistory($search['value'], $columns, $limit, $offset, $orders);
	//
	// 	echo json_encode($result);
	// }
	//
	// public function check_supplies(){
	// 	$input = $_POST;
	// 	$medicalSuppliesMapper = new App\Mapper\MedicalSuppliesMapper();
	// 	$medicalSupplies = $medicalSuppliesMapper->getByFilter("ms_id = '".$input['supply_id']."'", true);
	// 	echo json_encode($medicalSupplies);
	// }
	//
	// public function add_supplies(){
	// 	$medicalSuppliesMapper = new App\Mapper\MedicalSuppliesMapper();
	// 	$input = $_POST;
	// 	if(!empty($input)){
	// 		$insert_medical_supplies = array(
	// 			'ms_description'	=>	$input['ms-description']
	// 		,	'ms_actual_stock'	=>	number_format((float)$input['ms-actual-stock'], 4, '.', '')
	// 		,	'ms_unit'					=>	$input['ms-unit']
	// 		);
	// 		$medicalSuppliesMapper->insert($insert_medical_supplies);
	// 		$this->set_alert(array(
	// 			'message'=>	'Successfully added Supplies'
	// 		,	'type'	=> 	'success'
	// 		,	'href'	=> 	DOMAIN.'supplies/all-supplies'
	// 		,	'text'	=>	'Supplies List'
	// 		));
	// 	}
	//
	// 	$data = array(
	// 		'ms_description' 	=>	''
	// 	,	'ms_actual_stock'	=>	''
	// 	,	'ms_unit'	=>	''
	// 	);
	//
	// 	$this->_data['action'] = 'add';
	// 	$this->_data['form_data'] = $data;
	//
	// 	$this->is_secure = true;
	// 	$this->view('supplies/supplies_form');
	// }
	// public function edit_supplies($supplies_id){
	// 	$medicalSuppliesMapper = new App\Mapper\MedicalSuppliesMapper();
	// 	$medicalSupplies = $medicalSuppliesMapper->getByFilter("ms_id = '".$supplies_id."'", true);
	// 	$input = $_POST;
	// 	if(!empty($input)){
	// 		$update_medical_supplies = array(
	// 			'ms_description'	=>	$input['ms-description']
	// 		,	'ms_actual_stock'	=>	number_format((float)$input['ms-actual-stock'], 4, '.', '')
	// 		,	'ms_unit'					=>	$input['ms-unit']
	// 		);
	// 		$medicalSuppliesMapper->update($update_medical_supplies, "ms_id = '".$supplies_id."'");
	// 		$this->set_alert(array(
	// 			'message'=>	'Successfully updated Supplies'
	// 		,	'type'	=> 	'success'
	// 		,	'href'	=> 	DOMAIN.'supplies/all-supplies'
	// 		,	'text'	=>	'Supplies List'
	// 		));
	// 	}
	// 	$medicalSupplies = $medicalSuppliesMapper->getByFilter("ms_id = '".$supplies_id."'", true);
	//
	// 	$data = $medicalSupplies;
	//
	// 	$this->_data['action'] = 'edit';
	// 	$this->_data['form_data'] = $data;
	//
	// 	$this->is_secure = true;
	// 	$this->view('supplies/supplies_form');
	// }
	//
	// public function delete_supplies(){
	// 	$params = $_POST;
	// 	$ms_id = $params['id'];
	// 	$medicalSuppliesMapper = new App\Mapper\MedicalSuppliesMapper();
	// 	$medicalSuppliesMapper->delete("ms_id = '".$ms_id."'");
	// 	echo "1";
	// }
	//
	// public function allocation_history(){
	// 	$this->is_secure = true;
	// 	$this->view('supplies/allocation_history');
	// }
	//
	// public function allocate_supplies(){
	// 	$medicalSuppliesMapper = new App\Mapper\MedicalSuppliesMapper();
	// 	$suppliesAllocationDetailMapper = new App\Mapper\SuppliesAllocationDetailMapper();
	// 	$suppliesAllocationMapper = new App\Mapper\SuppliesAllocationMapper();
	// 	$current_user = $_SESSION['current_user']['id'];
	// 	$input = $_POST;
	// 	if(!empty($input)){
	// 		$supplies_allocation_detail = json_decode($input['supplies-allocation-detail-json'], true);
	// 		if(!empty($supplies_allocation_detail)){
	// 			$sa_id = $suppliesAllocationMapper->insert(array(
	// 				'sa_date'	=>	date('Y-m-d G:i:s')
	// 			,	'sa_from_user_id'	=> $_SESSION['current_user']['id']
	// 			,	'sa_summary'	=> $input['sa-summary']
	// 			,	'sa_to_user_id'	=> $input['user-id']
	// 			,	'sa_is_done'	=>	1
	// 			));
	// 			foreach($supplies_allocation_detail as $sad){
	// 				$ms = $medicalSuppliesMapper->getByFilter("ms_id = '".$sad['supply_id']."'", true);
	// 				$suppliesAllocationDetailMapper->insert(array(
	// 					'sad_ms_id'	=>	$sad['supply_id']
	// 				,	'sad_sa_id'	=>	$sa_id
	// 				,	'sad_quantity'	=> $sad['supply_quantity']
	// 				,	'sad_ms_unit'	=> $ms['ms_unit']
	// 				));
	// 				$current_supply = $ms['ms_actual_stock'] - $sad['supply_quantity'];
	// 				$ms['ms_actual_stock'] = $current_supply;
	// 				$medicalSuppliesMapper->update($ms, "ms_id = '".$ms['ms_id']."'");
	//
	// 			}
	// 			$this->set_alert(array(
	// 				'message'=>	'Successfully allocated supplies'
	// 			,	'type'	=> 	'success'
	// 			,	'href'	=> 	DOMAIN.'supplies/all-supplies'
	// 			,	'text'	=>	'Supplies List'
	// 			));
	// 		}
	// 		else{
	// 			$this->set_alert(array(
	// 				'message'=>	'Failed to save! Supply Details Is Empty'
	// 			,	'type'	=> 	'danger'
	// 			));
	// 		}
	//
	//
	// 	}
	//
	// 	$data = array(
	// 		'ms_description' 	=>	''
	// 	,	'ms_actual_stock'	=>	''
	// 	,	'ms_unit'	=>	''
	// 	);
	//
	// 	$this->_data['action'] = 'add';
	// 	$this->_data['form_data'] = $data;
	//
	// 	$this->is_secure = true;
	// 	$this->view('supplies/allocate_supplies');
	// }
	// public function edit_allocate_supplies($supplies_id){
	// 	$medicalSuppliesMapper = new App\Mapper\MedicalSuppliesMapper();
	// 	$medicalSupplies = $medicalSuppliesMapper->getByFilter("ms_id = '".$supplies_id."'", true);
	// 	$input = $_POST;
	// 	if(!empty($input)){
	// 		$update_medical_supplies = array(
	// 			'ms_description'	=>	$input['ms-description']
	// 		,	'ms_actual_stock'	=>	number_format((float)$input['ms-actual-stock'], 4, '.', '')
	// 		,	'ms_unit'					=>	$input['ms-unit']
	// 		);
	// 		$medicalSuppliesMapper->update($update_medical_supplies, "ms_id = '".$supplies_id."'");
	// 		$this->set_alert(array(
	// 			'message'=>	'Successfully updated Supplies'
	// 		,	'type'	=> 	'success'
	// 		,	'href'	=> 	DOMAIN.'supplies/all-supplies'
	// 		,	'text'	=>	'Supplies List'
	// 		));
	// 	}
	// 	$medicalSupplies = $medicalSuppliesMapper->getByFilter("ms_id = '".$supplies_id."'", true);
	//
	// 	$data = $medicalSupplies;
	//
	// 	$this->_data['action'] = 'edit';
	// 	$this->_data['form_data'] = $data;
	//
	// 	$this->is_secure = true;
	// 	$this->view('supplies/supplies_form');
	// }
	//
	// public function delete_allocate_supplies(){
	//
	// 	//Readd supplies here
	// 	$params = $_POST;
	// 	$ms_id = $params['id'];
	// 	$medicalSuppliesMapper = new App\Mapper\MedicalSuppliesMapper();
	// 	$medicalSuppliesMapper->delete("ms_id = '".$ms_id."'");
	// 	echo "1";
	// }



}
