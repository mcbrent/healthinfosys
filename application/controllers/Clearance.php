<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Clearance extends Controller {

	private $_default_province_id = 24;
	private $_default_city_id = 447;

	public function __construct(){
		parent::__construct();
	}

	public function request_ref(){
		$limit = $_POST['length'];
		$offset = $_POST['start'];
		$search = $_POST['search'];
		$columns = $_POST['columns'];
		$orders = array();

		foreach($_POST['order'] as $_order){
			array_push($orders, array(
				'col'=> $_POST['columns'][$_order['column']]['data']
			,	'type'	=> $_order['dir']
			));
		}
		$mapper = new App\Mapper\PCRequestMapper();

		$result = $mapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders);

		echo json_encode($result);
	}

	public function request_list(){
		$this->is_secure = true;
		$this->view('clearance/request_list');
	}

	public function process($pcq_id){
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$barangayMapper = new App\Mapper\BarangayMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$residentAttachmentMapper = new App\Mapper\ResidentAttachmentMapper();
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$pcRequestMapper = new App\Mapper\PCRequestMapper();
		$pcRequest = $pcRequestMapper->getByFilter("pcq_id = '".$pcq_id ."'", true);

		$resident_id = $pcRequest['pcq_resident_id'];
		$resident = $residentMapper->getByFilter("resident_id = '".$resident_id."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
		$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
		$brgy = $barangayMapper->getByFilter("brgy_id = '".$address['address_brgy_id']."'", true);
		$residentAttachment = $residentAttachmentMapper->getByFilter("rattachment_resident_id = '".$resident_id."'");

		$data = array(
				'resident_id'=>$resident_id
			,	'resident_first_name' => $basicContact['bc_first_name']
			,	'resident_middle_name' => $basicContact['bc_middle_name']
			,	'resident_last_name' => $basicContact['bc_last_name']
			,	'resident_name_ext' => $basicContact['bc_name_ext']
			,	'add_desc'	=> $address['address_desc']
			,	'address_brgy' => array(
					'brgy_id'	=> $brgy['brgy_id']
				,	'brgy_name'=> $brgy['brgy_name']
				)
			,	'resident_gender'=>$basicContact['bc_gender']
			,	'resident_birthday' => date("m/d/Y", strtotime($resident['resident_birthdate']))
			,	'resident_civil_status'=>$resident['resident_civil_status']
			,	'resident_nationality'=>$resident['resident_nationality']
			,	'phone_number_1'=>$basicContact['bc_phone_num1']
			,	'phone_number_2'=>$basicContact['bc_phone_num2']
			,	'resident_add_info'=>$resident['resident_add_info']
			,	'resident_attachment'=>$residentAttachment
			,	'resident_nickname'=>$resident['resident_nickname']
			,	'resident_occupation'=>$resident['resident_occupation']
			,	'resident_religion'=>$resident['resident_religion']
			,	'resident_yrs_community'=>$resident['resident_yrs_community']
			,	'resident_weight'=>$resident['resident_weight']
			,	'resident_height'=>$resident['resident_height']
			,	'resident_completion'=>$resident['resident_completion']
			,	'resident_distinguishing_marks'=>$resident['resident_distinguishing_marks']
			,	'resident_hair_color'=>$resident['resident_hair_color']
			,	'resident_eye_color'=>$resident['resident_eye_color']
			,	'resident_body_size'=>$resident['resident_body_size']
		);

		$this->_data['form_data'] = $data;
		$this->is_secure = true;
    $this->view('clearance/process');
	}

	public function request(){
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$pcRequestMapper = new App\Mapper\PCRequestMapper();
		$this->is_secure = false;

		if(!empty($_POST)){
			$entry = $_POST;
			$resident_id = 0;
			$filter = array(
					'bc_first_name' => $entry['resident-first-name']
				,	'bc_middle_name'=> $entry['resident-middle-name']
				,	'bc_last_name'=> $entry['resident-last-name']
				,	'bc_name_ext'=> $entry['resident-name-ext']
				,	'bc_gender'=>	$entry['resident-gender']
				,	'resident_birthdate'=>	$entry['resident-birthday']
			);
			$resident = $residentMapper->checkIfRegistered($filter, 0);
			if(empty($resident)){
				$this->createResident($entry);
			}
			else{
				if(count($resident) > 1){
					$resident = $residentMapper->checkIfRegistered($filter, $entry['current-barangay']);
				}
				$resident = $resident[0];
				$resident_id = $resident['resident_id'];
				if($entry['current-barangay'] == $resident['address_brgy_id']){
					//Means same requestor
					$pcRequest = $pcRequestMapper->checkExistByResident($resident_id);
					if(empty($pcRequest)){
						$pcq_id = $pcRequestMapper->insert(array(
							'pcq_resident_id'=>$resident_id
						,	'pcq_request_date'=>date('Y-m-d H:i:s', strtotime($_POST['target-date']. ' '.$_POST['target-time'].':00'))
						,	'pcq_purpose'=>$_POST['purpose']
						,	'pcq_stage'=>'requested'
						,	'pcq_status'=>0
						,	'pcq_no'=>''
						));
						$pcq_no = $pcRequestMapper->generate_pcq_no($pcq_id);
						$pcRequestMapper->update(
							array('pcq_no'=>$pcq_no), 'pcq_id = "'.$pcq_id.'"'
						);

						$this->set_alert(array(
							'message'=>"<h2><i class=\"fa fa-check-circle-o\"></i> You may now visit the police station with your Queue No. <h1>".$pcq_no."</h1></h2>"
						,	'type'=>'success'
						));


					}
					else{
						//There is an ongoing request
						$this->set_alert(array(
							'message'=>"<h2><i class=\"fa fa-exclamation\"></i> You already have an appoinment for Police Clearance!</h2>"
						,	'type'=>'danger'
						));
					}

				}
				// $resident_id = $resident['']
			}


		}
		$this->is_secure = false;
		$this->_template = 'templates/public';
		$this->view('clearance/request');
	}

	private function createResident($data){
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$bc_id = $basicContactMapper->insert(array(
				'bc_first_name' => $data['resident-first-name']
			,	'bc_middle_name'=> $data['resident-middle-name']
			,	'bc_last_name'=> $data['resident-last-name']
			,	'bc_name_ext'=> $data['resident-name-ext']
			,	'bc_phone_num1'=> $data['phone-number-1']
			,	'bc_phone_num2'=> $data['phone-number-2']
			,	'bc_phone_num3'=> ''
			,	'bc_gender'=>	$data['resident-gender']
		));

		$resident_address_id = $addressMapper->insert(array(
			'address_city_id' 		=> $this->_default_city_id
		,	'address_province_id' => $this->_default_province_id
		,	'address_brgy_id' => $data['current-barangay']
		,	'address_desc' => $data['add-desc']
		));

		$resident_id = $residentMapper->insert(array(
			'resident_bc_id'	=> $bc_id
		,	'resident_address_id'	=> $resident_address_id
		,	'resident_birthdate'	=> date("Y-m-d", strtotime($data['resident-birthday']))
		,	'resident_civil_status'	=> $data['resident-civil-status']
		,	'resident_nationality'	=> $data['resident-nationality']
		,	'resident_added_by' => 0
		,	'resident_added_time'=>  date('Y-m-d H:i:s', time())
		,	'resident_modified_by' => 0
		,	'resident_modified_time'=>  date('Y-m-d H:i:s', time())
		,	'resident_add_info'=> ''
		,	'resident_nickname'=> $data['resident-alias']
		,	'resident_occupation'=> $data['resident-occupation']
		,	'resident_religion'=> $data['resident-religion']
		,	'resident_yrs_community'=> $data['resident-yrs-community']
		,	'resident_weight'=> $data['resident-weight']
		,	'resident_height'=> $data['resident-height']
		,	'resident_completion'=> $data['resident-completion']
		,	'resident_distinguishing_marks'=> $data['resident-distinguishing-marks']
		,	'resident_hair_color'=> $data['resident-hair-color']
		,	'resident_eye_color'=> $data['resident-eye-color']
		,	'resident_body_size'=> $data['resident-body-size']
		));
	}

	// public function applicant_ref(){
	// 	$limit = $_POST['length'];
	// 	$offset = $_POST['start'];
	// 	$search = $_POST['search'];
	// 	$columns = $_POST['columns'];
	// 	$option = $_POST['option'];
	// 	$orders = array();
	//
	// 	foreach($_POST['order'] as $_order){
	// 		array_push($orders, array(
	// 			'col'=> $_POST['columns'][$_order['column']]['data']
	// 		,	'type'	=> $_order['dir']
	// 		));
	// 	}
	// 	$mapper = new App\Mapper\AdminMapper();
	// 	$result = $mapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders);
	// 	echo json_encode($result);
	// }
	//
	// public function file_attachment(){
	// 	$this->_template = 'templates/applicant_main';
  //   $this->view('applicant/file_attachment');
	// }
	//
	// public function list(){
	// 	$this->_template = 'templates/admin_main';
  //   $this->view('applicant/list');
	// }
	//
	// public function save_profile(){
	// 	$userMapper = new App\Mapper\UserMapper();
	// 	$educationMapper = new App\Mapper\EducationMapper();
	// 	$applicantMapper = new App\Mapper\ApplicantMapper();
	// 	$addressMapper = new App\Mapper\AddressMapper();
	// 	$user = $userMapper->getByFilter(array(
	// 		array(
	// 			'column'=>'user_name'
	// 		,	'value'	=>$_POST['applicant-username']
	// 		)
	// 	), true);
	// 	$applicant = $applicantMapper->getByFilter("applicant_user_id = '". $user['user_id']."' ", true);
	// 	if(!$user);//Show 404;
	//
	// 	if($applicant['applicant_present_id']){
	// 		//DO update if already exist
	// 		$addressMapper->update(array(
	// 				"address_city_id"		=>$_POST['present-add-city']
	// 			,	"address_province_id"=>$_POST['present-add-province']
	// 			,	"address_desc"	=>$_POST['present-add-desc']
	// 		), "address_id = '".$applicant['applicant_present_id']."'");
	// 	}
	// 	else{
	// 		$applicant['applicant_present_id'] = $addressMapper->insert(array(
	// 			"address_city_id"		=> $_POST['present-add-city']
	// 		,	"address_province_id"=>$_POST['present-add-province']
	// 		,	"address_desc"	=>$_POST['present-add-desc']
	// 		));
	// 	}
	//
	// 	if($applicant['applicant_permanent_add_id']){
	// 		//DO update if already exist
	// 		$addressMapper->update(array(
	// 				"address_city_id"		=>$_POST['permanent-add-city']
	// 			,	"address_province_id"=>$_POST['permanent-add-province']
	// 			,	"address_desc"	=>$_POST['permanent-add-desc']
	// 		), "address_id = '".$applicant['applicant_present_id']."'");
	// 	}
	// 	else{
	//
	// 		$applicant['applicant_permanent_add_id'] = $addressMapper->insert(array(
	// 			"address_city_id"		=> $_POST['permanent-add-city']
	// 		,	"address_province_id"=>$_POST['permanent-add-province']
	// 		,	"address_desc"	=>$_POST['permanent-add-desc']
	// 		));
	// 	}
	//
	// 	$basicContactMapper = new App\Mapper\BasicContactMapper();
	// 	$basicContactMapper->update(array(
	// 			'bc_first_name' => $_POST['applicant-first-name']
	// 		,	'bc_middle_name' => $_POST['applicant-middle-name']
	// 		,	'bc_last_name' => $_POST['applicant-last-name']
	// 		,	'bc_name_ext' => $_POST['applicant-name-ext']
	// 		,	'bc_phone_num1' => $_POST['phone-number-1']
	// 		,	'bc_phone_num2' => $_POST['phone-number-2']
	// 		,	'bc_phone_num3' => $_POST['phone-number-3']
	// 		,	'bc_gender' => $_POST['applicant-gender']
	// 	), "bc_id = '".$applicant['applicant_bc_id']."'");
	//
	// 	$applicantMapper->update(array(
	// 			'applicant_nationality'=> $_POST['applicant-nationality']
	// 		,	'applicant_citizenship'=> $_POST['applicant-citizenship']
	// 		,	'applicant_civil_status'=> $_POST['applicant-civil-status']
	// 		,	'applicant_birthday'=> ($_POST['applicant-birthday'])? date("Y-m-d", strtotime($_POST['applicant-birthday'])) : NULL
	// 		,	'applicant_present_id'=> $applicant['applicant_present_id']
	// 		,	'applicant_permanent_add_id'=> $applicant['applicant_permanent_add_id']
	// 	), " applicant_id = '".$applicant['applicant_id']."'");
	//
	// 	$educ_table = json_decode($_POST['educ-table'], true);
	//
	// 	foreach($educ_table as $educ){
	// 		$educ_row = array(
	// 					'educ_applicant_id' => $applicant['applicant_id']
	// 			,		'educ_school_id' => $educ['school']
	// 			,		'educ_fos_id' => $educ['field_of_study']
	// 			,		'educ_start_from' => $educ['start_date']
	// 			,		'educ_start_to' => $educ['end_date']
	// 			,		'educ_type' => $educ['educ_type']
	// 			,		'educ_degree' => $educ['course']
	// 			,		'educ_additional' => $educ['add_info']
	// 		);
	//
	// 		if($educ['action'] == 'add'){
	// 			$educationMapper->insert($educ_row);
	// 		}
	//
	// 	}
	// 	echo json_encode(array('success'=>1));
	// }
	//
  // public function update_profile($username = ""){
	// 	//View only here no saving!
	// 	$userMapper = new App\Mapper\UserMapper();
	// 	$applicantMapper = new App\Mapper\ApplicantMapper();
	// 	$basicContactMapper = new App\Mapper\BasicContactMapper();
	// 	$educAttainmentMapper = new App\Mapper\EducAttainmentMapper();
	// 	$addressMapper = new App\Mapper\AddressMapper();
	// 	$educationMapper = new App\Mapper\EducationMapper();
	// 	$user = null;
	// 	$applicant = null;
	// 	if($this->sess->isLogin()){
	// 		$user = $userMapper->getByFilter(array(
	// 			array(
	// 				'column'=>'user_id'
	// 			,	'value'	=>$_SESSION['current_user']['id']
	// 			)
	// 		), true);
	// 	}
	// 	if(!$user){
	// 		$user = $userMapper->getByFilter(array(
	// 			array(
	// 				'column'=>'user_name'
	// 			,	'value'	=>$username
	// 			)
	// 		), true);
	// 	}
	// 	if(!$user);//Show 404;
	// 	$applicant = $applicantMapper->getByFilter("applicant_user_id = '". $user['user_id']."' ", true);
	// 	$basicContact = $basicContactMapper->getByFilter("bc_id = '".$applicant['applicant_bc_id']."'", true);
	// 	$educAttainment = $educAttainmentMapper->getByFilter("ea_id = '".$applicant['applicant_ea_id']."'", true);
	// 	$presentAddress = $addressMapper->getCompleteAddressByID($applicant['applicant_present_id']);
	// 	$permanentAddress = $addressMapper->getCompleteAddressByID($applicant['applicant_permanent_add_id']);
	// 	$education = $educationMapper->getEducationTable($applicant['applicant_id']);
	//
	// 	$form_data = array(
	// 			'applicant_username'	=> $user['user_name']
	// 		,	'applicant_first_name' => $basicContact['bc_first_name']
	// 		,	'applicant_middle_name' => $basicContact['bc_middle_name']
	// 		,	'applicant_last_name' => $basicContact['bc_last_name']
	// 		,	'applicant_name_ext' => $basicContact['bc_name_ext']
	// 		,	'present_add_desc' => $presentAddress['address_desc']
	// 		,	'present_add_country' => array(
	// 				'country_id'	=> $presentAddress['country_id']
	// 			,	'country_name'=> $presentAddress['country_name']
	// 			)
	// 		,	'present_add_region' => array(
	// 				'region_id'	=> $presentAddress['region_id']
	// 			,	'region_code'=> $presentAddress['region_code']
	// 			,	'region_desc'=> $presentAddress['region_desc']
	// 			)
	// 		,	'present_add_province' => array(
	// 				'province_id'	=> $presentAddress['province_id']
	// 			,	'province_name'=> $presentAddress['province_name']
	// 			)
	// 		,	'present_add_city' => array(
	// 				'city_id'	=> $presentAddress['city_id']
	// 			,	'city_name'=> $presentAddress['city_name']
	// 			)
	// 		,	'permanent_add_desc' => $permanentAddress['address_desc']
	// 		,	'permanent_add_country' => array(
	// 			'country_id'	=> $permanentAddress['country_id']
	// 		,	'country_name'=> $permanentAddress['country_name']
	// 		)
	// 		,	'permanent_add_region' => array(
	// 				'region_id'	=> $permanentAddress['region_id']
	// 			,	'region_code'=> $permanentAddress['region_code']
	// 			,	'region_desc'=> $permanentAddress['region_desc']
	// 			)
	// 		,	'permanent_add_province' => array(
	// 				'province_id'	=> $permanentAddress['province_id']
	// 			,	'province_name'=> $permanentAddress['province_name']
	// 			)
	// 		,	'permanent_add_city' => array(
	// 				'city_id'	=> $permanentAddress['city_id']
	// 			,	'city_name'=> $permanentAddress['city_name']
	// 			)
	// 		,	'applicant_gender' => $basicContact['bc_gender']
	// 		,	'applicant_birthday' => date("m/d/Y", strtotime($applicant['applicant_birthday']))
	// 		,	'applicant_civil_status' => $applicant['applicant_civil_status']
	// 		,	'applicant_nationality' => $applicant['applicant_nationality']
	// 		,	'applicant_citizenship' => $applicant['applicant_citizenship']
	// 		,	'applicant_educ_attainment' => array(
	// 				'ea_id'	=> $educAttainment['ea_id']
	// 			,	'ea_name'=> $educAttainment['ea_name']
	// 			)
	// 		,	'phone_number_1' => $basicContact['bc_phone_num1']
	// 		,	'phone_number_2' => $basicContact['bc_phone_num2']
	// 		,	'phone_number_3' => $basicContact['bc_phone_num3']
	// 		,	'education_table'=> $education
	// 	);
	// 	$this->_data['form_data'] = $form_data;
	//
	// 	$this->is_secure = true;
  //   $this->_template = 'templates/applicant_main';
  //   $this->view('applicant/update_profile');
  // }
	//
	// public function my_skills(){
	// 	$userMapper = new App\Mapper\UserMapper();
	// 	$applicantMapper = new App\Mapper\ApplicantMapper();
	// 	$skillTagMapper = new App\Mapper\SkillTagMapper();
	// 	$user = null;
	// 	$applicant = null;
	// 	if($this->sess->isLogin()){
	// 		$user = $userMapper->getByFilter(array(
	// 			array(
	// 				'column'=>'user_id'
	// 			,	'value'	=>$_SESSION['current_user']['id']
	// 			)
	// 		), true);
	// 	}
	// 	if(!$user){
	// 		$user = $userMapper->getByFilter(array(
	// 			array(
	// 				'column'=>'user_name'
	// 			,	'value'	=>$username
	// 			)
	// 		), true);
	// 	}
	// 	if(!$user){ };//Show 404;
	// 	$this->_data['skill_list'] = $skillTagMapper->getAll();
	// 	$this->_template = 'templates/applicant_main';
  //   $this->view('applicant/my_skills');
	// }


}
