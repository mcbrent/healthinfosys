<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends Controller {

	public function __construct(){
		parent::__construct();
	}

	public function login(){
		if(!empty($_POST)){
			$userMapper = new App\Mapper\UserMapper();
			$basicContactMapper = new App\Mapper\BasicContactMapper();

			$user = array();
			if($_POST['method'] == 'login'){
				$user_email = $_POST['user_email'];
				$user_password =  encrypt($_POST['user_password']);
				$user = $userMapper->selectByLoginPassword($user_email, $user_password);
			}
			else{
				$user = $userMapper->getByFilter("user_qr_msg = '".$_POST['qr-code']."'", true);
			}
			//

			if($user){
				$bc_id = 0;
				$basicContact;
				if($user['user_type'] == 1){
					//Admin
					$adminMapper = new App\Mapper\AdminMapper();
					//$admin = $adminMapper->getByID($user['user_id']);
					$admin = $adminMapper->getByFilter("admin_user_id = '".$user['user_id']."'", true);
					$bc_id = $admin['admin_bc_id'];
				}
				if($user['user_type'] == 2){
					//Resident
					$residentMapper = new App\Mapper\ResidentMapper();
					$resident = $residentMapper->getByFilter("resident_user_id = '".$user['user_id']."'", true);
					$bc_id = $resident['resident_bc_id'];
				}
				if($user['user_type'] == 3){
					//Pharmacist
					$pharmacistMapper = new App\Mapper\PharmacistMapper();
					$pharmacist = $pharmacistMapper->getByFilter("pharmacist_user_id = '".$user['user_id']."'", true);
					$bc_id = $pharmacist['pharmacist_bc_id'];
				}
				if($user['user_type'] == 4){
					//Medical Officer
					$medicalOfficerMapper = new App\Mapper\MedicalOfficerMapper();
					$medicalOfficer = $medicalOfficerMapper->getByFilter("mo_user_id = '".$user['user_id']."'", true);
					$bc_id = $medicalOfficer['mo_bc_id'];
				}
				if($user['user_type'] == 5){
					//Barangay Health Worker
					$barangayHealthWorkerMapper = new App\Mapper\BarangayHealthWorkerMapper();
					$barangayHealthWorker = $barangayHealthWorkerMapper->getByFilter("bhws_user_id = '".$user['user_id']."'", true);
					$bc_id = $barangayHealthWorker['bhws_id'];
				}

				$basicContact = $basicContactMapper->getByID($bc_id);
				$displayname = $basicContact['bc_first_name'] . ' ' . $basicContact['bc_middle_name'] . ' ' . $basicContact['bc_last_name'] . ' ' . $basicContact['bc_name_ext'];
				$user_details = array(
						'displayname'=>	$displayname
					,	'id'=>	$user['user_id']
					,	'email'		=>$user['user_email']
					,	'type'		=>$user['user_type']
				);
				$this->sess->setLogin($user_details);
				if(isset($_GET['redirect_url'])){
					$this->redirect($_GET['redirect_url']);
				}
				else{
					$this->redirect(DOMAIN.'dashboard');
				}
			}
			else{
				echo "Invalid User Credentials";
			}
		}
		if(!$this->sess->isLogin()){
			$this->_data['action_url'] = (isset($_GET['redirect_url']))? $_SERVER['REQUEST_URI'] : '?';
			$this->view('auth/login');
		}
		else{
			$this->redirect(DOMAIN.'dashboard');
		}

	}

	public function logout(){
		$this->sess->clearLogin();
		$this->redirect(DOMAIN.'login');
	}

}
