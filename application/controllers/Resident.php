<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Resident extends Controller {
	//TYSN-2018-00000
	private $_default_province_id = 24;
	private $_default_city_id = 474;
	public function __construct(){
		parent::__construct();
	}

	public function resident_ref(){
		$limit = $_POST['length'];
		$offset = $_POST['start'];
		$search = $_POST['search'];
		$columns = $_POST['columns'];
		$params = isset($_POST['filter'])? $_POST['filter']: array();
		$orders = array();

		foreach($_POST['order'] as $_order){
			array_push($orders, array(
				'col'=> $_POST['columns'][$_order['column']]['data']
			,	'type'	=> $_order['dir']
			));
		}
		$mapper = new App\Mapper\ResidentMapper();
		$result = $mapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders,$params);

		echo json_encode($result);
	}

	public function resident_lookup(){
		$filter = $_POST['term'];

		$residentMapperpper = new App\Mapper\ResidentMapper();
		$result = $residentMapperpper->selectByNameOrNo($filter);

		echo json_encode($result);
	}

	public function print(){
		// $filters = $this->input->post();
    // $this->load->model('app/reports/daily_sales_company_model');

		$data = array();
    // $data['filter'] = $filters;
    // $data['month_date'] = date('m/d/Y', strtotime('-6 months', strtotime($filters['start_date'])));
    // $data['lastyear_date'] = date('m/d/Y', strtotime('-12 months', strtotime($filters['start_date'])));
		//
    // $data['company'] = $this->daily_sales_company_model->getCompanyName($filters);
    // $data['rows'] = $this->daily_sales_company_model->getReport($filters);
		$html = $this->load->view('report/personal_data_sheet', $data, true);

    $this->load->library('MPdf');
    //echo $html;
		$this->mpdf->generate(
			array(	'format'=>'Legal',
					'orientation'=>'P',
					'html'=>$html
			));
	}

	public function save_file(){
		$option = $_POST;
		$this->load->model('FileManagement/Copy_Model');
		$residentAttachmentMapper = new App\Mapper\ResidentAttachmentMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();

		$residentAttachment = $residentAttachmentMapper->getByFilter("rattachment_id = '". $option['id']."' ", true);
		$fileManager = $fileManagerMapper->getByFilter("fm_id = '". $residentAttachment['rattachment_fm_id']."' ", true);

		$file = $this->Copy_Model->copyFile(array(
			'file_name' => empty($residentAttachment['rattachment_name'])? 'Untitled' : $residentAttachment['rattachment_name']
		,	'encrypted_name'=>$fileManager['fm_encypted_name']
		));
		echo json_encode($file);
	}

	public function delete_file(){
		$option = $_POST;
		$this->load->model('FileManagement/Upload_Model');
		$residentAttachmentMapper = new App\Mapper\ResidentAttachmentMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();

		$residentAttachment = $residentAttachmentMapper->getByFilter("rattachment_id = '". $option['id']."' ", true);
		$fileManager = $fileManagerMapper->getByFilter("fm_id = '". $residentAttachment['rattachment_fm_id']."' ", true);

		$file_path = 'upload/files/'.$fileManager['fm_encypted_name'];
		if(file_exists($file_path)){
			$this->Upload_Model->delete_file($file_path);
		}
		//echo
		$residentAttachmentMapper->delete(array(
			array(
							'column'=>'rattachment_id'
						,	'value'=> $option['id'])
		));
		$result = $fileManagerMapper->delete(array(
			array(
							'column'=>'fm_id'
						,	'value'=> $fileManager['fm_id'])
		));
		echo json_encode($result);
	}



	public function list(){
		$this->is_secure = true;
		$this->all_resident();
	}
	private function barangay_resident(){

	}

	private function all_resident(){
		$current_barangay = array();
		$barangayMapper = new App\Mapper\BarangayMapper();
		$barangayList = $barangayMapper->getAll();

		if($this->sess->isLogin()){
			if($_SESSION['current_user']['type'] == '5'){
				$userMapper = new App\Mapper\UserMapper();
				$barangayHealthWorkerMapper = new App\Mapper\BarangayHealthWorkerMapper();
				$user = $userMapper->getByFilter("user_id = '".$_SESSION['current_user']['id']."'", true);
				$barangayHealthWorker = $barangayHealthWorkerMapper->getByFilter("bhws_user_id = '".$user['user_id']."'", true);
				$current_barangay = $barangayMapper->getByFilter("brgy_id = '".$barangayHealthWorker['bhws_brgy_id']."'", true);
			}
		}
		$this->_data['brgy'] = $current_barangay;
		$this->_data['brgy_list'] = $barangayList;
		$this->view('resident/list');
	}

	public function add_resident(){
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$userMapper = new App\Mapper\UserMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();

		$data = array(
				'resident_no' => '<This will be generated upon saving>'
			,	'resident_first_name' => ''
			,	'resident_middle_name' => ''
			,	'resident_last_name' => ''
			,	'resident_name_ext' => ''
			,	'resident_maiden_name' => ''
			,	'add_desc'	=> ''
			,	'address_brgy_id' => ''
			,	'address_brgy' => array(
					'brgy_id'	=> ''
				,	'brgy_name'=> ''
				)
			,	'resident_gender'=>'male'
			,	'resident_blood_type'=>''
			,	'resident_birthday' => ''
			,	'resident_civil_status'=>''
			,	'resident_father_first_name'=>''
			,	'resident_father_middle_name'=>''
			,	'resident_father_last_name'=>''
			,	'resident_father_name_ext'=>''
			,	'resident_mother_first_name'=>''
			,	'resident_mother_middle_name'=>''
			,	'resident_mother_last_name'=>''
			,	'resident_mother_name_ext'=>''
			,	'resident_employment_status'=>''
			,	'phone_number_1'=>''
			,	'phone_number_2'=>''
			,	'resident_add_info'=>''
			,	'resident_attachment'=>array()
			,	'resident_occupation'=>''
			,	'resident_religion'=>''
			,	'resident_birthplace'=>''
			,	'resident_householdno'=>''
			,	'resident_philhealth'=>''
			,	'resident_philhealth_status'=>''
			,	'resident_philhealthno'=>''
			,	'resident_philhealthcat'=>''
			,	'resident_allergies'=>''
			,	'resident_family_history'=>''
			,	'appointment_list'=> array()
		);

		if(!empty($_POST)){
			$current_user_id = $_SESSION['current_user']['id'];
			$bc_id = $basicContactMapper->insert(array(
					'bc_first_name' => $_POST['resident-first-name']
				,	'bc_middle_name'=> $_POST['resident-middle-name']
				,	'bc_last_name'=> $_POST['resident-last-name']
				,	'bc_name_ext'=> $_POST['resident-name-ext']
				,	'bc_maiden_name'=>$_POST['resident-maiden-name']
				,	'bc_birthdate'=> date("Y-m-d", strtotime($_POST['resident-birthday']))
				,	'bc_phone_num1'=> $_POST['phone-number-1']
				,	'bc_phone_num2'=> $_POST['phone-number-2']
				,	'bc_phone_num3'=> ''
				,	'bc_gender'=>	$_POST['resident-gender']
			));

			$parent_father_bc_id = $basicContactMapper->insert(array(
					'bc_first_name' => $_POST['resident-father-first-name']
				,	'bc_middle_name'=> $_POST['resident-father-middle-name']
				,	'bc_last_name'=> $_POST['resident-father-last-name']
				,	'bc_name_ext'=> $_POST['resident-father-name-ext']
				,	'bc_maiden_name'=>''
				,	'bc_phone_num1'=> ''
				,	'bc_phone_num2'=> ''
				,	'bc_phone_num3'=> ''
				,	'bc_gender'=>	'male'
			));

			$parent_mother_bc_id = $basicContactMapper->insert(array(
					'bc_first_name' => $_POST['resident-mother-first-name']
				,	'bc_middle_name'=> $_POST['resident-mother-middle-name']
				,	'bc_last_name'=> $_POST['resident-mother-last-name']
				,	'bc_name_ext'=> $_POST['resident-mother-name-ext']
				,	'bc_maiden_name'=>''
				,	'bc_phone_num1'=> ''
				,	'bc_phone_num2'=> ''
				,	'bc_phone_num3'=> ''
				,	'bc_gender'=>	'female'
			));

			$resident_address_id = $addressMapper->insert(array(
				'address_city_id' 		=> $this->_default_city_id
			,	'address_province_id' => $this->_default_province_id
			,	'address_brgy_id' => $_POST['current-barangay']
			,	'address_desc' => $_POST['add-desc']
			));

			$this->load->library('Phpqrcode');

			$generatedKey = $userMapper->generateKey();

			$fm_encrypted_name = $this->phpqrcode->generate(array(
				'message'	=>$generatedKey
			));

			$fm_id = $fileManagerMapper->insert(array(
				'fm_encypted_name' => $fm_encrypted_name
			));

			$user_id = $userMapper->insert(array(
				'user_name'=>strtolower(substr($_POST['resident-first-name'],0, 2).$_POST['resident-last-name'])
			,	'user_password'	=>encrypt('123456')
			,	'user_email'	=>''
			,	'user_type'		=>'2'
			,	'user_qr_msg'	=>$generatedKey
			,	'user_qr_fm_id'		=>$fm_id
			));

			$resident_id = $residentMapper->insert(array(
				'resident_bc_id'	=> $bc_id
			,	'resident_no'	=> ''
			,	'resident_user_id'=>$user_id
			,	'resident_address_id'	=> $resident_address_id
			,	'resident_father_bc_id'	=> $parent_father_bc_id
			,	'resident_mother_bc_id'	=> $parent_mother_bc_id
			,	'resident_emp_status'	=>	$_POST['resident-employment-status']
			,	'resident_civil_status'	=> $_POST['resident-civil-status']
			,	'resident_birthplace'	=>	$_POST['resident-birthplace']
			,	'resident_blood_type'	=>	$_POST['resident-blood']
			,	'resident_occupation'	=>	$_POST['resident-occupation']
			,	'resident_religion'=> $_POST['resident-religion']
			,	'resident_householdno'=>$_POST['resident-household-no']
			,	'resident_philhealth'=>$_POST['resident-philhealth']
			,	'resident_philhealth_status'=>$_POST['resident-philhealth-status']
			,	'resident_philhealthno'=>$_POST['resident-philhealth-no']
			,	'resident_philhealthcat'=>$_POST['resident-philhealth-cat']
			,	'resident_allergies'=>$_POST['resident-allergies']
			,	'resident_family_history'=>$_POST['resident-family-history']
			,	'resident_add_info'=> $_POST['resident-additional-info']
			));



			$residentMapper->update(array(
				'resident_no'=>$residentMapper->generate_no($resident_id)
			), "resident_id = '".$resident_id."'");



			$this->set_alert(array(
				'message'=>'Resident Successful Added'
			,	'type'=>'success'
			));
			$this->redirect(DOMAIN.'resident/edit-resident/'.$resident_id);
		}

		$this->_data['action'] = 'add';
		$this->_data['form_data'] = $data;
		$this->is_secure = true;
    $this->view('resident/form');
	}

	public function edit_resident($resident_id){
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$userMapper = new App\Mapper\UserMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$barangayMapper = new App\Mapper\BarangayMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$residentAttachmentMapper = new App\Mapper\ResidentAttachmentMapper();
		$appointmentMapper = new App\Mapper\AppointmentMapper();

		$resident = $residentMapper->getByFilter("resident_id = '".$resident_id."'", true);
		$user = $userMapper->getByFilter("user_id = '".$resident['resident_user_id']."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
		$fatherBasicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_father_bc_id']."'", true);
		$motherBasicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_mother_bc_id']."'", true);
		$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
		$brgy = $barangayMapper->getByFilter("brgy_id = '".$address['address_brgy_id']."'", true);
		$residentAttachment = $residentAttachmentMapper->getByFilter("rattachment_resident_id = '".$resident_id."'");
		$qrImage = $fileManagerMapper->getByFilter("fm_id = '".$user['user_qr_fm_id']."'", true);

		if(!empty($_POST)){
			$upload_list = array();

			$fileAttachment = $_FILES['file-attachment'];
			$numOfFiles = count($fileAttachment['name']);
			$this->load->model('FileManagement/Upload_Model');
			for($i=0;$i<$numOfFiles;$i++){
				array_push($upload_list, array(
					'name'=>$fileAttachment['name'][$i]
				,	'type'=>$fileAttachment['type'][$i]
				,	'tmp_name'=>$fileAttachment['tmp_name'][$i]
				,	'error'=>$fileAttachment['error'][$i]
				,	'size'=>$fileAttachment['size'][$i]
				));
			}

			$residentMapper->update(array(
					'resident_emp_status'	=>	$_POST['resident-employment-status']
				,	'resident_civil_status'	=> $_POST['resident-civil-status']
				,	'resident_birthplace'	=>	$_POST['resident-birthplace']
				,	'resident_blood_type'	=>	$_POST['resident-blood']
				,	'resident_occupation'	=>	$_POST['resident-occupation']
				,	'resident_religion'=> $_POST['resident-religion']
				,	'resident_householdno'=>$_POST['resident-household-no']
				,	'resident_philhealth'=>$_POST['resident-philhealth']
				,	'resident_philhealth_status'=>$_POST['resident-philhealth-status']
				,	'resident_philhealthno'=>$_POST['resident-philhealth-no']
				,	'resident_philhealthcat'=>$_POST['resident-philhealth-cat']
				,	'resident_allergies'=>$_POST['resident-allergies']
				,	'resident_family_history'=>$_POST['resident-family-history']
				,	'resident_add_info'=> $_POST['resident-additional-info']
			), "resident_id = '".$resident['resident_id']."'");

			$basicContactMapper->update(array(
					'bc_first_name' => $_POST['resident-first-name']
				,	'bc_middle_name'=> $_POST['resident-middle-name']
				,	'bc_last_name'=> $_POST['resident-last-name']
				,	'bc_name_ext'=> $_POST['resident-name-ext']
				,	'bc_maiden_name'=>$_POST['resident-maiden-name']
				,	'bc_birthdate'=> date("Y-m-d", strtotime($_POST['resident-birthday']))
				,	'bc_phone_num1'=> $_POST['phone-number-1']
				,	'bc_phone_num2'=> $_POST['phone-number-2']
				,	'bc_phone_num3'=> ''
				,	'bc_gender'=>	$_POST['resident-gender']
			), "bc_id = '".$resident['resident_bc_id']."'");

			//Father
			$basicContactMapper->update(array(
					'bc_first_name' => $_POST['resident-father-first-name']
				,	'bc_middle_name'=> $_POST['resident-father-middle-name']
				,	'bc_last_name'=> $_POST['resident-father-last-name']
				,	'bc_name_ext'=> $_POST['resident-father-name-ext']
				,	'bc_maiden_name'=>''
				, 'bc_phone_num1'=> ''
				,	'bc_phone_num2'=> ''
				,	'bc_phone_num3'=> ''
				,	'bc_gender'=>	'male'
			), "bc_id = '".$resident['resident_father_bc_id']."'");

			//Mother
			$basicContactMapper->update(array(
					'bc_first_name' => $_POST['resident-mother-first-name']
				,	'bc_middle_name'=> $_POST['resident-mother-middle-name']
				,	'bc_last_name'=> $_POST['resident-mother-last-name']
				,	'bc_name_ext'=> $_POST['resident-mother-name-ext']
				,	'bc_maiden_name'=>''
				,	'bc_phone_num1'=> ''
				,	'bc_phone_num2'=> ''
				,	'bc_phone_num3'=> ''
				,	'bc_gender'=>	'female'
			), "bc_id = '".$resident['resident_mother_bc_id']."'");


			$addressMapper->update(array(
					'address_brgy_id' => $_POST['current-barangay']
				,	'address_desc' => $_POST['add-desc']
			), "address_id = '".$resident['resident_address_id']."'");

			$this->load->model('FileManagement/Upload_Model');

			foreach($upload_list as $i=>$temp_file){
					if($temp_file['error'] == '0'){
						$result = $this->Upload_Model->upload_file($temp_file);
						$fm_id = $fileManagerMapper->insert(array(
							'fm_encypted_name'	=> $result['new_file_name']
						));

						$ra_id = $residentAttachmentMapper->insert(array(
							'rattachment_fm_id'=>$fm_id
						,	'rattachment_resident_id'=>$resident['resident_id']
						,	'rattachment_name'=>$_POST['file-name'][$i]
						));
					}
			}
			$resident = $residentMapper->getByFilter("resident_id = '".$resident_id."'", true);
			$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
			$fatherBasicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_father_bc_id']."'", true);
			$motherBasicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_mother_bc_id']."'", true);
			$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
			$brgy = $barangayMapper->getByFilter("brgy_id = '".$address['address_brgy_id']."'", true);
			$residentAttachment = $residentAttachmentMapper->getByFilter("rattachment_resident_id = '".$resident_id."'");
			$this->set_alert(array(
				'message'=>'<i class="fa fa-check"></i> Resident Information Successfully Updated'
			,	'type'=>'success'
			));
		}
		$appointment = $appointmentMapper->getMedicalHistory($resident_id);

		$data = array(
				'resident_no' => $resident['resident_no']
			,	'resident_first_name' => $basicContact['bc_first_name']
			,	'resident_middle_name' => $basicContact['bc_middle_name']
			,	'resident_last_name' => $basicContact['bc_last_name']
			,	'resident_name_ext' => $basicContact['bc_name_ext']
			,	'resident_maiden_name' => $basicContact['bc_maiden_name']
			,	'add_desc'	=> $address['address_desc']
			,	'address_brgy_id' => $address['address_brgy_id']
			,	'address_brgy' => array(
					'brgy_id'	=> $brgy['brgy_id']
				,	'brgy_name'=> $brgy['brgy_name']
				)
			,	'resident_gender'=>$basicContact['bc_gender']
			,	'resident_blood_type'=>$resident['resident_blood_type']
			,	'resident_birthday' => date("m/d/Y", strtotime($basicContact['bc_birthdate']))
			,	'resident_civil_status'=>$resident['resident_civil_status']
			,	'resident_father_first_name'=>$fatherBasicContact['bc_first_name']
			,	'resident_father_middle_name'=>$fatherBasicContact['bc_middle_name']
			,	'resident_father_last_name'=>$fatherBasicContact['bc_last_name']
			,	'resident_father_name_ext'=>$fatherBasicContact['bc_name_ext']
			,	'resident_mother_first_name'=>$motherBasicContact['bc_first_name']
			,	'resident_mother_middle_name'=>$motherBasicContact['bc_middle_name']
			,	'resident_mother_last_name'=>$motherBasicContact['bc_last_name']
			,	'resident_mother_name_ext'=>$motherBasicContact['bc_name_ext']
			,	'resident_employment_status'=>$resident['resident_emp_status']
			,	'phone_number_1'=>$basicContact['bc_phone_num1']
			,	'phone_number_2'=>$basicContact['bc_phone_num2']
			,	'resident_add_info'=>$resident['resident_add_info']
			,	'resident_attachment'=>$residentAttachment
			,	'resident_occupation'=>$resident['resident_occupation']
			,	'resident_religion'=>$resident['resident_religion']
			,	'resident_birthplace'=>$resident['resident_birthplace']
			,	'resident_householdno'=>$resident['resident_householdno']
			,	'resident_philhealth'=>$resident['resident_philhealth']
			,	'resident_philhealth_status'=>$resident['resident_philhealth_status']
			,	'resident_philhealthno'=>$resident['resident_philhealthno']
			,	'resident_philhealthcat'=>$resident['resident_philhealthcat']
			,	'resident_allergies'=>$resident['resident_allergies']
			,	'resident_family_history'=>$resident['resident_family_history']
			,	'qrcode_url'	=>	$qrImage['fm_encypted_name']
			,	'appointment_list'=> $appointment
		);

		$this->_data['action'] = 'edit';
		$this->_data['form_data'] = $data;
		$this->is_secure = true;
    $this->view('resident/form');
	}

	public function my_profile(){
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$barangayMapper = new App\Mapper\BarangayMapper();
		$userMapper = new App\Mapper\UserMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$residentAttachmentMapper = new App\Mapper\ResidentAttachmentMapper();
		$appointmentMapper = new App\Mapper\AppointmentMapper();

		$user_id = $_SESSION['current_user']['id'];
		$user = $userMapper->getByFilter("user_id = '".$user_id."'", true);

		$resident = $residentMapper->getByFilter("resident_user_id = '".$user_id."'", true);
		$resident_id = $resident['resident_id'];
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
		$fatherBasicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_father_bc_id']."'", true);
		$motherBasicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_mother_bc_id']."'", true);
		$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
		$brgy = $barangayMapper->getByFilter("brgy_id = '".$address['address_brgy_id']."'", true);
		$residentAttachment = $residentAttachmentMapper->getByFilter("rattachment_resident_id = '".$resident_id."'");
		$qrImage = $fileManagerMapper->getByFilter("fm_id = '".$user['user_qr_fm_id']."'", true);
		if(!empty($_POST)){
			$upload_list = array();

			$fileAttachment = $_FILES['file-attachment'];
			$numOfFiles = count($fileAttachment['name']);
			$this->load->model('FileManagement/Upload_Model');
			for($i=0;$i<$numOfFiles;$i++){
				array_push($upload_list, array(
					'name'=>$fileAttachment['name'][$i]
				,	'type'=>$fileAttachment['type'][$i]
				,	'tmp_name'=>$fileAttachment['tmp_name'][$i]
				,	'error'=>$fileAttachment['error'][$i]
				,	'size'=>$fileAttachment['size'][$i]
				));
			}

			$residentMapper->update(array(
					'resident_emp_status'	=>	$_POST['resident-employment-status']
				,	'resident_civil_status'	=> $_POST['resident-civil-status']
				,	'resident_birthplace'	=>	$_POST['resident-birthplace']
				,	'resident_blood_type'	=>	$_POST['resident-blood']
				,	'resident_occupation'	=>	$_POST['resident-occupation']
				,	'resident_religion'=> $_POST['resident-religion']
				,	'resident_householdno'=>$_POST['resident-household-no']
				,	'resident_philhealth'=>$_POST['resident-philhealth']
				,	'resident_philhealth_status'=>$_POST['resident-philhealth-status']
				,	'resident_philhealthno'=>$_POST['resident-philhealth-no']
				,	'resident_philhealthcat'=>$_POST['resident-philhealth-cat']
				,	'resident_add_info'=> $_POST['resident-additional-info']
			), "resident_id = '".$resident['resident_id']."'");

			$basicContactMapper->update(array(
					'bc_first_name' => $_POST['resident-first-name']
				,	'bc_middle_name'=> $_POST['resident-middle-name']
				,	'bc_last_name'=> $_POST['resident-last-name']
				,	'bc_name_ext'=> $_POST['resident-name-ext']
				,	'bc_maiden_name'=>$_POST['resident-maiden-name']
				,	'bc_phone_num1'=> $_POST['phone-number-1']
				,	'bc_phone_num2'=> $_POST['phone-number-2']
				,	'bc_phone_num3'=> ''
				,	'bc_gender'=>	$_POST['resident-gender']
			), "bc_id = '".$resident['resident_bc_id']."'");

			//Father
			$basicContactMapper->update(array(
					'bc_first_name' => $_POST['resident-father-first-name']
				,	'bc_middle_name'=> $_POST['resident-father-middle-name']
				,	'bc_last_name'=> $_POST['resident-father-last-name']
				,	'bc_name_ext'=> $_POST['resident-father-name-ext']
				,	'bc_maiden_name'=>''
				,	'bc_phone_num1'=> ''
				,	'bc_phone_num2'=> ''
				,	'bc_phone_num3'=> ''
				,	'bc_gender'=>	'male'
			), "bc_id = '".$resident['resident_father_bc_id']."'");

			//Mother
			$basicContactMapper->update(array(
					'bc_first_name' => $_POST['resident-mother-first-name']
				,	'bc_middle_name'=> $_POST['resident-mother-middle-name']
				,	'bc_last_name'=> $_POST['resident-mother-last-name']
				,	'bc_name_ext'=> $_POST['resident-mother-name-ext']
				,	'bc_maiden_name'=>''
				,	'bc_phone_num1'=> ''
				,	'bc_phone_num2'=> ''
				,	'bc_phone_num3'=> ''
				,	'bc_gender'=>	'female'
			), "bc_id = '".$resident['resident_mother_bc_id']."'");


			$addressMapper->update(array(
					'address_brgy_id' => $_POST['current-barangay']
				,	'address_desc' => $_POST['add-desc']
			), "address_id = '".$resident['resident_address_id']."'");

			$this->load->model('FileManagement/Upload_Model');

			foreach($upload_list as $i=>$temp_file){
					if($temp_file['error'] == '0'){
						$result = $this->Upload_Model->upload_file($temp_file);
						$fm_id = $fileManagerMapper->insert(array(
							'fm_encypted_name'	=> $result['new_file_name']
						));

						$ra_id = $residentAttachmentMapper->insert(array(
							'rattachment_fm_id'=>$fm_id
						,	'rattachment_resident_id'=>$resident['resident_id']
						,	'rattachment_name'=>$_POST['file-name'][$i]
						));
					}
			}
			$resident = $residentMapper->getByFilter("resident_id = '".$resident_id."'", true);
			$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
			$fatherBasicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_father_bc_id']."'", true);
			$motherBasicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_mother_bc_id']."'", true);
			$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
			$brgy = $barangayMapper->getByFilter("brgy_id = '".$address['address_brgy_id']."'", true);
			$residentAttachment = $residentAttachmentMapper->getByFilter("rattachment_resident_id = '".$resident_id."'");

			$this->set_alert(array(
				'message'=>'<i class="fa fa-check"></i> Resident Information Successfully Added'
			,	'type'=>'success'
			));
		}
		$appointment = $appointmentMapper->getMedicalHistory($resident_id);
		$data = array(
				'resident_no' => $resident['resident_no']
			,	'resident_first_name' => $basicContact['bc_first_name']
			,	'resident_middle_name' => $basicContact['bc_middle_name']
			,	'resident_last_name' => $basicContact['bc_last_name']
			,	'resident_name_ext' => $basicContact['bc_name_ext']
			,	'resident_maiden_name' => $basicContact['bc_maiden_name']
			,	'add_desc'	=> $address['address_desc']
			,	'address_brgy_id' => $address['address_brgy_id']
			,	'address_brgy' => array(
					'brgy_id'	=> $brgy['brgy_id']
				,	'brgy_name'=> $brgy['brgy_name']
				)
			,	'resident_gender'=>$basicContact['bc_gender']
			,	'resident_blood_type'=>$resident['resident_blood_type']
			,	'resident_birthday' => date("m/d/Y", strtotime($basicContact['bc_birthdate']))
			,	'resident_civil_status'=>$resident['resident_civil_status']
			,	'resident_father_first_name'=>$fatherBasicContact['bc_first_name']
			,	'resident_father_middle_name'=>$fatherBasicContact['bc_middle_name']
			,	'resident_father_last_name'=>$fatherBasicContact['bc_last_name']
			,	'resident_father_name_ext'=>$fatherBasicContact['bc_name_ext']
			,	'resident_father_birthday'=>date("m/d/Y", strtotime($fatherBasicContact['bc_birthdate']))
			,	'resident_mother_first_name'=>$motherBasicContact['bc_first_name']
			,	'resident_mother_middle_name'=>$motherBasicContact['bc_middle_name']
			,	'resident_mother_last_name'=>$motherBasicContact['bc_last_name']
			,	'resident_mother_name_ext'=>$motherBasicContact['bc_name_ext']
			,	'resident_mother_birthday'=>date("m/d/Y", strtotime($motherBasicContact['bc_birthdate']))
			,	'resident_employment_status'=>$resident['resident_emp_status']
			,	'phone_number_1'=>$basicContact['bc_phone_num1']
			,	'phone_number_2'=>$basicContact['bc_phone_num2']
			,	'resident_add_info'=>$resident['resident_add_info']
			,	'resident_attachment'=>$residentAttachment
			,	'resident_occupation'=>$resident['resident_occupation']
			,	'resident_religion'=>$resident['resident_religion']
			,	'resident_birthplace'=>$resident['resident_birthplace']
			,	'resident_householdno'=>$resident['resident_householdno']
			,	'resident_philhealth'=>$resident['resident_philhealth']
			,	'resident_philhealth_status'=>$resident['resident_philhealth_status']
			,	'resident_philhealthno'=>$resident['resident_philhealthno']
			,	'resident_philhealthcat'=>$resident['resident_philhealthcat']
			,	'resident_allergies'=>$resident['resident_allergies']
			,	'appointment_list'=> $appointment
			,	'resident_family_history'=>$resident['resident_family_history']
			,	'qrcode_url'=>$qrImage['fm_encypted_name']
		);



		$this->_data['action'] = 'edit';
		$this->_data['form_data'] = $data;
		$this->is_secure = true;
    $this->view('resident/form');
	}

	public function view_resident($resident_id){
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$barangayMapper = new App\Mapper\BarangayMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$residentAttachmentMapper = new App\Mapper\ResidentAttachmentMapper();
		$userMapper = new App\Mapper\UserMapper();
		$appointmentMapper = new App\Mapper\AppointmentMapper();

		$resident = $residentMapper->getByFilter("resident_id = '".$resident_id."'", true);
		$user = $userMapper->getByFilter("user_id = '".$resident['resident_user_id']."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
		$fatherBasicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_father_bc_id']."'", true);
		$motherBasicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_mother_bc_id']."'", true);
		$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
		$brgy = $barangayMapper->getByFilter("brgy_id = '".$address['address_brgy_id']."'", true);
		$residentAttachment = $residentAttachmentMapper->getByFilter("rattachment_resident_id = '".$resident_id."'");
		$qrImage = $fileManagerMapper->getByFilter("fm_id = '".$user['user_qr_fm_id']."'", true);
		$appointment = $appointmentMapper->getMedicalHistory($resident_id);
		$data = array(
				'resident_no' => $resident['resident_no']
			,	'resident_first_name' => $basicContact['bc_first_name']
			,	'resident_middle_name' => $basicContact['bc_middle_name']
			,	'resident_last_name' => $basicContact['bc_last_name']
			,	'resident_name_ext' => $basicContact['bc_name_ext']
			,	'resident_maiden_name' => $basicContact['bc_maiden_name']
			,	'add_desc'	=> $address['address_desc']
			,	'address_brgy_id' => $address['address_brgy_id']
			,	'address_brgy' => array(
					'brgy_id'	=> $brgy['brgy_id']
				,	'brgy_name'=> $brgy['brgy_name']
				)
			,	'resident_gender'=>$basicContact['bc_gender']
			,	'resident_blood_type'=>$resident['resident_blood_type']
			,	'resident_birthday' => date("m/d/Y", strtotime($basicContact['bc_birthdate']))
			,	'resident_civil_status'=>$resident['resident_civil_status']
			,	'resident_father_first_name'=>$fatherBasicContact['bc_first_name']
			,	'resident_father_middle_name'=>$fatherBasicContact['bc_middle_name']
			,	'resident_father_last_name'=>$fatherBasicContact['bc_last_name']
			,	'resident_father_name_ext'=>$fatherBasicContact['bc_name_ext']
			,	'resident_mother_first_name'=>$motherBasicContact['bc_first_name']
			,	'resident_mother_middle_name'=>$motherBasicContact['bc_middle_name']
			,	'resident_mother_last_name'=>$motherBasicContact['bc_last_name']
			,	'resident_mother_name_ext'=>$motherBasicContact['bc_name_ext']
			,	'resident_employment_status'=>$resident['resident_emp_status']
			,	'phone_number_1'=>$basicContact['bc_phone_num1']
			,	'phone_number_2'=>$basicContact['bc_phone_num2']
			,	'resident_add_info'=>$resident['resident_add_info']
			,	'resident_attachment'=>$residentAttachment
			,	'resident_occupation'=>$resident['resident_occupation']
			,	'resident_religion'=>$resident['resident_religion']
			,	'resident_birthplace'=>$resident['resident_birthplace']
			,	'resident_householdno'=>$resident['resident_householdno']
			,	'resident_philhealth'=>$resident['resident_philhealth']
			,	'resident_philhealth_status'=>$resident['resident_philhealth_status']
			,	'resident_philhealthno'=>$resident['resident_philhealthno']
			,	'resident_philhealthcat'=>$resident['resident_philhealthcat']
			,	'resident_allergies'=>$resident['resident_allergies']
			,	'resident_family_history'=>$resident['resident_family_history']
			,	'qrcode_url'	=>	$qrImage['fm_encypted_name']
			,	'appointment_list'=> $appointment
		);



		$this->_data['action'] = 'edit';
		$this->_data['form_data'] = $data;
		$this->is_secure = true;
    $this->view('resident/view');
	}

	public function delete_resident(){
		$params = $_POST;
		$resident_id = $params['id'];

		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$residentAttachmentMapper = new App\Mapper\ResidentAttachmentMapper();
		$userMapper = new App\Mapper\UserMapper();

		$resident = $residentMapper->getByFilter("resident_id = '".$resident_id."'", true);
		$user = $userMapper->getByFilter("user_id = '".$resident['resident_user_id']."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
		$dpImage = $fileManagerMapper->getByFilter("fm_id = '".$user['user_dp_fm_id']."'", true);
		$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
		$fatherBasicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_father_bc_id']."'", true);
		$motherBasicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_mother_bc_id']."'", true);
		$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
		$qrImage = $fileManagerMapper->getByFilter("fm_id = '".$user['user_qr_fm_id']."'", true);

		$residentMapper->delete("resident_id = '".$resident['resident_id']."'");
		$userMapper->delete("user_id = '".$user['user_id']."'");
		$basicContactMapper->delete("bc_id = '".$basicContact['bc_id']."'");
		$fileManagerMapper->delete("fm_id = '".$dpImage['fm_id']."'");
		$addressMapper->delete("address_id = '".$address['address_id']."'");
		$basicContactMapper->delete("bc_id = '".$fatherBasicContact['bc_id']."'");
		$basicContactMapper->delete("bc_id = '".$motherBasicContact['bc_id']."'");
		$addressMapper->delete("address_id = '".$address['address_id']."'");
		$residentAttachmentMapper->delete("rattachment_resident_id = '".$resident['resident_id']."'");
		$fileManagerMapper->delete("fm_id = '".$qrImage['fm_id']."'");

		$this->load->model('FileManagement/Upload_Model');
		if(!empty($dpImage)){
			$file_path = 'upload/profile/'.$dpImage['fm_encypted_name'];
			if(file_exists($file_path)){
				$this->Upload_Model->delete_file($file_path);
			}
		}

		if(!empty($qrImage)){
			$file_path = 'upload/qrcode/'.$qrImage['fm_encypted_name'];
			if(file_exists($file_path)){
				$this->Upload_Model->delete_file($file_path);
			}
		}

		echo json_encode(array('status'=>"Deleted"));
	}


	// public function admin_ref(){
	// 	$limit = $_POST['length'];
	// 	$offset = $_POST['start'];
	// 	$search = $_POST['search'];
	// 	$columns = $_POST['columns'];
	// 	$option = $_POST['option'];
	// 	$orders = array();
	//
	// 	foreach($_POST['order'] as $_order){
	// 		array_push($orders, array(
	// 			'col'=> $_POST['columns'][$_order['column']]['data']
	// 		,	'type'	=> $_order['dir']
	// 		));
	// 	}
	// 	$mapper = new App\Mapper\AdminMapper();
	//
	// 	$result = $mapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders);
	//
	// 	echo json_encode($result);
	// }
	//
	// public function add_admin(){
	// 	$userMapper = new App\Mapper\UserMapper();
	// 	$adminMapper = new App\Mapper\AdminMapper();
	// 	$basicContactMapper = new App\Mapper\BasicContactMapper();
	//
	// 	$regionMapper = new App\Mapper\RegionMapper();
	// 	$countryMapper = new App\Mapper\CountryMapper();
	// 	$data = array(
	// 			'region_id' => ''
	// 		,	'region_country_id' => ''
	// 		,	'region_code' => ''
	// 		,	'region_desc' => ''
	// 	);
	// 	if(!empty($_POST)){
	// 			$insert_basic_contact = array(
	// 				'bc_first_name'			=>$_POST['first-name']
	// 			,	'bc_middle_name'	=>$_POST['middle-name']
	// 			,	'bc_last_name'	=>$_POST['last-name']
	// 			,	'bc_name_ext'	=>$_POST['name-ext']
	// 			,	'bc_phone_num1'	=>''
	// 			,	'bc_phone_num2'	=>''
	// 			,	'bc_phone_num3'	=>''
	// 			,	'bc_gender'	=>$_POST['gender']
	// 			,	'bc_email_address'	=>$_POST['user-email']
	// 			);
	// 			$bc_id = $basicContactMapper->insert($insert_basic_contact);
	//
	// 			$insert_user = array(
	// 				'user_name'			=>$_POST['user-id']
	// 			,	'user_password'	=>$_POST['user-password']
	// 			,	'user_email'	=>$_POST['user-email']
	// 			,	'user_type'	=> 1
	// 			);
	// 			$userMapper->insert($insert_user);
	//
	// 			$insert_admin = array(
	// 				'admin_user_name'	=> $insert_user['user_name']
	// 			,	'admin_bc_id'	=> $bc_id
	// 			);
	// 			$adminMapper->insert($insert_admin);
	//
	// 	}
	// 	$this->_data['action'] = 'add';
	// 	$this->_data['form_data'] = $data;
	//
	// 	$this->_template = 'templates/admin_main';
  //   $this->view('administrator/form');
	// }




}
