<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Appointment extends Controller {

	private $_default_province_id = 24;
	private $_default_city_id = 474;

	public function __construct(){
		parent::__construct();
	}

	public function approve_appointment($appointment_id){
		$appointmentMapper = new App\Mapper\AppointmentMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$appointmentMapper = new App\Mapper\AppointmentMapper();
		$appointmentMapper = new App\Mapper\AppointmentMapper();
		$appointment = $appointmentMapper->getByFilter("appointment_id = '".$appointment_id."'", true);
		// echo "<pre>";
		// print_r($appointment);
		// echo "</pre>";
		$resident = $residentMapper->getByFilter("resident_id = '".$appointment['appointment_resident_id']."'", true);
		$data = $_POST;
		if(!empty($data)){
			$datetime = date('Y-m-d G:i:s', strtotime($data['schedule-date'].' '.$data['schedule-time'].':00:00'));

			$is_approved = ($data['action'] == 'approved')? '1': '0';
			$appointment['appointment_is_approved_sched'] = $is_approved;
			$appointment['appointment_approved_message'] = $data['appointment-approved-message'];
			$appointment['appointment_approved_sched'] = $datetime;
			$appointment['appointment_status'] = ($is_approved == '1')? '2' : '6';
			$appointmentMapper->update($appointment, "appointment_id = '".$appointment_id."'");
			if($is_approved == '1'){
				$this->set_alert(array(
					'type'=>'success'
				,	'message'=>'Successfully Approved Appointment Request'
				));
			}
			else{
				$this->set_alert(array(
					'type'=>'danger'
				,	'message'=>'Appointment Request has been declined'
				));
			}

			$this->redirect(DOMAIN.'appointment/request_appointment_list');

		}
		$form_data = array(
			'appointment_initial_summary' => $appointment['appointment_initial_summary']
		,	'appointment_requested_datetime' => $appointment['appointment_requested_datetime']
		);
		$this->_data['form_data'] = $form_data;
		$this->is_secure = true;
    $this->view('appointment/approve_appointment');
	}

	public function view_medical_treatment($appointment_id){
		//Admin Side, Medical Officer, Pharmacist
		$appointmentMapper = new App\Mapper\AppointmentMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$appointmentMapper = new App\Mapper\AppointmentMapper();
		$diagnosisMapper = new App\Mapper\DiagnosisMapper();
		$appointment = $appointmentMapper->getByFilter("appointment_id = '".$appointment_id."'", true);
		$resident = $residentMapper->getByFilter("resident_id = '".$appointment['appointment_resident_id']."'", true);
		$this->_data['diagnosis_list'] = $diagnosisMapper->getAll();
		$data = $_POST;
		if(!empty($data)){
			//$datetime = date('Y-m-d G:i:s', strtotime($data['schedule-date'].' '.$data['schedule-time'].':00:00'));
			$current_status = $appointment['appointment_status'];
			$datetime = date('Y-m-d G:i:s');

			if($data['action'] == 'complete' || $data['action'] == 'save'){
				if($data['action'] == 'save'){
					$appointment['appointment_status'] = '4';
				}
				else{
					$appointment['appointment_status'] = '5';
					$appointment['appointment_mo_id'] = $_SESSION['current_user']['id'];
				}

				if(!empty($appointment['appointment_check_datetime'])){
					$appointment['appointment_check_datetime'] = $datetime;
				}
				$appointment['appointment_diagnosis_type'] = $data['diagnosis-type'];
				$appointment['appointment_diagnosis_description'] = $data['diagnosis-description'];
				$appointment['appointment_diagnosed_datetime'] = $datetime;
				$this->set_alert(array(
					'type'=>'info'
				,	'message'=>'Patient\'s Medical Treatment / Checkup has been COMPLETED.'
				));
			}
			else if($data['action'] == 'check-in'){
				$appointment['appointment_status'] = '4';
				$appointment['appointment_check_datetime'] = $datetime;
				$this->set_alert(array(
					'type'=>'success'
				,	'message'=>'Patient has been marked CHECKED IN.'
				));
			}
			else if($data['action'] == 'cancel'){
				$appointment['appointment_status'] = '6';
				$this->set_alert(array(
					'type'=>'danger'
				,	'message'=>'Patient\'s Medical Treatment / Checkup has been CANCELLED.'
				));
			}
			$appointmentMapper->update($appointment, "appointment_id = '".$appointment['appointment_id']."'");
			$this->redirect(DOMAIN.'appointment/request_appointment_list');
		}
		$appointment = $appointmentMapper->getByFilter("appointment_id = '".$appointment_id."'", true);
		$status = $this->getAccessFromAppointment($appointment['appointment_status'], 'admin');


		$form_data = $appointment;
		$this->_data['form_data'] = $form_data;
		$this->_data['status'] = $status;
		$this->is_secure = true;
    $this->view('appointment/view_medical_treatment');
	}

	public function read_medical_treatment($appointment_id){
		//Admin Side, Medical Officer, Pharmacist
		$appointmentMapper = new App\Mapper\AppointmentMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$appointmentMapper = new App\Mapper\AppointmentMapper();
		$diagnosisMapper = new App\Mapper\DiagnosisMapper();
		$appointment = $appointmentMapper->getByFilter("appointment_id = '".$appointment_id."'", true);
		$resident = $residentMapper->getByFilter("resident_id = '".$appointment['appointment_resident_id']."'", true);
		$this->_data['diagnosis'] = $diagnosisMapper->getByFilter("dt_id = '".$appointment['appointment_diagnosis_type']."'", true);

		$appointment = $appointmentMapper->getByFilter("appointment_id = '".$appointment_id."'", true);
		$status = $this->getAccessFromAppointment($appointment['appointment_status'], 'admin');

		$form_data = $appointment;
		$this->_data['form_data'] = $form_data;
		$this->_data['status'] = $status;
		$this->is_secure = true;
    $this->view('appointment/read_medical_treatment');
	}

	public function view_appointment($appointment_id){
		$appointmentMapper = new App\Mapper\AppointmentMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$appointmentMapper = new App\Mapper\AppointmentMapper();
		$appointmentMapper = new App\Mapper\AppointmentMapper();
		$appointment = $appointmentMapper->getByFilter("appointment_id = '".$appointment_id."'", true);
		$resident = $residentMapper->getByFilter("resident_id = '".$appointment['appointment_resident_id']."'", true);
		$data = $_POST;
		if(!empty($data)){
			//$datetime = date('Y-m-d G:i:s', strtotime($data['schedule-date'].' '.$data['schedule-time'].':00:00'));

			$is_confirmed = ($data['action'] == 'confirm')? '1': '0';
			$appointment['appointment_is_confirm'] = $is_confirmed;
			$appointment['appointment_confirmed_datetime'] = date('Y-m-d G:i:s');
			$appointment['appointment_status'] = ($is_confirmed == '1')? '3' : '7';
			$appointmentMapper->update($appointment, "appointment_id = '".$appointment_id."'");
			if($is_confirmed == '1'){
				$this->set_alert(array(
					'type'=>'success'
				,	'message'=>'Successfully Confirmed Appointment. Be present on the scheduled appointment'
				));
			}
			else{
				$this->set_alert(array(
					'type'=>'danger'
				,	'message'=>'Appointment has been cancelled'
				));
			}
			//
			// $this->redirect(DOMAIN.'appointment/request_appointment_list');
		}
		$appointment = $appointmentMapper->getByFilter("appointment_id = '".$appointment_id."'", true);


		$status = $this->getAccessFromAppointment($appointment['appointment_status'], 'resident');

		$form_data = $appointment;
		$this->_data['form_data'] = $form_data;
		$this->_data['status'] = $status;
		$this->is_secure = true;
    $this->view('appointment/view_appointment');
	}

	public function quick_check_in(){
		$data = $_POST;
		$appointmentMapper = new App\Mapper\AppointmentMapper();

		$appointment = array(
			'appointment_status'=>'4'
		);

		$appointmentMapper->update($appointment, "appointment_id = '".$data['appointment-id']."'");

		$output = array(
			'result'=>'success'
		);

		echo json_encode($output);

	}


	public function create_appointment($type){
		$data = $_POST;
		switch($type){
			case 'walk-in':
				//Create Walk in By Admin
				$this->create_appointment_walkin($data);
			break;
			case 'resident-sched':
				$this->create_appointment_resident_sched($data);
			break;
		}
		/*
			1 = Resident Queue
			2	=	Walk in
		*/
	}

	public function resident_appointment_list(){
		$appointmentMapper = new App\Mapper\AppointmentMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$user_id = $_SESSION['current_user']['id'];

		$resident = $residentMapper->getByFilter("resident_user_id = '". $user_id."' ", true);

		$appointmentList = $appointmentMapper->getResidentAppointmentList($resident['resident_id']);
		$this->_data['appointment_list'] = $appointmentList;
		$this->is_secure = true;
    $this->view('appointment/resident_appointment_list');
	}

	public function request_appointment_list(){
		$appointmentMapper = new App\Mapper\AppointmentMapper();
		$params = array(
			'type'	=> array('1')
		);
		$requestedAppointmentList = $appointmentMapper->getRequestedAppointmentList($params);
		$params['type'] = array('3', '4');
		$confirmedAppointmentList = $appointmentMapper->getRequestedAppointmentList($params);
		$this->_data['requestedAppointmentList'] = $requestedAppointmentList;
		$this->_data['confirmedAppointmentList'] = $confirmedAppointmentList;
		$this->is_secure = true;
    $this->view('appointment/request_appointment_list');
	}


	private function create_appointment_walkin($data){
		$appointmentMapper = new App\Mapper\AppointmentMapper();
		if(!empty($data)){
			if(!empty($data['resident-no'])){
				$datetime = date('Y-m-d G:i:s', strtotime($data['schedule-date'].' '.$data['schedule-time'].':00:00'));
				$appointmentMapper->insert(array(
					'appointment_title'	=>	''
				,	'appointment_resident_id'=>$data['resident-no']
				,	'appointment_requested_datetime'=>$datetime
				,	'appointment_initial_summary'=>$data['initial-summary']
				,	'appointment_is_approved_sched'=>'1'
				,	'appointment_approved_sched'=>$datetime
				,	'appointment_approved_message'=>'Walk In'
				,	'appointment_is_confirm'=>'1'
				,	'appointment_confirmed_datetime'=>$datetime
				,	'appointment_check_datetime'=>$datetime
				,	'appointment_status'	=> '4'
				));
				$this->set_alert(array(
					'type'=>'success'
				,	'message'=>'Successfully Created an appointment'
				));
			}
			else{
				$this->set_alert(array(
					'type'=>'danger'
				,	'message'=>'Invalid! No Selected Resident!'
				));
			}
		}
		$form_data = array();
		$this->_data['form_data'] = $form_data;
		$this->is_secure = true;
    $this->view('appointment/create_appointment_walkin');
	}

	private function create_appointment_resident_sched($data){
		$appointmentMapper = new App\Mapper\AppointmentMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$user_id = $_SESSION['current_user']['id'];

		$resident = $residentMapper->getByFilter("resident_user_id = '". $user_id."' ", true);
		$form_data = array(
			'resident_no'=>$resident['resident_id']
		);
		if(!empty($data)){
			if(!empty($data['resident-no'])){
				$datetime = date('Y-m-d G:i:s', strtotime($data['schedule-date'].' '.$data['schedule-time'].':00:00'));
				$appointmentMapper->insert(array(
					'appointment_title'	=>	''
				,	'appointment_resident_id'=>$data['resident-no']
				,	'appointment_requestsent_datetime'=>date('Y-m-d H:i:s')
				,	'appointment_requested_datetime'=>$datetime
				,	'appointment_initial_summary'=>$data['initial-summary']
				,	'appointment_status'	=> '1'
				));
				$this->set_alert(array(
					'type'=>'success'
				,	'message'=>'Successfully Requested an appointment! '
				));
				$this->redirect(DOMAIN.'appointment/resident-appointment-list');
			}
			else{
				$this->set_alert(array(
					'type'=>'danger'
				,	'message'=>'Invalid! No Selected Resident!'
				));
			}
		}
		$this->_data['form_data'] = $form_data;
		$this->is_secure = true;
    $this->view('appointment/create_appointment_resident_sched');
	}

	private function getAccessFromAppointment($appointStatus, $access){
		$status = array(
			'description'=>''
		,	'cancel_button'=>false
		,	'confirm_button'=>false
		,	'checkin_button'=>false
		,	'has_requested_date'=>false
		,	'requested_date_done'=>false
		,	'approved_sched'=>false
		,	'approved_message'=>false
		,	'do_treatment'=>false
		);

		switch($appointStatus){
			case '1':
				$status['description'] = 'Waiting for Approval';
				$status['cancel_button'] = true;
			break;
			case '2':
				$status['description'] = 'Approved. Needs Confirmation on the said appointment';
				$status['has_requested_date'] = true;
				$status['requested_date_done'] = true;
				$status['cancel_button'] = true;
				$status['confirm_button'] = true;
				$status['approved_sched'] = true;
				$status['approved_message'] = true;
			break;
			case '3':
				$status['description'] = 'Confirmed Appointment. Waiting for Physical Appearance';
				$status['cancel_button'] = true;
				$status['has_requested_date'] = true;
				$status['requested_date_done'] = true;
				$status['approved_sched'] = true;
				$status['approved_message'] = true;
				if($access == 'admin'){
					$status['checkin_button'] = true;
					$status['do_treatment'] = true;
				}

			break;
			case '4':
				$status['description'] = 'Resident Arrived';
				$status['cancel_button'] = true;
				$status['has_requested_date'] = true;
				$status['requested_date_done'] = true;
				$status['approved_sched'] = true;
				$status['approved_message'] = true;
				if($access == 'admin'){
					$status['do_treatment'] = true;
				}

			break;
			case '5':
				$status['description'] = 'Medical Appointment / Treatment Completed ';
			break;
			case '6':
			case '7':
			case '8':
			case '9':
				$status['description'] = 'Cancelled';
			break;
		}
		return $status;
	}

}
