<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends Controller {

	public function __construct(){
		parent::__construct();
	}

	public function system_info(){
		echo phpinfo();
	}

  public function dashboard(){
			$this->is_secure = true;
			if(!$this->sess->isLogin()) $this->redirect(DOMAIN.'login');
			$this->_data['user_id'] = $_SESSION['current_user']['id'];
			if($_SESSION['current_user']['type'] == '1'){
				//Means Admin
				$this->load_admin();
			}
			if($_SESSION['current_user']['type'] == '2'){
				//Means Resident
				$this->load_resident();
			}
			if($_SESSION['current_user']['type'] == '3'){
				//Means Resident
				$this->load_pharmacist();
			}
			if($_SESSION['current_user']['type'] == '4'){
				//Means Resident
				$this->load_mofficer();
			}
			if($_SESSION['current_user']['type'] == '5'){
				//Means Resident
				$this->load_bhws();
			}
  }

	public function dashboard_catch(){
		$input = $_POST;
		$result = array();
		$date = $input['today'];
		$user_id = $input['user'];
		$userMapper = new App\Mapper\UserMapper();
		// $employerMapper = new App\Mapper\EmployerMapper();
		// $applicantMapper = new App\Mapper\ApplicantMapper();
		// $adminMapper = new App\Mapper\AdminMapper();
		// $jobPostingMapper = new App\Mapper\JobPostingMapper();
		// $applicantSkillMapper = new App\Mapper\ApplicantSkillMapper();
		// $applicantApplicationMapper = new App\Mapper\ApplicantApplicationMapper();


		$user = $userMapper->getByFilter("user_id = '".$user_id."'", true);
		if($user['user_type'] == 1){
			$appointmentMapper = new App\Mapper\AppointmentMapper();

			$appointmentList = $appointmentMapper->getDashboardStatus($date);

			$result['incoming_appointment'] = $appointmentList;
			// $admin = $adminMapper->getByFilter("admin_user_id = '".$user['user_id']."'");
			// $registered_applicant_count = $userMapper->getByFilter("user_type = '2'");
			// $result['registered_applicant_count'] = count($registered_applicant_count);
			// $registered_employer_count = $userMapper->getByFilter("user_type = '3'");
			// $result['registered_employer_count'] = count($registered_employer_count);
			// $job_posted_count = $jobPostingMapper->getAllCount();
			// $result['job_posted_count'] = $job_posted_count;
			// $applicant_registration_linegraph = $applicantMapper->getApplicantRegisteredCount();
			// $result['applicant_registration_linegraph'] = $applicant_registration_linegraph;
			// $top_skill = $applicantSkillMapper->getSkillTop();
			// $result['top_skill'] = $top_skill;


		}
		else if($user['user_type'] == 2){
			// $applicant = $applicantMapper->getByFilter("applicant_user_id = '".$user['user_id']."'", true);
			// $searchResult = $this->JobSearch_Model->getJobList($applicant['applicant_id']);
			//
			// $applicantApplication = $applicantApplicationMapper->getByFilter("aa_applicant_id = '".$applicant['applicant_id']."'");
			// $result['job_qualify_count'] = count($searchResult);
			// $result['application_count'] = count($applicantApplication);
		}
		else if($user['user_type'] == 3){
			// $employer = $employerMapper->getByFilter("employer_user_id = '".$user['user_id']."'", true);
			// $jobPosting = $jobPostingMapper->getByFilter("jp_employer_id = '".$employer['employer_id']."'");
			// $result['job_posted_count'] = count($jobPosting);
		}
		// $user_id = $_POST

		// $PCRequestMapper = new App\Mapper\PCRequestMapper();
		// $date = $input['today'];
		//
		// $currentProcessedPCRequest = $PCRequestMapper->getTotalPendingByDate($date);
		// $result['sched_police_clearance'] = $currentProcessedPCRequest;
		echo json_encode($result);
	}

	private function load_admin(){
		$this->view('home/dashboard_admin');
	}

	private function load_resident(){
		$this->view('home/dashboard_resident');
	}

	private function load_pharmacist(){
		$this->view('home/dashboard_pharmacist');
	}

	private function load_mofficer(){
		$this->view('home/dashboard_mofficer');
	}

	private function load_bhws(){
		$this->view('home/dashboard_bhws');
	}




	public function check_qrcode($qrcode){
		//echo $qrcode;

	}

	public function error_404(){
		$this->is_secure = false;
		$this->_template = 'templates/main';
		$this->view('home/error_404');
	}

	public function landing_page(){
		$announcementMapper = new App\Mapper\AnnouncementMapper();
		$referenceMapper = new App\Mapper\ReferenceMapper();
		$announcement = $announcementMapper->getAll();

		$data = array(
			'annoucement_list'=>$announcement,
			'act_jan' => $referenceMapper->getByFilter("ref_key = 'JAN'", true)['ref_value'],
            'act_feb' => $referenceMapper->getByFilter("ref_key = 'FEB'", true)['ref_value'],
            'act_mar' => $referenceMapper->getByFilter("ref_key = 'MAR'", true)['ref_value'],
            'act_apr' => $referenceMapper->getByFilter("ref_key = 'APR'", true)['ref_value'],
            'act_may' => $referenceMapper->getByFilter("ref_key = 'MAY'", true)['ref_value'],
            'act_jun' => $referenceMapper->getByFilter("ref_key = 'JUN'", true)['ref_value'],
            'act_jul' => $referenceMapper->getByFilter("ref_key = 'JUL'", true)['ref_value'],
            'act_aug' => $referenceMapper->getByFilter("ref_key = 'AUG'", true)['ref_value'],
            'act_sep' => $referenceMapper->getByFilter("ref_key = 'SEP'", true)['ref_value'],
            'act_oct' => $referenceMapper->getByFilter("ref_key = 'OCT'", true)['ref_value'],
            'act_nov' => $referenceMapper->getByFilter("ref_key = 'NOV'", true)['ref_value'],
            'act_dec' => $referenceMapper->getByFilter("ref_key = 'DEC'", true)['ref_value']
		);
		$this->load->view('home/landing_page', $data);
	}
}
